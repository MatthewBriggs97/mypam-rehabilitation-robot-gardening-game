More information on MyPAM see:
https://eps.leeds.ac.uk/faculty-engineering-physical-sciences/news/article/5515/using-robotics-to-treat-stroke-survivors
https://youtu.be/ZqntdASwN00
https://alumni.leeds.ac.uk/stroke-robotics 

SOURCE CODE:
To view, the C# scripts and project files check the directory "GameSourceCode/Assets" here is all the source code for each game within the main game. Each file within the source code has a partner meta file, this is to tell the Unity Engine how to import each file is its loaded into a new project. See below at "HOW TO OPEN AS UNITY PROJECT FROM SOURCE CODE:".

EXECUTING THE GAME:
In the exe zip file is the game ready to be run. Just double click the Tulip Street.exe within that folder. 
You can use Shift+Click to pull options up to select which screen and settings to use.  
Make a shortcut to your desktop of the Tulip Street.exe. For ease of use.

HOW TO OPEN AS UNITY PROJECT FROM SOURCE CODE:

Use Unity engine 2018.3.1f1.

When Unity is installed, make a new project.
Once the new project is made close the Unity.
This new project is simply a template for The Tulip Street Game.

Export the Source code and copy and paste both the assets folder and project settings folder into the new template projects folder.
Overwriting any of the other projectsettings/asset files.
As said here: https://answers.unity.com/questions/273912/moving-the-project.html

This should give you access to the project.
Ensure all scenes are in the hierarchy are in the following order:
Pentagram scene (Active)
Falling Game (Not active)
dotToDot (Not active)
SnakeGame (Not active)
GameGardening (Not active)

Thanks, if you have any inquires email me at matthewbriggs97@outlook.com

Matthew Briggs


