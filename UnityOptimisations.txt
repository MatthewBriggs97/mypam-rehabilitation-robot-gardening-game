Using the following links, I was able to optimise the MyPAM game, Tulip street to work on low-level hardware.
These will give you an understanding of what to check when trying to optimise your project:
https://unity3d.com/learn/tutorials/topics/performance-optimization 
https://unity3d.com/how-to/unity-ui-optimization-tips

Make sure you give the sections a good read. Especially the garbage collection. 
Some of the main selections of where to increase performance was disabling unneeded ray casts on text and images within the UI.
An important note is to ensure that any animation scripts only play when the attached objects are visible by the camera. 
If water textures and effects are used, they can have a massive impact, ensure it is only reflecting the most important objects/skyboxes.
The water resolution should be kept tweaked as when running on low-end equipment it is important to have a low load on any integrated graphics. 
V-sync should never be used. To minimise screen tearing tweak the applications frame rate, around 55 FPS is a good spot to avoid FPS spikes, as well as lowering visible screen tear and visible rapid decrease in the frame rate. 
The current custom graphics settings for playing the game Gardening game are called “test”. This is a very good balance between the visibility of the game and the responsiveness needed.
