﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

/// <summary>
/// This <c>SnakePlayerController</c> class.
/// Contains all methods for growing the snake tail and stopping asnd starting LabVIEW.
/// <list type="bullet">
/// <item>
/// <term>InitTail</term>
/// <description>Creates a new place for the apple to appear</description>
/// </item>
/// <item>
/// <term>AddTail</term>
/// <description>Makes the tail longer</description>
/// </item>
/// <item>
/// <term>EndGame</term>
/// <description>Calls the relevent methods to end the game and send data over LabVIEW</description>
/// </item>
/// <item>
/// <term>DemoGame</term>
/// <description>Method called when demo starts to show the user the data</description>
/// </item>
/// <item>
/// <term>MoveTail</term>
/// <description>animatesthe tail to the relevent postion</description>
/// </item>
/// <item>
/// <term>ResetTail</term>
/// <description>makes the tail the defualt length</description>
/// </item>
/// <item>
/// <term>UpdateScore</term>
/// <description>updates the current score to be displayed</description>
/// </item>
/// <item>
/// <term>CheckSwallow</term>
/// <description>checking is the snake has eaten a apple to call relevent methods to animate</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class SnakePlayerController : MonoBehaviour
{

    /// <summary>
    /// Short cut to the Transform body
    /// </summary>
    Transform tr;

    /// <summary>
    /// A few public text's for setting score and welcoming text
    /// </summary>
    public Text scoreText;
    public Text mainGameText;
    public GameObject mainGameTextGameObject;

    // keyboard control enabled
    public bool keyboardControlEnabled;

    public GameObject tailPrefab;
    public GameObject apple;

    List<GameObject> tail;

    public float freeMoveRadius = 0.4f;
    Vector3 circleCentre;
    public int maxScore;
    public float swallowSpeed;

    List<float> applesInBody;

    public int initialTailSize;

    Vector3 prevPos;

    public bool alanMode;

    private bool delay = false;

    private float speedOfSnake;

    private bool endGame = false;

    /// <summary>
    /// on start up of the snake game sets all relevent values, and init's the tail and sets texts.
    /// </summary>
    void Start()
    {
        // get transform, as we will need to set the position from labview
        tr = GetComponent<Transform>();
        applesInBody = new List<float>();
        tail = new List<GameObject>();
        InitTail();

        // fetch score text

        scoreText.text = "Current Score: " + (PlayerPrefs.GetInt("mainGameScore", 0));
        //define intial position
        circleCentre = tr.position;
        prevPos = tr.position;

        if (SnakeDemo.doSnakeDemo)
        {
            StartCoroutine(DemoGame());
        }
        else
        {
            OverScenesManager.PlayLabView = true; // turn the assistance on
        }

        speedOfSnake = OverScenesManager.Speed;
        StartCoroutine(RecordLocation());

    }

    /// <summary>
    /// called every frame, checks if its in demo, also moves the snake when needed
    /// </summary>    
    void Update()
    {

        if (SnakeDemo.doSnakeDemo)
        {
            if ((Vector3.Distance(transform.position, apple.transform.position) > 0.1f) && delay)
            {
                transform.position = Vector3.MoveTowards(transform.position, apple.transform.position, speedOfSnake * Time.deltaTime);
            }
        }

        //movement
        Vector3 diffVector;
        diffVector = tr.position - circleCentre;
        while (diffVector.magnitude > freeMoveRadius)
        {
            circleCentre = circleCentre + diffVector.normalized * Mathf.Min(freeMoveRadius, diffVector.magnitude);
            if (tail.Count != 0)
            {
                MoveTail();
            }
            diffVector = tr.position - circleCentre;

        }
        //turning
        Vector3 moveDirection = tr.position - prevPos;
        if (moveDirection != Vector3.zero)
        {
            float angle = Mathf.Atan2(moveDirection.z, moveDirection.x) * Mathf.Rad2Deg;
            tr.rotation = Quaternion.AngleAxis(270 - angle, Vector3.up);
        }
        prevPos = tr.position;

        CheckSwallow();

    }


    IEnumerator RecordLocation()
    {
        yield return new WaitForSeconds(0.75f);

        //OverScenesManager.coordinatesList.Reset();
       // OverScenesManager.coordinatesList.SetGameName("SnakeGame");
        int i = 0;
        while (!endGame && i < 600)
        {
           /* OverScenesManager.coordinatesList.AddToList(apple.transform.position.x, apple.transform.position.z,
                                            transform.position.x, transform.position.z,
                                            apple.transform.position.x, apple.transform.position.z); */
            yield return new WaitForSeconds(0.1f);
            i++;
        }
        //OverScenesManager.coordinatesList.WriteToCSV();
    }

    /// <summary>
    /// Makes tail with the preset sizes
    /// </summary>
    void InitTail()
    {
        for (int i = 0; i < initialTailSize; i++)
        {
            AddTail(true);
        }
    }

    /// <summary>
    /// Adds new object to tail
    /// </summary>
    /// <param name="init"></param>
    public void AddTail(bool init)
    {
        if (tail.Count > maxScore + initialTailSize - 1)
        {
            endGame = true;
            ResetTail();
        }
        else
        {

            Vector3 lastPos;
            if (tail.Count != 0)
            {
                lastPos = tail.Last().transform.position;
            }
            else
            {
                lastPos = tr.position;
            }

            // creates new tail part

            GameObject tailPart = Instantiate(tailPrefab, lastPos, Quaternion.identity);
            int c = tail.Count;
            float h, s, v;
            Color.RGBToHSV(tailPart.GetComponent<MeshRenderer>().material.color, out h, out s, out v);

            h = (h + 0.02f * Mathf.Cos(c * 0.1f * Mathf.PI)) % 1;
            s = Mathf.Min(1f, s + 0.05f * Mathf.Cos(c * 0.5f * Mathf.PI));
            //v = Mathf.Min (1f, v + 1f * Mathf.Sin(c*Mathf.PI));

            tailPart.GetComponent<MeshRenderer>().material.color = Color.HSVToRGB(h, s, v);

            //int numDisabledColliders = (int) Mathf.Floor(tailPart.GetComponent<SphereCollider> ().radius/freeMoveRadius);

            if (tail.Count <= 5)
            {
                tailPart.GetComponent<SphereCollider>().enabled = false;
            }

            tail.Add(tailPart);

            if (!init)
            {
                applesInBody.Add(1f);
            }
        }

    }

    /// <summary>
    /// Ends the game and increments the level to start next level
    /// </summary>
    private IEnumerator EndGame()
    {
        delay = false;

        mainGameTextGameObject.SetActive(true);
        mainGameText.text = "Good Job!";
        yield return new WaitForSeconds(3);
        mainGameTextGameObject.SetActive(true);
        mainGameText.text = "You stopped all the caterpillars from eating your plants!";

        SnakeDemo.doSnakeDemo = false;

        if (OverScenesManager.gameMode)
        {
            OverScenesManager.gameMode = false;
        }
        else
        {
            OverScenesManager.level += 1;
        }
        OverScenesManager.NextScene = "Gamegardening";
        OverScenesManager.NextPlayerName = "Test player";
        OverScenesManager.NextTargetName = "Target";
        OverScenesManager.LoadOverScene = true;
    }

    /// <summary>
    /// Shows the text at the start of the snake game in demo case.
    /// </summary>
    IEnumerator DemoGame()
    {
        mainGameTextGameObject.SetActive(true);
        mainGameText.text = "Welcome to the Caterpillar demo!";
        yield return new WaitForSeconds(3);
        mainGameText.text = "All the caterpillars from eating your plants!";
        yield return new WaitForSeconds(3);
        mainGameText.text = "Give them apples to stop them!";
        yield return new WaitForSeconds(3);
        mainGameTextGameObject.SetActive(false);
        delay = true;
    }

    /// <summary>
    /// Resets the tail to rhe default size.
    /// </summary>
    void ResetTail()
    {
        foreach (GameObject n in tail)
        {
            Destroy(n);
        }
        tail.Clear();
        StartCoroutine(EndGame());
        InitTail();
    }

    /// <summary>
    /// sets the postion of the snakes tail to the last, to make it look like it moves
    /// </summary>
    public void MoveTail()
    {
        if (tail.Count != 1)
        {
            for (int i = tail.Count - 1; i > 0; i--)
            {
                tail[i].transform.position = tail[i - 1].transform.position;
            }
        }

        //first part replaces head position
        tail[0].transform.position = circleCentre;

    }

    /// <summary>
    /// Updates the score to save.
    /// </summary>
    public void UpdateScore()
    {
        int score = tail.Count - initialTailSize;
        if (!SnakeDemo.doSnakeDemo)
        {
            PointSystem.ScoreValue += 10;
            scoreText.text = "Score: " + (PlayerPrefs.GetInt("mainGameScore", 0) + score).ToString();
        }
        else
        {
            scoreText.text = "Score: " + (PlayerPrefs.GetInt("mainGameScore", 0)).ToString();
        }

    }

    /// <summary>
    /// if swallowed it animates the tail to look like its eating
    /// </summary>
    public void CheckSwallow()
    {
        // updates positions of apples inside stomach
        if (applesInBody.Count > 0)
        {
            for (int i = 0; i < applesInBody.Count; i++)
            {
                applesInBody[i] += swallowSpeed * Time.deltaTime;
                if (applesInBody[i] > tail.Count + 1)
                {
                    applesInBody.RemoveAt(i);
                    i--;
                }
            }
        }

        // calculates scale of all tail objects;
        for (int i = 0; i < tail.Count; i++)
        {
            float s = CalculateScale(i);
            tail[i].transform.localScale = new Vector3(s, s, s);
        }

    }

    /// <summary>
    /// Uses preset values to make tail grow or shrink
    /// </summary>
    /// <param name="x"></param>
    public float CalculateScale(int x)
    {
        float w = 3.0f;
        float p = 0.25f;
        float b = 0.8f;

        float tempS = 0f;

        if (applesInBody.Count > 0)
        {
            foreach (float n in applesInBody)
            {
                if (x > n - w && x <= n)
                {
                    tempS += p / w * (x - (n - w));
                }
                else if (x < n + w && x > n)
                {
                    tempS += -p / w * (x - n) + p;
                }
            }
            tempS = Mathf.Min(tempS, 0.4f);
            return b + tempS;
        }
        else
        {
            return b;
        }
    }

}
