﻿using System.Collections;
using UnityEngine;


/// <summary>
/// This <c>SnakeGameScript</c> class.
/// When the public static started is set to true will run the snake game
/// <list type="bullet">
/// <item>
/// <term>StartSnakeGame</term>
/// <description>Plays snake game</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class SnakeGameScript : MonoBehaviour
{
    /// <summary>
    /// The static bool started for changing between scenes
    /// </summary>
    public static bool started = false;

    /// <summary>
    /// Called everyframe just checks if it should start the game
    /// </summary>
    void Update()
    {
        if (started)
        {
            Movement.MoveToPostion = new Vector3(2.6f, 0.58f, 4.43f);
            Movement.MoveToRotation = new Vector3(3.6f, -109.95f, 0f);

            started = false;

            StartCoroutine(StartSnakeGame());
        }
    }

    /// <summary>
    /// when called will get the game ready to play the snake game
    /// </summary>
    private IEnumerator StartSnakeGame()
    {
        yield return new WaitForSeconds(6);
        Movement.MoveToPostion = new Vector3(-3.48f, 0.67f, 4.51f);
        Movement.MoveToRotation = new Vector3(-1.03f, -150.49f, 0f);
        yield return new WaitForSeconds(5);

        OverScenesManager.NextScene = "SnakeGame";
        OverScenesManager.NextPlayerName = "Snake";
        OverScenesManager.NextTargetName = "apple";
        yield return new WaitForSeconds(0.5f);
        OverScenesManager.LoadOverScene = true;
    }
}