﻿using UnityEngine;

/// <summary>
/// This <c>AppleController</c> class.
/// Contains all methods for eating and moving the apple.
/// <list type="bullet">
/// <item>
/// <term>New Position</term>
/// <description>Creates a new place for the apple to appear</description>
/// </item>
/// <item>
/// <term>randomPos</term>
/// <description>gets the new postion for a random rang</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class AppleController : MonoBehaviour
{
    /// <summary>
    /// The script on the snake object
    /// </summary>
    public SnakePlayerController snakeCon;

    /// <summary>
    /// Short cut to the Transform body
    /// </summary>
    Transform tr;

    Vector3 newPos;

    Vector3 oldPos = new Vector3(1000, 0.5f, 1000);
    
    /// <summary>
    /// On start up of the game, gets the transform and new position
    /// </summary>
    void Start()
    {

        // get transform, as we will need to use it when changing target position
        tr = GetComponent<Transform>();
        // fetch snake object

        // being at random position
        NewPosition();
    }



    /// <summary>
    /// Every frame this is called, makes the apple rotate.
    /// </summary>
    void Update()
    {
        tr.Rotate(0, 0, Time.deltaTime * 50f);
    }

    /// <summary>
    /// when the snake hits calls methods to make the snake grow, also changes postion 
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        snakeCon.AddTail(false);
        snakeCon.UpdateScore();
        NewPosition();
    }

    /// <summary>
    /// Gets the new postion for the apple by calling random position
    /// </summary>
    void NewPosition()
    {

        int i = 0;
        newPos = RandomPos();

        while (Vector3.Distance(newPos, oldPos) < 3 && i < 10)
        {
            newPos = RandomPos();
            i++;
        }

        tr.position = newPos;
        oldPos = newPos;

    }

    /// <summary>
    /// returns a new random postion
    /// </summary>
    /// <returns></returns>
    Vector3 RandomPos()
    {
        return new Vector3(Random.Range(-4.5f, 4.5f), 0.5f, Random.Range(-4.5f, 4.5f));
    }

}
