﻿using UnityEngine;

/// <summary>
/// This <c>SnakeDemo</c> class.
/// This script weather the user wants to see the demo or play the game.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class SnakeDemo : MonoBehaviour
{
    public static bool doSnakeDemo = false;

    public GameObject snake;
    public GameObject demoSnake;

    /// <summary>
    /// makes the set snake true if is in demo mode or not.
    /// </summary>
    void Start()
    {
        if (doSnakeDemo)
        {
            demoSnake.SetActive(true);
            GameObject.Find("apple").GetComponent<AppleController>().snakeCon = demoSnake.GetComponent<SnakePlayerController>();
        }
        else
        {
            snake.SetActive(true);

            GameObject.Find("apple").GetComponent<AppleController>().snakeCon = snake.GetComponent<SnakePlayerController>();
        }
    }
}
