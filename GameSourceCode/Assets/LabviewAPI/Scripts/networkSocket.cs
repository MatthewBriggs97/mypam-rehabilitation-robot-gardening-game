using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;

 

public class networkSocket : MonoBehaviour
{
    public String host = "localhost";
    public Int32 port = 50000;

    internal Boolean socket_ready = false;
	internal String jsonString = "";
    TcpClient tcp_socket;
    NetworkStream net_stream;

    StreamWriter socket_writer;
    StreamReader socket_reader;

	public ServerData serverData;
	public ClientData clientData;

	public GameObject targetGameObject;
	public GameObject playerGameObject;

	public float xScale, yScale = 1f;

	Transform targetTr;
	Transform playerTr;

    void Update()
    {
		try{
			//writing
			clientData.TargetPosition.x = targetTr.localPosition.x/xScale;
			clientData.TargetPosition.y = targetTr.localPosition.z/yScale;
			jsonString = JsonUtility.ToJson (clientData);

			//reading
			writeSocket (jsonString);
			string received_data = readSocket(true);
	        if (received_data != "")
	        {
	        	// Do something with the received data,
				try{
					//serverData = JsonUtility.FromJson<ServerData>(received_data);
					JsonUtility.FromJsonOverwrite(received_data, serverData);
					playerTr.localPosition = new Vector3(serverData.EndEffectorPosition.x*xScale, playerTr.localPosition.y, serverData.EndEffectorPosition.y*yScale);
				}
				catch (Exception e){
					Debug.Log ("JSON PARSE ERROR: " + e);
				}
			}
		} catch (Exception e){
			Debug.Log ("NETWORK ERROR: " + e);
		}


    }


    void Awake()
    {
        setupSocket();
		targetTr = targetGameObject.GetComponent<Transform> ();
		playerTr = playerGameObject.GetComponent<Transform> ();
		clientData = new ClientData ();
		clientData.TargetPosition = new TargetPosition();
    }

    void OnApplicationQuit()
    {
        closeSocket();
    }

	[Serializable]
	public class ServerData
	{
		public EndEffectorPosition EndEffectorPosition;
	}

	[Serializable]
	public class EndEffectorPosition
	{
		public float x;
		public float y;

	}

	[Serializable]
	public class ClientData
	{
		public TargetPosition TargetPosition;
	}

	[Serializable]
	public class TargetPosition
	{
		public float x;
		public float y;

	}


    public void setupSocket()
    {
        try
        {
            tcp_socket = new TcpClient(host, port);

            net_stream = tcp_socket.GetStream();
            socket_writer = new StreamWriter(net_stream);
            socket_reader = new StreamReader(net_stream);


            socket_ready = true;
        }
        catch (Exception e)
        {
        	// Something went wrong
            Debug.Log("Socket error: " + e);
        }
    }

    public void writeSocket(string line)
    {
        if (!socket_ready)
            return;
            
        line = line + "\r\n";
        socket_writer.Write(line);
        socket_writer.Flush();
    }

	public String readSocket(bool wait)
    {
        if (!socket_ready)
            return "";

		if (wait) {
			// waits for data
			return socket_reader.ReadLine();
		}

		// outputs empty string if nothing available
		if (net_stream.DataAvailable)
        	return socket_reader.ReadLine();
        return "";
    }

    public void closeSocket()
    {
        if (!socket_ready)
            return;

        socket_writer.Close();
        socket_reader.Close();
        tcp_socket.Close();
        socket_ready = false;
		Debug.Log ("Closing connection");
    }

}