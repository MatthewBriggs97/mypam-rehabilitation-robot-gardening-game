﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System;
using System.Net;
using System.Text;
using System.Threading;

public class udpTransmission : MonoBehaviour {

    public string IPAddr = "127.0.0.1";
    public int Port = 61557;

    private UdpClient Client;
    private bool Connected = false;
    private string Packet;

    // Use this for initialization
    void OnGUI() {

        if (!Connected) {
            GUILayout.BeginHorizontal();
            IPAddr = GUILayout.TextField(IPAddr);
            GUILayout.Label("IP Address");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            string TempPort = GUILayout.TextField(Port.ToString());
            Port = int.Parse(TempPort);
            GUILayout.Label("Port");
            GUILayout.EndHorizontal();

            if (GUILayout.Button("CONNECT"))
            {
                print("Connecting to " + IPAddr + ":" + Port + "...");
                InitClient();
            }
        }
        else {
            // We are connected
            if (GUILayout.Button("DISCCONNECT"))
            {
                CloseClient();
            }

            GUILayout.BeginHorizontal();
            GUILayout.Label("Number : " + Packet);
            GUILayout.EndHorizontal();

        }
    }


    private void InitClient()
    {
        Client = new UdpClient(Port);
        try
        {
            Client.BeginReceive(new AsyncCallback(recv), null);
            Connected = true;
        }
        catch (Exception e)
        {
            print(e.ToString());
        }
    }

    private void CloseClient()
    {
        Client.Close();
        Connected = false;
    }

    //CallBack
    private void recv(IAsyncResult res)
    {
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 8000);
        byte[] received = Client.EndReceive(res, ref RemoteIpEndPoint);

        if (received == null || received.Length == 0)
        {
            print("No package recieved, aborting");
            return;
        }

        //Process codes
        Packet = Encoding.UTF8.GetString(received);

        print("got packet " + DateTime.Now.ToString());

        Client.BeginReceive(new AsyncCallback(recv), null);
    }
}
