﻿using UnityEngine;
using System;

/// <summary>
/// This <c>BasicController</c> class.
/// This controls the play and the target. Converts the workspace for the ow level hardware/microcontrollers
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class BasicController : MonoBehaviour
{
    /// <summary>
    /// The speed of the manule controls not using the MyPAM
    /// </summary>
    public float maxKBHorizontalSpeed = 10f;
    public float maxKBVerticalSpeed = 10f;
    /// <summary>
    /// The player from the scene.
    /// </summary>
    private Transform player;
    /// <summary>
    /// The target from the scene for assist.
    /// </summary>
    private Transform target;
    /// <summary>
    /// The player from the scene.
    /// </summary>
    public GameObject controllableGameObject;
    /// <summary>
    /// The target from the scene for assist
    /// </summary>
    public GameObject targetGameObject;
    /// <summary>
    /// The offset where the workspace is.
    /// </summary>
    public Vector3 gameOffset = new Vector3(0, 0, 0);
    /// <summary>
    ///  game coordiantes.
    /// </summary>
    public Vector3 gameWorkspace = new Vector3(10, 10, 10);
    /// <summary>
    /// Makesure the player can only go in the work space.
    /// </summary>
    public bool limitPlayerToWorkspace = false;
    /// <summary>
    /// Makes the game inverted when pressing "i".
    /// </summary>
    public bool invertControls = false;

    public bool invertDirections = false;
    /// <summary>
    /// This will be set to -1, and this will invert the controls.
    /// </summary>
    private int doInvert = 1;

    public AxesToControl axesToControl = AxesToControl.XandZ;
    public KeyCode keyToQuit;

    private LabviewConnection lvCon;

    /// <summary>
    /// Called at the startup of the scene, checks for invertion and finds the labview connection script.
    /// </summary>
    void Start()
    {
        InitObjects();
        /*if (invertControls)
        {
            doInvert = -1;

        }
        if (invertDirections)
        {
            invertDirections = true;
        }*/
        // fetch labview connection from contoller
        UDP_Handling.Traj_Flag = false;
        lvCon = GameObject.Find("LVUMain").GetComponent<LabviewConnection>();
    }

    /// <summary>
    /// Sets the player and target to there Transform.
    /// </summary>
    void InitObjects()
    {
        try
        {
            player = controllableGameObject.GetComponent<Transform>();
        }
        catch (Exception err)
        {
            Debug.Log("<color=orange>Warning, no player object set: </color>" + err);
        }

        try
        {
            target = targetGameObject.GetComponent<Transform>();
        }
        catch (Exception err)
        {
            Debug.Log("<color=orange>Warning, no target object set: </color>" + err);
        }

    }

    /// <summary>
    /// Makesure that the Transform are set.
    /// </summary>
    void GetObjects()
    {
        try
        {
            player = controllableGameObject.GetComponent<Transform>();
        }
        catch { }

        try
        {
            target = targetGameObject.GetComponent<Transform>();
        }
        catch { }
    }


    /// <summary>
    /// Called every frame, checks what keys are pressed for quiting and for inversions.
    /// </summary>
    void Update()
    {
        if (Input.GetKey(keyToQuit))
        {
            lvCon.clientStatusData.gameState = LabviewConnection.GameState.Close;
            lvCon.SendStatus();
            Debug.Log("Closing");
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }

      /*  if (Input.GetKeyDown("i")) // makes left right and up down
        {
            if (doInvert == 1)
            {
                doInvert = -1;
            }
            else
            {
                doInvert = 1;
            }
        }

        if (Input.GetKeyDown("c")) // c key makes left up and right down
        {
            if (invertDirections == true)
            {
                invertDirections = false;
            }
            else
            {
                invertDirections = true;
            }
        }*/
    }

    /// <summary>
    /// FixedUpdate is called at regular intervals. this gets objects and sets there positions
    /// </summary>
    void FixedUpdate()
    {
        GetObjects();

        if (!UDP_Handling.working)
        {
            // keyboard control, if we aren't connected
            if (player != null)
            {
                float newDelta1 = maxKBHorizontalSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
                float newDelta2 = maxKBVerticalSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
                if (axesToControl == AxesToControl.XandY)
                  /*  if (invertDirections)
                    {
                        player.position += new Vector3(doInvert * newDelta2, doInvert * newDelta1, 0f);
                    }
                    else
                    {*/
                        player.position += new Vector3(doInvert * newDelta1, doInvert * newDelta2, 0f);
                   // }

                else if (axesToControl == AxesToControl.XandZ)
                  /*  if (invertDirections)
                    {
                        player.position += new Vector3(doInvert * newDelta2, 0f, doInvert * newDelta1);
                    }
                    else
                    {*/
                        player.position += new Vector3(doInvert * newDelta1, 0f, doInvert * newDelta2);
                  //  }

            }
        }
        else
        {
            // labview control, if we are connected


            // set the position of the player in the game to the robot position
            if (player != null) SetPlayerPosition();
            // send the target position to the labview
            if (target != null) SendTargetPosition();
        }
    }

    /// <summary>
    /// Sets the new postion according to there workspace
    /// </summary>
    void SetPlayerPosition()
    {
        // Vector3 newPos = convertToWorld(lvCon.serverStreamData.hPos);
        Vector3 newPos = convertToWorld(new Vector3((float)UDP_Handling.X2pos, (float)UDP_Handling.Y2pos, (float)UDP_Handling.Z2pos));

        // set player position to robot's hand position
        float x, y, z;

        if (limitPlayerToWorkspace)
        {

            Vector3 halfWrkspce = gameWorkspace / 2;

            x = LimitToRange(newPos.x, -halfWrkspce.x, halfWrkspce.x);
            y = LimitToRange(newPos.y, -halfWrkspce.y, halfWrkspce.y);
            z = LimitToRange(newPos.z, -halfWrkspce.z, halfWrkspce.z);
        }
        else
        {
            x = newPos.x;
            y = newPos.y;
            z = newPos.z;
        }
        

        if (axesToControl == AxesToControl.XandY)
        {
            player.localPosition = gameOffset + new Vector3(x, y, player.position.z);
        }
        else
        {
            player.localPosition = gameOffset + new Vector3(x, player.position.y, z);
        }
    }

    /// <summary>
    /// Limits the player to the workspace.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="inclusiveMinimum"></param>
    /// <param name="inclusiveMaximum"></param>
    /// <returns></returns>
    public float LimitToRange(float value, float inclusiveMinimum, float inclusiveMaximum)
    {
        if (value < inclusiveMinimum) { return inclusiveMinimum; }
        if (value > inclusiveMaximum) { return inclusiveMaximum; }
        return value;
    }

    /// <summary>
    /// Sends the data to the labveiw connection.
    /// </summary>
    void SendTargetPosition()
    {
        Vector3 scaledTarget = convertToUDP(target.localPosition - gameOffset);

        UDP_Handling.Xtarget = scaledTarget.x;
        UDP_Handling.Ytarget = scaledTarget.y;
        UDP_Handling.Ztarget = scaledTarget.z;

        //lvCon.clientStreamData.tPos = scaledTarget;

        // you may want some logic to decide the force scale of the target... 0 = no force, 1 = max force for this assistance level
        //lvCon.clientStreamData.tWeight = 1f;
    }

    /// <summary>
    /// Changes the data from the arm to work in our Unity workspace. UDP
    /// </summary>
    /// <param name="inputPosition">The input postion vector</param>
    /// <returns></returns>
    public Vector3 convertToWorld(Vector3 inputPosition)
    {
        // scaledMovement = (float)UDP_Handling.X2pos / 150;

        // scales labview/robot space into game space
        float x, y, z;
        if (axesToControl == AxesToControl.XandY)
        {
            x = inputPosition.x / 150 * gameWorkspace.x;
            y = inputPosition.y / 105 * gameWorkspace.y;
            z = inputPosition.z / 125 * gameWorkspace.z;
        }
        else
        {
            x = inputPosition.x / 150 * gameWorkspace.x;
            y = inputPosition.z / 125 * gameWorkspace.y;
            z = inputPosition.y / 105 * gameWorkspace.z;
        }

        Vector3 gamePosition;
       /* if (invertDirections)
        {
            gamePosition = new Vector3(doInvert * z, doInvert * y, doInvert * x);
        }
        else
        {*/
            gamePosition = new Vector3(doInvert * x, doInvert * y, doInvert * z);
       // }

        return gamePosition;
    }

    /// <summary>
    /// Changes the data from the arm to work in our Unity workspace.
    /// </summary>
    /// <param name="inputPosition">The input postion vector</param>
    /// <returns></returns>
    public Vector3 convertToWorldLabViewEditionTCP(Vector3 inputPosition)
    {

        // scales labview/robot space into game space
        float x, y, z;
        if (axesToControl == AxesToControl.XandY)
        {
            x = inputPosition.x / lvCon.serverConfigData.workspace.x * gameWorkspace.x;
            y = inputPosition.y / lvCon.serverConfigData.workspace.y * gameWorkspace.y;
            z = inputPosition.z / lvCon.serverConfigData.workspace.z * gameWorkspace.z;
        }
        else
        {
            x = inputPosition.x / lvCon.serverConfigData.workspace.x * gameWorkspace.x;
            y = inputPosition.z / lvCon.serverConfigData.workspace.z * gameWorkspace.y;
            z = inputPosition.y / lvCon.serverConfigData.workspace.y * gameWorkspace.z;
        }

        Vector3 gamePosition;
        /* if (invertDirections)
         {
             gamePosition = new Vector3(doInvert * z, doInvert * y, doInvert * x);
         }
         else
         {*/
        gamePosition = new Vector3(doInvert * x, doInvert * y, doInvert * z);
        // }

        return gamePosition;
    }

    /// <summary>
    /// converts the position in unity to display in LabVIEW.
    /// </summary>
    /// <param name="gamePosition">position in game</param>
    /// <returns>The new converted output vector 3</returns>
    public Vector3 convertToUDP(Vector3 gamePosition)
    {
        // scales game space to labview/robot workspace
        float x, y, z;
        if (axesToControl == AxesToControl.XandY)
        {
            x = gamePosition.x * 150 / gameWorkspace.x;
            y = gamePosition.y * 105 / gameWorkspace.y;
            z = gamePosition.z * 125 / gameWorkspace.z;
        }
        else
        {
            x = gamePosition.x * 150 / gameWorkspace.x;
            y = gamePosition.z * 125 / gameWorkspace.z;
            z = gamePosition.y * 105 / gameWorkspace.y;
        }

        Vector3 outputPosition = new Vector3(doInvert * x, doInvert * y, doInvert * z);


        return outputPosition;
    }


    /// <summary>
    /// converts the position in unity to display in LabVIEW.
    /// </summary>
    /// <param name="gamePosition">position in game</param>
    /// <returns>The new converted output vector 3</returns>
    public Vector3 convertToLabview(Vector3 gamePosition)
    {
        // scales game space to labview/robot workspace
        float x, y, z;
        if (axesToControl == AxesToControl.XandY)
        {
            x = gamePosition.x * lvCon.serverConfigData.workspace.x / gameWorkspace.x;
            y = gamePosition.y * lvCon.serverConfigData.workspace.y / gameWorkspace.y;
            z = gamePosition.z * lvCon.serverConfigData.workspace.z / gameWorkspace.z;
        }
        else
        {
            x = gamePosition.x * lvCon.serverConfigData.workspace.x / gameWorkspace.x;
            y = gamePosition.z * lvCon.serverConfigData.workspace.y / gameWorkspace.z;
            z = gamePosition.y * lvCon.serverConfigData.workspace.z / gameWorkspace.y;
        }

        Vector3 outputPosition = new Vector3(doInvert * x, doInvert * y, doInvert * z);


        return outputPosition;
    }

    /// <summary>
    /// This shows what plane to play in.
    /// </summary>
    public enum AxesToControl
    {
        XandY, XandZ
    }
}