﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using UnityEngine.UI;


/// <summary>
/// This <c>LabviewConnection</c> class.
/// This is the most important script when it comes to the connection between Unity and the MyPAM's ower level hardware. It uses TCP.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class LabviewConnection : MonoBehaviour
{
    /// <summary>
    /// When this variable is set to true it will makesure that the Plugin is kept over new scenes
    /// </summary>
    public bool keepBetweenScenes = true;

    /// <summary>
    /// This is the key pressed to make the reconnect panel open, normally F1
    /// </summary>
    public KeyCode controlPanelKey;

    /// <summary>
    /// when set to true shows its connected to the server
    /// </summary>
    public bool connectedToServer = false;

    /// <summary>
    /// There is a method what goes throuugh the single board Rio IP and the My Rio IP.\n
    /// This is what the IP is of the suggested server.
    /// </summary>
    public string remoteServerIp;

    // TCP stuff
    /// <summary>
    /// The port for the tcp to connect to.
    /// </summary>
    public int tcpPort;

    /// <summary>
    /// The client been used.
    /// </summary>
    TcpClient tcpClient;

    NetworkStream netStream;
    StreamWriter streamWriter;
    StreamReader streamReader;
    public Boolean tcpReady = false;
    // declare objects which contain the data
    /// <summary>
    /// Some of the packages that will be set up and down back and fourth.
    /// </summary>
    public ServerConfigData serverConfigData;
    public ClientConfigData clientConfigData;
    string configString;
    public ClientStatusData clientStatusData;

    // UDP stuff
    public int udpLocalPort;
    public int udpRemotePort;
    public Boolean udpReady = false;
    // udpclient object
    UdpClient udpClient;
    // thread
    Thread receiveThread;
    // Remote ip & port
    IPEndPoint remoteIpEndPoint;
    // declare objects which contain the data
    public ServerStreamData serverStreamData;
    public ClientStreamData clientStreamData;

    string streamText;
    /// <summary>
    /// The Intup feild of where the ip will be sent to.
    /// </summary>
    InputField ipInputField;
    Toggle connectedToggle;

    public GameObject controlPanelCanvasObject;
    Canvas controlPanelCanvas;
    Boolean controlPanelActive;

    /// <summary>
    /// This sets the framerate for the game.
    /// </summary>
    public int frameRate = 59;

    private string jsonStringUpdate;

    /// <summary>
    /// Just before the scene starts the awake is called.
    /// </summary>
    void Awake()
    {
        if (keepBetweenScenes)
        {//makesure it is set to not be destoryed on loading newe scenes
            DontDestroyOnLoad(gameObject);
        }
        if (GameObject.Find(gameObject.name) && GameObject.Find(gameObject.name) != gameObject)
        {
            Destroy(gameObject);
        }

        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = frameRate;
    }

    /// <summary>
    /// On start up of frame after awake, trys connecting and starts up the UI.
    /// </summary>
    void Start()
    {
        UIInit();
        //StartCoroutine(TryDifferntIps());
        connectedToServer = true;
    }

    /// <summary>
    /// The two Ip's for the MyRio and SingeBoardRio
    /// </summary>
    IEnumerator TryDifferntIps()
    {
        if (!connectedToServer)
        {
            ipInputField.text = "172.22.11.2";

            ReConnect();
            yield return new WaitForSeconds(0.5f);
        }

        if (!connectedToServer)
        {
            ipInputField.text = "192.168.0.15";

            ReConnect();
            yield return new WaitForSeconds(0.5f);
        }

    }

    /// <summary>
    /// called every frame, when this button is pressed.
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(controlPanelKey))
        {
            ToggleControlPanel();
        }
    }

    /// <summary>
    ///   The GUI connected tick
    /// </summary>
    void OnGUI()
    {
        connectedToggle.isOn = connectedToServer;
    }


    /// <summary>
    /// UI setup
    /// </summary>
    private void UIInit()
    {
        // get input field object, set text to that in public var (soon: load from config)
        ipInputField = GameObject.Find("IPInputField").GetComponent<InputField>();
        ipInputField.text = remoteServerIp;
        // hide menu until button press
        controlPanelCanvas = GameObject.Find("Control Panel").GetComponent<Canvas>(); ;
        controlPanelActive = false;
        controlPanelCanvas.enabled = controlPanelActive;

        // get connected check box, set it
        connectedToggle = GameObject.Find("ConnectedState").GetComponent<Toggle>();
        connectedToggle.isOn = connectedToServer;

        controlPanelCanvasObject.SetActive(false);
    }

    /// <summary>
    /// turn the controll panel on and off.
    /// </summary>
    public void ToggleControlPanel()
    {
        controlPanelActive = !controlPanelActive;
        controlPanelCanvas.enabled = controlPanelActive;
        controlPanelCanvasObject.SetActive(controlPanelActive);
    }

    /// <summary>
    /// The first person working on this plugin made this, speaks for its self. a little odd.
    /// </summary>
    /// <param name="line"></param>
    public void LogLine(string line)
    {
        Debug.Log(line);
    }

    /// <summary>
    /// This method attemps to connect to the hardware.
    /// </summary>
    private void Connect()
    {
        connectedToServer = false;
        tcpReady = false;
        udpReady = false;

        TCPInit();
        if (tcpReady)
        {
            UDPLocalInit();
            WaitForConfig();
            SendConfig();
            UDPRemoteInit();
        }

        if (tcpReady && udpReady)
            connectedToServer = true;
        else
            connectedToServer = false;

    }

    /// <summary>
    /// Called to try reconnect to the hardware server.
    /// </summary>
    public void ReConnect()
    {
        if (!connectedToServer)
        {//if not connected retrys.
            LogLine("<color=green>Reconnecting</color>");
            remoteServerIp = ipInputField.text;
            Connect();
        }
        else
        {
            LogLine("<color=green>Already connected</color>");
        }

    }

    /// <summary>
    /// TCP DATA (config, player, score data, sent/received infrequently)
    /// </summary>
    private void WaitForConfig()
    {
        configString = ReadSocket(true);
        serverConfigData = JsonUtility.FromJson<ServerConfigData>(configString);

        // interpret config
        udpRemotePort = serverConfigData.network.serverUdpPort;
    }

    /// <summary>
    /// Sends the starting config data.
    /// </summary>
    private void SendConfig()
    {

        // send config

        clientConfigData.network.clientUdpPort = udpLocalPort;

        string jsonString = JsonUtility.ToJson(clientConfigData);
        WriteSocket(jsonString);

    }

    /// <summary>
    /// Start, Stop, Pause, Play. What the assist should do.
    /// </summary>
    public void SendStatus()
    {
        // send current status
        string jsonString = JsonUtility.ToJson(clientStatusData);
        WriteSocket(jsonString);
    }

    /// <summary>
    /// Starts up the TCP
    /// </summary>
    private void TCPInit()
    {
        try
        {
            //tcpClient = new TcpClient(remoteServerIp, tcpPort);

            tcpClient = new TcpClient();
            var connectionResult = tcpClient.BeginConnect(remoteServerIp, tcpPort, null, null);

            if (!connectionResult.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(10)))
            {
                tcpReady = false;
                throw new Exception("TCP connection timeout");
            }
            else
            {
                netStream = tcpClient.GetStream();
                streamWriter = new StreamWriter(netStream);
                streamReader = new StreamReader(netStream);
                netStream.ReadTimeout = 5000;//time outs
                netStream.WriteTimeout = 5000;
                tcpReady = true;
            }
            tcpClient.EndConnect(connectionResult);
        }
        catch (Exception e)
        {
            CloseConnection();
            // Something went wrong
            LogLine("<color=red>Socket error: </color>" + e);
        }
    }

    /// <summary>
    /// Writes to the sockets link.
    /// </summary>
    /// <param name="line">Value to write</param>
    private void WriteSocket(string line)
    {
        if (!tcpReady)
            return;

        try
        {
            line = line + "\r\n";
            streamWriter.Write(line);
            streamWriter.Flush();
        }
        catch (Exception e)
        {
            CloseConnection();
            // Something went wrong
            LogLine("<color=red>TCP write error: </color>" + e);
        }

    }

    /// <summary>
    /// If data is avliable or should wait for it, it will try recive this data.
    /// </summary>
    /// <param name="wait">If true waits for line</param>
    /// <returns>The data</returns>
    private String ReadSocket(bool wait)
    {
        try
        {
            string line;
            if (!tcpReady)
                return "";

            // waits for data
            if (wait)
            {
                line = streamReader.ReadLine();
                return line;
            }

            // outputs empty string if nothing available
            if (netStream.DataAvailable)
            {
                line = streamReader.ReadLine();
                return line;
            }
        }
        catch (Exception e)
        {
            CloseConnection();
            // Something went wrong
            LogLine("<color=red>TCP read error: </color>" + e);
        }

        return "";
    }

    /// 
    /// JSON Data type declaration - must be same as labview clusters
    /// 


    /// Data received from labview
    [Serializable]
    public class ServerConfigData
    {
        public NetworkServerData network;
        public WorkspaceData workspace;
        public UserInfoData userInfo;

    }

    [Serializable]
    public class NetworkServerData
    {
        public int serverUdpPort;

    }

    [Serializable]
    public class WorkspaceData
    {
        public float x;
        public float y;
        public float z;
    }

    [Serializable]
    public class UserInfoData
    {
        public string id;
        public float disabilityLevel;
        public float upperArmLength;
        public float lowerArmLength;
    }

    /// Data sent to LabVIEW

    [Serializable]
    public class ClientConfigData
    {
        public NetworkClientData network;

    }

    [Serializable]
    public class NetworkClientData
    {
        public int clientUdpPort;

    }

    /// Data sent periodically to labview, on changes in the game state, and score information etc

    [Serializable]
    public class ClientStatusData
    {
        public GameState gameState;
        public string playerName;
        public float playerScore;
        public string gameUnits;
    }


    public enum GameState
    {
        Start, Pause, Stop, Close
    }


    /// <summary>
    /// Closes the connection safely so that it can be opened in the future.
    /// </summary>
    public void CloseTCP()
    {


        //if (!tcpReady)
        //	return;

        LogLine("<color=blue>Closing TCP connection</color>");

        tcpReady = false;
        if (tcpClient != null)
            tcpClient.Close();

        if (streamWriter != null)
            streamWriter.Close();

        if (streamReader != null)
            streamReader.Close();

    }

    /// <summary>
    /// UDP DATA (positional data, sent/received frequently), init.
    /// </summary>
    void UDPLocalInit()
    {
        try
        {
            // init client
            udpClient = new UdpClient(0);
            udpLocalPort = ((IPEndPoint)udpClient.Client.LocalEndPoint).Port;
            // blocking means it waits for data
            udpClient.Client.Blocking = true;
            udpClient.Client.ReceiveTimeout = 10000;//extended to work over scenes
            // being receiving
            //udpClient.BeginReceive(new AsyncCallback(ReceiveUDP), null);

        }
        catch (Exception err)
        {
            LogLine(err.ToString());
            udpReady = false;
        }
    }

    private void UDPRemoteInit()
    {
        try
        {
            // define server ip and port
            remoteIpEndPoint = new IPEndPoint(IPAddress.Parse(remoteServerIp), udpRemotePort);

            //thread
            receiveThread = new Thread(new ThreadStart(ReceiveUDP));
            receiveThread.IsBackground = true;
            receiveThread.Start();

            udpReady = true;
        }
        catch (Exception err)
        {
            LogLine(err.ToString());
            udpReady = false;
        }


    }


    /// <summary>
    /// send the jsonString at a constant rate.
    /// </summary>
    void FixedUpdate()
    {

        if(udpReady && connectedToServer)
        {
            jsonStringUpdate = JsonUtility.ToJson(clientStreamData);
            SendUDP(jsonStringUpdate);
        }
      /*  try
        {
            jsonStringUpdate = JsonUtility.ToJson(clientStreamData);
            SendUDP(jsonStringUpdate);
        }
        catch (Exception err)
        {
            //LogLine(err.ToString());
        }*/

    }

    /// <summary>
    /// Packages the datastring then sens to the udpclient.
    /// </summary>
    /// <param name="dataString">Changes this to bytes then sends</param>
    private void SendUDP(string dataString)
    {
        if (udpReady)
        {
            Byte[] sendBytes = Encoding.ASCII.GetBytes(dataString);
            udpClient.Send(sendBytes, sendBytes.Length, remoteServerIp, udpRemotePort);
            //LogLine ("Sending " + dataString);		
        }
    }

    /// <summary>
    /// Receiver of the UDP connection.
    /// </summary>
    private void ReceiveUDP()
    {
        while (Thread.CurrentThread.IsAlive && udpReady)
        {
            try
            {
                byte[] data = udpClient.Receive(ref remoteIpEndPoint);

                streamText = Encoding.UTF8.GetString(data);

                // parses JSON string and adds it to serverStreamData object
                JsonUtility.FromJsonOverwrite(streamText, serverStreamData);
            }
            catch (Exception err)
            {
                LogLine("<color=red>UDP connection timeout: </color>" + err.ToString());
                CloseConnection();
                Thread.Sleep(100);
            }
        }
    }
    
    /// 
    /// JSON Data type declaration - must be same as labview clusters
    /// 

    /// Data received from LabVIEW
    [Serializable]
    public class ServerStreamData
    {
        public Vector3 hPos;
        public Vector3 ePos;
        public Vector3 sPos;
        public UserInput uInput;

    }

    [Serializable]
    public class UserInput
    {
        public float g; //grip sensor 0-1
    }


    /// Data sent to LabVIEW
    [Serializable]
    public class ClientStreamData
    {
        public Vector3 tPos;
        public float tWeight;
        public Vector3 f;
        public float v;

    }

    /// <summary>
    /// Safely closes the UDP connection.
    /// </summary>
    private void CloseUDP()
    {
        //if (!udpReady)
        //	return;

        LogLine("<color=blue>Closing UDP connection</color>");

        udpReady = false;

        if (udpClient != null)
        {
            udpClient.Close();
        }
    }
    
    /// <summary>
    /// CLOSE
    /// Completly closes the whole connection calling the needed methods.
    /// </summary>
    void CloseConnection()
    {

        connectedToServer = false;
        CloseUDP();
        CloseTCP();
        if (receiveThread != null)
        {
            receiveThread.Abort();
        }

    }

    /// <summary>
    /// When the application is closed this should be called, and trys to safely disconnected from the servers.
    /// </summary>
    void OnApplicationQuit()
    {
        CloseConnection();
        //Debug.Log("CLOSING CONNECTION");
    }

    /// <summary>
    /// When the application is closed this should be called, and trys to safely disconnected from the servers.
    /// </summary>
    void OnDestroy()
    {
        CloseConnection();
       // Debug.Log("CLOSING CONNECTION");
    }
}