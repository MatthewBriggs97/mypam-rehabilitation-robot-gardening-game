﻿using UnityEngine;

public class GameScalingOptions : MonoBehaviour {

	public Vector3 robotToWorld = new Vector3(10,10,10);

	private LabviewConnection lvCon;

	// Use this for initialization
	void Start () {
		lvCon = GetComponent<LabviewConnection> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public Vector3 convertToWorld(Vector3 inputPosition){

		float x = inputPosition.x / lvCon.serverConfigData.workspace.x * robotToWorld.x;
		float y = inputPosition.y / lvCon.serverConfigData.workspace.y * robotToWorld.y;	
		float z = inputPosition.z / lvCon.serverConfigData.workspace.z * robotToWorld.z;	

		Vector3 outputPosition = new Vector3(x,y,z);
		return outputPosition;
	}

	public Vector3 convertToLabview(Vector3 inputPosition){
		
		float x = inputPosition.x  * lvCon.serverConfigData.workspace.x / robotToWorld.x;
		float y = inputPosition.y  * lvCon.serverConfigData.workspace.y / robotToWorld.y;
		float z = inputPosition.z  * lvCon.serverConfigData.workspace.z / robotToWorld.z;

		Vector3 outputPosition = new Vector3(x,y,z);
		return outputPosition;
	}

}
