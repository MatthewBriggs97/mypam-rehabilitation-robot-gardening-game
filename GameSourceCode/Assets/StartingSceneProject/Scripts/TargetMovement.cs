﻿using UnityEngine;


/// <summary>
/// This <c>TargetMovement</c> class.
/// Moves the object towords a new target.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class TargetMovement : MonoBehaviour
{
    public static Vector3 TargetMoveto = new Vector3(0, 0, 2);

    public bool UseDefaultPosition = true;

    private float UseSpeed;

    void Start()
    {
        UseSpeed = OverScenesManager.Speed;
    }

    void Update()
    {
        if (transform.localPosition != TargetMoveto && UseDefaultPosition)
        {
            float step = UseSpeed * Time.deltaTime;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, TargetMoveto, step);
        }
    }
}
/*  if (transform.position != TargetMoveto && UseDefaultPosition)//BACKUP
  {
      float step = UseSpeed * Time.deltaTime;
      transform.position = Vector3.MoveTowards(transform.position, TargetMoveto, step);
  }*/
