﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// This <c>OverScenesManager</c> class.
/// Probably One of the most important scripts, in fully charge of the LabVIEW assist state.
/// It also has the scripting for letting the LabVIEW stay connected whilst loading new scenes.
/// As it is kept over every scene, it holds most variables that will be used accross a lot of scripts.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class OverScenesManager : MonoBehaviour
{
    /// <summary>
    /// For recording data for each game. Will dump in a file for plotting to exel
    /// </summary>
    public static CoordinatesList coordinatesList;

    /// <summary>
    /// This is used in most scripts for the speed of the assist.
    /// </summary>
    public static float Speed = 5.5f;

    public GameObject RestGame;


    /// <summary>
    /// This records what is the current level.
    /// </summary>
    public static int level = 0;

    public static int plantLevel = 0;

    /// <summary>
    /// When true it will start trying to load over scenes.
    /// </summary>
    public static bool LoadOverScene = false;

    /// <summary>
    /// The Target from the current/new scene.
    /// </summary>
    public GameObject Target;

    /// <summary>
    /// The Player from the current/new scene.
    /// </summary>
    public GameObject Player;

    /// <summary>
    /// The name of the next scene to load.
    /// </summary>
    public static string NextScene = "Gamegardening";

    /// <summary>
    /// The name of the Player in the new scene to find.
    /// </summary>
    public static string NextPlayerName = "Test player";

    /// <summary>
    /// The name of the Target in the new scene to find.
    /// </summary>
    public static string NextTargetName = "Target";

    /// <summary>
    /// When this or the other LabVIEW bools are set to true it will stop, start or pause the assist.
    /// </summary>
    public static bool PlayLabView = false;
    public static bool StopLabView = false;//ONLY USE THIS IF THE GAME ENDS
    public static bool PauseLabView = false;//USE THIS INSTEAD OF PAUSE.

    /// <summary>
    /// The LabVIEW connection pluginn.
    /// </summary>
    private LabviewConnection lv;
    private bool LoadOverSceneChecker = false;
    public static bool SceneChanging = false;

    /// <summary>
    /// The next/new scenes objects.
    /// </summary>
    private GameObject[] arrayOfGameObjects;

    private bool allowedToSendCommand = true;

    public static float[,] theCropGrowth = new float[3, 6] { { 0, 0, 0, 0, 0, 0 },
                                                             { 0, 0, 0, 0, 0, 0 },
                                                             { 0, 0, 0, 0, 0, 0 }
                                                            };

    public static float[,] theDirtSize = new float[3, 6] { { 0, 0, 0, 0, 0, 0 },
                                                           { 0, 0, 0, 0, 0, 0 },
                                                           { 0, 0, 0, 0, 0, 0 }
                                                         };

    public static int[,] theSeedPlanted = new int[3, 6] { { 0, 0, 0, 0, 0, 0 },
                                                          { 0, 0, 0, 0, 0, 0 },
                                                          { 0, 0, 0, 0, 0, 0 }
                                                        };
    private bool InPromt = false;
    private bool RestScores;

    private Transform targetToLabView;
    private Transform userOnMyPAM;

    public float speed = 5f;

    /// <summary>
    /// These will be used for using a feture called game mode, this will play a selected game.
    /// </summary>
    public GameObject gameModeMenu;
    public static bool initGameMode = false;
    public static bool endGameMode = false;
    public static bool gameMode = false;
    public static int gameModeLevel = -1;
    public float GrowthSpeed = 1;
    private List<GameObject> imagesGameMode = new List<GameObject>();
    private bool inGameModeSelect = false;
    private bool chosen = false;
    private GameObject feedBack;

    /// <summary>
    /// At the start of the scene it will be called, getting the labview connection.
    /// </summary>
    void Start()
    {
        //PlayerPrefs.DeleteAll();
        PauseLabView = true;

        targetToLabView = transform.GetComponent<BasicController>().targetGameObject.transform;
        userOnMyPAM = transform.GetComponent<BasicController>().controllableGameObject.transform;

        lv = transform.GetComponent<LabviewConnection>();
        level = PlayerPrefs.GetInt("overScenesManagerLevel", 0);// 0

        foreach (Transform child in gameModeMenu.transform)
        {
            if (child.GetSiblingIndex() > 7)
            {
                imagesGameMode.Add(child.gameObject);
            }
            else if(child.name == "Selected")
            {
                feedBack = child.gameObject;
            }
        }
        coordinatesList = new CoordinatesList();
        // level = 0;
    }

    /// <summary>
    /// Called every frame checks if the LabVIEW status should change and if a new scene should be loaded.
    /// </summary>
    void Update()
    {
        if (LoadOverScene)
        {
            SceneChanging = true;
            LoadOverScene = false;
            StartCoroutine(LoadScene());

            LoadOverSceneChecker = true;

         //   print("LoadOverScene is true and alwys loading");

        }
        else if (!LoadOverSceneChecker)
        { // change to add max speed
            //Player.transform.position = transform.GetComponent<BasicController>().controllableGameObject.transform.position;
            // transform.GetComponent<BasicController>().targetGameObject.transform.position = Target.transform.position;
            Player.transform.position = userOnMyPAM.position;
            if(Vector3.Distance(Player.transform.position, Target.transform.position) <= 5)
            {
                //targetToLabView.position = Vector3.MoveTowards(targetToLabView.position, Target.transform.position, speed * Time.deltaTime);
                targetToLabView.position = Target.transform.position;
            }
            else
            {
                targetToLabView.position = Vector3.Lerp(Player.transform.position, Target.transform.position, 0.4f);
            }
           
        }

        if (lv.connectedToServer && allowedToSendCommand)
        {
            if (PlayLabView || Input.GetKeyDown(KeyCode.P))
            {
                StartCoroutine(TimerSendCommand());
                print("Play!");
                /*  lv.GetComponent<LabviewConnection>().clientStatusData.gameState = LabviewConnection.GameState.Start;
                  lv.GetComponent<LabviewConnection>().SendStatus(); */
                UDP_Handling.Assistance = true;
                UDP_Handling.AssistanceLevel = 50;
                PlayLabView = false;
            }
            else if (StopLabView)
            {
                StartCoroutine(TimerSendCommandStop());
                /*   lv.GetComponent<LabviewConnection>().clientStatusData.gameState = LabviewConnection.GameState.Stop;
                   lv.GetComponent<LabviewConnection>().SendStatus();*/
                UDP_Handling.Assistance = false;
                UDP_Handling.AssistanceLevel = 10;
                StopLabView = false;
            }
            else if (PauseLabView || Input.GetKeyDown(KeyCode.G))
            {
                StartCoroutine(TimerSendCommand());
                /*
                 *  fOR LAB VIEW TCP    
                 * 
                 * lv.GetComponent<LabviewConnection>().clientStatusData.gameState = LabviewConnection.GameState.Pause;
                 lv.GetComponent<LabviewConnection>().SendStatus();
                 */
                UDP_Handling.Assistance = false;
                UDP_Handling.AssistanceLevel = 10;
                PauseLabView = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.R) && !LoadOverScene)
        {
           // print("The r key was pressed and is now going to try load a scene");
            if (!InPromt)
            {
                InPromt = true;
                StartCoroutine(RestPrompt());
               // print("Now is running the method for the panel");
            }
            else
            {
              //  print("else starts loading scene");
                RestScores = true;
            }
        }
        else if ((Input.GetKeyDown(KeyCode.Space) || endGameMode) && !LoadOverScene && !chosen)
        {
            if(inGameModeSelect && !endGameMode)
            {
                chosen = true;
                feedBack.GetComponent<Text>().text = "Selected... Loading...";
                feedBack.SetActive(true);
            }
            else
            {
                GameMode();
            }
        }
        else if (Input.GetKeyDown(KeyCode.B) && inGameModeSelect && !LoadOverScene)
        {
            chosen = false;
            gameModeLevel = -2;
            feedBack.GetComponent<Text>().text = "Closing...";
            feedBack.SetActive(true);
        }
    }

    IEnumerator RestPrompt()
    {
        RestGame.SetActive(true);
        int i = 0;
        int time = 5;
        while (i < 40)
        {
            if (LoadOverScene)
            {
                RestScores = false;
                i = 40;
            }
            yield return new WaitForSeconds(0.125f);
            if (i == 10 || i == 20 || i == 30 || i == 0 || i == 40)
            {
                RestGame.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "Are you sure you want to clear the current score?\n" +
                                                                                        "Click \"R\" again to reset, if not please wait for: " + time;
                time -= 1;
            }

            if (RestScores)
            {
                i = 40;
            }
            if (LoadOverScene)
            {
                RestScores = false;
                i = 40;
            }
            i += 1;
        }
        RestGame.SetActive(false);
        InPromt = false;
        yield return new WaitForSeconds(0.5f);

        if (RestScores)
        {
            RestAllPlayerPrefs();
            RestScores = false;
        }
    }

    void RestAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        gameMode = false;
        level = 0;
        plantLevel = 0;
        theCropGrowth = new float[3, 6] { { 0, 0, 0, 0, 0, 0 },
                                          { 0, 0, 0, 0, 0, 0 },
                                          { 0, 0, 0, 0, 0, 0 }
                                        };

        theDirtSize = new float[3, 6] { { 0, 0, 0, 0, 0, 0 },
                                        { 0, 0, 0, 0, 0, 0 },
                                        { 0, 0, 0, 0, 0, 0 }
                                      };

        theSeedPlanted = new int[3, 6] { { 0, 0, 0, 0, 0, 0 },
                                         { 0, 0, 0, 0, 0, 0 },
                                         { 0, 0, 0, 0, 0, 0 }
                                       };
        LevelChoice.NextLevel = 0;
        PointSystem.ScoreValue = 0;
        LevelSlideSelector.SlideLevel = 0;
        LevelSupport.Level = 0;

        Movement.MoveToPostion = new Vector3(0, 75.2f, -17);
        Movement.MoveToRotation = new Vector3(54.4f, 0, 0);



        PlayerPrefs.DeleteAll();
        if (!LoadOverSceneChecker)
        {
            NextScene = "pentagramScene";
            NextPlayerName = "armPosition";
            NextTargetName = "armTarget";
            StartCoroutine(LoadScene());
            LoadOverSceneChecker = true;
        }
    }

    void GameMode()
    {
        if(endGameMode && !LoadOverSceneChecker)
        {
            NextPlayerName = "Test player";
            NextTargetName = "Target";
            NextScene = "Gamegardening";


            gameMode = false;
            endGameMode = false;
            level = PlayerPrefs.GetInt("overScenesManagerLevel", 0);
            feedBack.SetActive(false);
            LoadOverScene = true;
        }
        else if(!endGameMode)
        {
            inGameModeSelect = true;
            StartCoroutine(GameModeSelection());
        }

    }

    IEnumerator GameModeSelection()
    {
        gameModeMenu.SetActive(true);
        WaitForSeconds delay = new WaitForSeconds(0.01f);

        float maxSize = imagesGameMode[0].GetComponent<Transform>().localScale.x + 1f;
        int indexBackUp = 0; 
        foreach (GameObject img in imagesGameMode)
        {
            indexBackUp = img.transform.GetSiblingIndex();
            img.transform.SetSiblingIndex(12);
            while (maxSize > img.GetComponent<Transform>().localScale.x)
            {
                img.GetComponent<Transform>().localScale += Vector3.one * Time.deltaTime * GrowthSpeed;
                yield return delay;
            }

            while (1.5f < img.GetComponent<Transform>().localScale.y)
            {
                img.GetComponent<Transform>().localScale -= Vector3.one * Time.deltaTime * GrowthSpeed;
                yield return delay;
            }

            while (maxSize > img.GetComponent<Transform>().localScale.x)
            {
                img.GetComponent<Transform>().localScale += Vector3.one * Time.deltaTime * GrowthSpeed;
                yield return delay;
            }

            while (1 < img.GetComponent<Transform>().localScale.y)
            {
                img.GetComponent<Transform>().localScale -= Vector3.one * Time.deltaTime * GrowthSpeed;
                yield return delay;
            }
            img.transform.SetSiblingIndex(indexBackUp);
            if (chosen && !endGameMode) { int.TryParse(img.name, out gameModeLevel); break; }
            else if(gameModeLevel == -2) { break; }
            else if (endGameMode) { gameModeLevel = -2; }
        }
        feedBack.SetActive(false);
        gameModeMenu.SetActive(false);

        endGameMode = false;
        inGameModeSelect = false;
        if (gameModeLevel > 0 && !LoadOverSceneChecker)
        {
            NextPlayerName = "Test player";
            NextTargetName = "Target";
            NextScene = "Gamegardening";

            gameMode = true;
            LoadOverScene = true;
        }
        else
        {
            gameModeLevel = -1;
        }
    }

    /// <summary>
    /// A small delay between been able to send the start,stop, and pause commands to ensure the LabVIEW side dont get over requested.
    /// </summary>
    IEnumerator TimerSendCommand()
    {
        allowedToSendCommand = false;

        yield return new WaitForSeconds(0.5f);

        allowedToSendCommand = true;
    }

    /// <summary>
    /// A small delay between been able to send the start,stop, and pause commands to ensure the LabVIEW side dont get over requested.
    /// </summary>
    IEnumerator TimerSendCommandStop()
    {
        allowedToSendCommand = false;

        yield return new WaitForSeconds(2);

        allowedToSendCommand = true;
    }

    /// <summary>
    /// One of the most important method in the entire game.
    /// This has been improved many, many times. To ensure it works over scenes without timing out the LabVIEW on the Unity end and the lower end.
    /// </summary>
    IEnumerator LoadScene()
    {

        //This is making sure the assist isnt going to be running when the next scene is loading and been swapped out.
        //When the assist was on it caused problems.
        PauseLabView = true;
        if (!LoadOverSceneChecker)
        {
            LoadOverSceneChecker = true;
            allowedToSendCommand = false;
            //A small delay to ensure that every last command has been done and all to worry about is loading a new scene.
            yield return new WaitForSeconds(1.50f);

            //Using Async as everything should be running within the background,
            //if not it will crash/timeout and stop the connection from working.
            print(NextScene);
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(NextScene);

            //This is going to be loading the new scene, but returns every loop, to let the game play at the same time.
            while (!asyncLoad.isDone)
            {
                if (asyncLoad.progress == 0.9f)
                {//Checking untill its complete, this means at 0.9f its at 100% done.
                    asyncLoad.allowSceneActivation = true;//Lets the scene load.
                }
                yield return null;
            }
            //Gets all the game objects from the new scene.
            arrayOfGameObjects = SceneManager.GetSceneByName(NextScene).GetRootGameObjects();

            //Looping through the new game objects to find target and the player.
            for (int i = 0; i < arrayOfGameObjects.Length; i++)
            {
                if (arrayOfGameObjects[i].name == NextTargetName)
                {
                    Target = arrayOfGameObjects[i];
                }
                else if (arrayOfGameObjects[i].name == NextPlayerName)
                {
                    Player = arrayOfGameObjects[i];
                }
                yield return null;//FIXED SO MANY PROBLEMS WITH LOW END MACHINES
            }

            //If it disconnected in the process it will try reconnecting, just in case a time out happened.
            //lv.ReConnect();

            if (lv.connectedToServer)
            {//Gets into the new scene with the assist off.
                PauseLabView = true;
            }
            if (SpaceGameDemo.SpaceDemo && NextScene == "Gamegardening")
            {//Checking that the demos are set to false.
                SpaceGameDemo.SpaceDemo = false;
            }
            else if (DoToDoCore.DemoDotToDot && NextScene == "Gamegardening")
            {//Checking that the demos are set to false.
                DoToDoCore.DemoDotToDot = false;
            }
            if (gameMode && gameModeLevel <= 8)
            {
                PlayLabView = true; // turn the assistance on
            }
            //Now its over its just saying it is.
            LoadOverSceneChecker = false;
            allowedToSendCommand = true;
            chosen = false;
            if (gameMode && gameModeLevel <= 8)
            {
                yield return new WaitForSeconds(0.75f);
                PlayLabView = true; // turn the assistance on
            }
        }
    }

    void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("overScenesManagerLevel", level);
    }

    void OnDestroy()
    {
        PlayerPrefs.SetInt("overScenesManagerLevel", level);
    }










   





}

public class Coordinates
{
    public float TimeStamp = 0;
    public float TargetX = 0;
    public float TargetY = 0;
    public float PlayerX = 0;
    public float PlayerY = 0;
    public float EndTargetX = 0;
    public float EndTargetY = 0;
}

public class CoordinatesList
{

    /**
    * 
    * 
    * SECOND PART,
    * 
    * THIS SHOULD BE MOVED TO ANOTHER FILE FOR NICER READING HOW EVER NO TIME AS NEEDS TO BE DONE
    * 
    * FOR DATA READING AND RECORDING
    * 
    * MAIN SOURCE OF REFERENCE OF CODE:
    * https://sushanta1991.blogspot.com/2015/02/how-to-write-data-to-csv-file-in-unity.html
    * 
    */


    private List<Coordinates> ListOfCoordinates = new List<Coordinates>();
    private string gameName = "";
    private float TimeStamp = 0;
    

    public void Reset()
    {
        gameName = "";
        TimeStamp = 0;
        ListOfCoordinates.Clear();
    }

    public void SetGameName(string gameName)
    {
        this.gameName = gameName;
    }

    public void AddToList(float TargetX, float TargetY, float PlayerX, float PlayerY, float EndTargetX , float EndTargetY)
    {
        /*Coordinates coordinates = new Coordinates
        {
            PlayerX = PlayerX,
            PlayerY = PlayerY,
            TargetX = TargetX,
            TargetY = TargetY,
            EndTargetX = EndTargetX,
            EndTargetY = EndTargetY,
            TimeStamp = TimeStamp
        };
        ListOfCoordinates.Add(coordinates);*/
        TimeStamp += 0.01f;
    }

    public void WriteToCSV()
    {
        ListOfCoordinates.Clear();
        /*string delimiter = ",";

        StringBuilder stringBuilder = new StringBuilder();
        string[] stringArray = new string[8];

        stringArray[0] = "PlayerX";
        stringArray[1] = "PlayerY";
        stringArray[2] = "TargetX";
        stringArray[3] = "TargetY";
        stringArray[4] = "EndTargetX";
        stringArray[5] = "EndTargetY";
        stringArray[6] = "TimeStamp";
        stringArray[7] = gameName;
        stringBuilder.AppendLine(string.Join(delimiter, stringArray));
        stringArray[7] = "";//reset name
        foreach (Coordinates coordinates in ListOfCoordinates)
        {
            stringArray[0] = coordinates.PlayerX.ToString();
            stringArray[1] = coordinates.PlayerY.ToString();
            stringArray[2] = coordinates.TargetX.ToString();
            stringArray[3] = coordinates.TargetY.ToString();
            stringArray[4] = coordinates.EndTargetX.ToString();
            stringArray[5] = coordinates.EndTargetY.ToString();
            stringArray[6] = coordinates.TimeStamp.ToString();
            stringBuilder.AppendLine(string.Join(delimiter, stringArray));
        }


        string filePath = GetPath(DateTime.Now.ToString("yyyy_MM_dd_H_mm_ss") + "_" + gameName + ".csv");
        StreamWriter outStream = File.CreateText(filePath);
        outStream.WriteLine(stringBuilder);
        outStream.Close();*/
        Reset();
    }

    private string GetPath(string fileName)
    {
#if UNITY_EDITOR
        return "C:/data/" + fileName;
#else
        return "C:/data/"+ fileName;
#endif
    }


}