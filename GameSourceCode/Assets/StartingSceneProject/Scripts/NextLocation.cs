﻿using UnityEngine;

/// <summary>
/// This <c>NextLocation</c> class.
/// Used in the first scene, checks if the user is playing, if the user is then will assist in a pentaram fasion.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class NextLocation : MonoBehaviour
{
    private Vector3 NextPostion;

    /// <summary>
    /// Preset in the editor, this script will be on many object to let the user bounce around them.
    /// </summary>
    public GameObject NextObject;

    void Start()
    {
        NextPostion = NextObject.transform.position;
    }

    void Update()
    {
        if (LabVIEWStart.GameState == "Stop")
        {
            transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);
            transform.GetComponent<SphereCollider>().enabled = false;

            transform.GetComponent<MeshRenderer>().enabled = false;

        }
    }

    /// <summary>
    /// Moves the user onto the next target.
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if (LabVIEWStart.GameState == "Play")
        {
            TargetMovement.TargetMoveto = NextPostion;
            transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);
            NextObject.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(NextObject.GetComponent("Halo"), true, null);

            transform.GetComponent<SphereCollider>().enabled = false;
            NextObject.GetComponent<SphereCollider>().enabled = true;
            transform.GetComponent<MeshRenderer>().enabled = false;
            NextObject.GetComponent<MeshRenderer>().enabled = true;
            LabVIEWStart.targetsReached += 1;
        }
    }
}
