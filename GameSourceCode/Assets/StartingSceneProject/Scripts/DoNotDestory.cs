﻿using UnityEngine;

/// <summary>
/// This <c>DoNotDestory</c> class.
/// Makes object its on not be destroyed.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class DoNotDestory : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (GameObject.Find(gameObject.name) && GameObject.Find(gameObject.name) != gameObject)
        {
            Destroy(gameObject);
        }
    }
}
