﻿using System.Collections;
using UnityEngine;


/// <summary>
/// This <c>CameraTestLevel</c> class.
/// A test movement used, not in use at the moment.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class CameraTestLevel : MonoBehaviour {

    public static bool started = false;
    public GameObject player;

    // Update is called once per frame
    void Update()
    {


        if (started)
        {
            started = false;
            StartCoroutine(startCameraTest());
        }
    }

    private IEnumerator startCameraTest()
    {
        Movement.MoveToPostion = new Vector3(83.4281f, 31.69259f, -129.6809f);
        Movement.MoveToRotation = new Vector3(3.4f, 0f, 0f);
        yield return new WaitForSeconds(5f);

        Movement.MoveToPostion = new Vector3(83.4281f, 31.6f, 0.6809f);
        Movement.MoveToRotation = new Vector3(3.4f, -90f, 0f);
        yield return new WaitForSeconds(5f);

        Movement.MoveToPostion = new Vector3(53.4281f, 20f, 0.6809f);
        Movement.MoveToRotation = new Vector3(3.4f, 90f, 0f);
        yield return new WaitForSeconds(5f);

        Movement.MoveToPostion = new Vector3(33.4281f, 30f, 50.6809f);
        Movement.MoveToRotation = new Vector3(20f, 0f, 0f);
        yield return new WaitForSeconds(5f);

        Movement.MoveToPostion = new Vector3(0.4281f, 30f, 200.6809f);
        Movement.MoveToRotation = new Vector3(-20f, -180f, 0f);
        yield return new WaitForSeconds(5f);
    }
}