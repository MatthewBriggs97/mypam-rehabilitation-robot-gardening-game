﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;


/// <summary>
/// This graphing <c>DataRecord</c> class.
/// This is used to workout the person scorer in a more advanced way, for example working out there distance over time from the target.
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class DataRecord : MonoBehaviour
{
    public Text CountDown;
    public GameObject Target;

    private Transform TargetT;
    private string path;
    private float tempTime;
    public int testTime = 60;
    public float writeFrequency = 0.1f;

    public Text hudText;
    List<float> distances = new List<float>();

    public static float minimumDistance;
    public static float maximumDistance;
    public static float averageDistanceAway;

    private bool Checker = true;

    void Start()
    {
        TargetT = Target.transform;
    }

    void Update()
    {

        if (LabVIEWStart.startTimer == true)
        {
            StartCoroutine(RecordLocation());
            LabVIEWStart.startTimer = false;
        }
        if ((LabVIEWStart.GameState == "Stop") && (Checker == true))
        {
            ResetGame();
            Checker = false;
        }
    }

    IEnumerator RecordLocation()
    {
        yield return new WaitForSeconds(0.75f);
        /*  for (i = 0; i < (testTime / writeFrequency); i++)
          {
              distances.Add(Vector3.Distance(TargetT.position, transform.position));
              yield return new WaitForSeconds(writeFrequency);
          }*/
        OverScenesManager.coordinatesList.Reset();
        OverScenesManager.coordinatesList.SetGameName("Pentagram");
        while (testTime > 0)
        {
            //    print("Made it here in loop" + (testTime/writeFrequency));
            /* OverScenesManager.coordinatesList.AddToList(TargetT.position.x, TargetT.position.z,
                                                        transform.position.x, transform.position.z,
                                                        TargetMovement.TargetMoveto.x, TargetMovement.TargetMoveto.z); */
            CountDown.text = "Warm up!\n" + testTime.ToString() + " seconds to go!";
            yield return new WaitForSeconds(1);
            testTime--;
        }

        OverScenesManager.coordinatesList.WriteToCSV();
        // CalculateResults(distances);
        LabVIEWStart.gameEnd = true;
    }

    void CalculateResults(List<float> list)
    {

        minimumDistance = 999f;
        maximumDistance = 0f;
        averageDistanceAway = distances.Average();

        minimumDistance = distances.Min();
        maximumDistance = distances.Max();

        LabVIEWStart.gameEnd = true;
    }

    void PrintScores(int Score, float Time)
    {
        if (distances.Count > 1)
        {
            float DistancePoints = Mathf.Round(1000 - (100 * distances.Average()));
            hudText.text = "Score:\t\t\t" + Score + "\nAccuracy:\t" + DistancePoints + "\nTime:\t\t\t" + Time;
        }
        else
        {
            hudText.text = "Score:\t\t\t NA" + "\nAccuracy:\t NA" + "\nTime:\t\t\t NA";
        }

    }

    void ResetGame()
    {

        TargetMovement.TargetMoveto = new Vector3(0, 0, 2);
        minimumDistance = 999f;
        maximumDistance = 0f;
        LabVIEWStart.targetsReached = 0;

        distances.Clear();
        LabVIEWStart.GameState = "Pause";

        OverScenesManager.NextScene = "Gamegardening";
        OverScenesManager.NextPlayerName = "Test player";
        OverScenesManager.NextTargetName = "Target";
        OverScenesManager.LoadOverScene = true;
    }
}
