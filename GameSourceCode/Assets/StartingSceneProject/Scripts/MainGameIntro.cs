﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



/// <summary>
/// This <c>MainGameIntro</c> class.
/// This class contains all the information
/// for the main introduction to the game
/// it is done by flying the camera around
/// to all of the games the player will unlock #
/// and teaching them how the games work
/// 
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>


public class MainGameIntro : MonoBehaviour
{
    public static bool started = false;

    public static bool playing = false;

    public static bool showingIntro = true;
    public static float globalSpeedUp = 1f;


    

    /// <summary>
    /// these lists contain all the camera locations and rotations in a nice list
    /// </summary>
    private float[] posX = {  -13.8f, -0.3f,     0,   0.8f,    45.2f, 134.5f, 54.6f, -118.1f, -64f, -35.8f, 149.5f, 178.4f, 280.4f, };
    private float[] posY = {  4.9f,  9.1f,     3,  1.97f,     3f, 9.1f, 22.6f, 3.5f, 35.6f, 7.7f, 29.4f, 27.6f, 21.6f, 14.2f };
    private float[] posZ = {    -3.1f, -4.8f, -4.4f,  1.28f, -2.6f, 104.6f, 105.9f, 242.4f, 361f, 242.4f, 412.4f, 265.8f, 373.5f };
    private float[] rotX = {     6.5f, 65.3f, 19.6f, 13.95f,     7.2f, 11.8f, 4.5f, 12.72f, 12.5f, 7.5f, 3.6f, 1.4f, 7.7f };
    private float[] rotY = {   62.3f,    0f, 58.3f, 72.43f,   44f, -60f, 0.7f, 66.9f, 115.2f, 135.2f, -166f, 12.3f, -106.6f };
    private float[] rotZ = {  0f,     0f,    0f, -4.4f,     0f,    0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f };


    /// <summary>
    /// this array contrains all the text that will be displayed on the screen, it will tell the user what games there are and how to play them
    /// </summary>
    private string[] sentences = { "Welcome to the Tulip Street!",
                                   "This is your garden, where you will be growing your plants",
                                   "The first thing you do in your garden is to prepare your garden to plant seeds",
                                   "As well as raking, you can also plant the seeds in your garden",
                                   "You will also have to water plants to maintain them",
                                   "Finally, you will also have you get rid of growing weeds ",

                                 };


    public GameObject finalDispalyText;

    public GameObject helper;

    private Text textOfIntro;

    public int placeInLoop = 0;

    private GameObject[] arrayOfGameObjects;

    /*
    /// <summary>
    /// makes sure that the object containing this 
    /// script works over scenes by killing itself 
    /// if there is another version with the same name
    /// </summary>
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (GameObject.Find(gameObject.name) && GameObject.Find(gameObject.name) != this.gameObject)
        {
            Destroy(this.gameObject);
        }
    }
    */
    /// <summary>
    /// assigns the textOfIntro by finding it in the scene
    /// </summary>
    void Start()
    {
        textOfIntro = finalDispalyText.transform.GetChild(1).GetComponent<Text>();
        
    }

    /// <summary>
    /// checks for when started is set to true
    /// and moves the camera into the initial position
    /// </summary>
    void Update()
    {
        if (started)
        {
           //Movement .MoveToPostion = new Vector3(305f, 27.23f, 398.07f);
            //Movement.MoveToRotation = new Vector3(3.78f, -129.75f, 0f);
            started = false;

            StartCoroutine(DisplayTextAndFaffAbout());

            GameObject player = GameObject.Find("Test player");
            GameObject target = GameObject.Find("Target");

            player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(player.GetComponent("Halo"), false, null);
            target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(target.GetComponent("Halo"), false, null);
        }
    }

    /// <summary>
    /// The main part of the logic
    /// will keep on moving the camera around and show the relevent messages
    /// on the screen
    /// it can do diffferent things depending on the level, such as display the
    /// main game helper
    /// and show that the user is in a demo.
    /// It may need to call the start function of other games
    /// to show a demo or switch scenes.
    /// </summary>
    /// <returns></returns>
    private IEnumerator DisplayTextAndFaffAbout()
    {
        playing = true;
        Movement.speed = 100f * globalSpeedUp;
        finalDispalyText.SetActive(false);

        while (placeInLoop <= 5) // change this backto 5
        {
            if (placeInLoop == 1)
            {
                Movement.MoveToPostion = new Vector3(posX[placeInLoop], posY[placeInLoop], posZ[placeInLoop]);
                Movement.MoveToRotation = new Vector3(rotX[placeInLoop], rotY[placeInLoop], rotZ[placeInLoop]);

            }
            finalDispalyText.SetActive(true);
            textOfIntro.text = sentences[placeInLoop];

            yield return new WaitForSeconds(5);
            finalDispalyText.SetActive(false);


            yield return new WaitForSeconds(5);

            placeInLoop++;
        }
        Movement.started = true;
        //OverScenesManager.level += 1;
    }
}