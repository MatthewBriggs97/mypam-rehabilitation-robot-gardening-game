using UnityEngine;

/// <summary>
/// This <c>LabVIEWStart</c> class.
/// Stops, Starts and pauses LabVIEW, the assist.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class LabVIEWStart : MonoBehaviour
{
    public static string GameState;

    public GameObject inputField;

    public static bool gameEnd = false;

    public GameObject Player;

    public static bool startTimer = false;

    public static int targetsReached = 0;
    

    void Update()
    {
        if (gameEnd == true)
        {
           // print("EndGametrue\n");
            EndGame();
            gameEnd = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        GameState = "Play";
        //print("Play LabView");

        transform.GetComponent<SphereCollider>().enabled = false;
        transform.GetComponent<MeshRenderer>().enabled = false;
        TargetMovement.TargetMoveto = new Vector3(0, 0, 4);
        GameObject.Find("armTarget").GetComponent<TargetMovement>().UseDefaultPosition = true;
        startTimer = true;
    }

    void EndGame()
    {
        TargetMovement.TargetMoveto = new Vector3(0, 0, 2);
        
        GameState = "Pause";
      //  print("GAME ENDED");
        GameState = "Stop";
    }
}
