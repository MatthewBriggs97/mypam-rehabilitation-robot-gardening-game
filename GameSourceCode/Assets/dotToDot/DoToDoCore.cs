﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This major <c>DoToDoCore</c> class.
/// Contains all the methods used for dot to dot game
/// <list type="bullet">
/// <item>
/// <term>Start</term>
/// <description>Puts all of the possible dot to dots into the same array</description>
/// </item>
/// <item>
/// <term>Timer</term>
/// <description>Ends the game after 90 seconds and awards the user some points for their trouble</description>
/// </item>
/// <item>
/// <term>StartNewLevel</term>
/// <description>Puts the correct image for the background, begins a timer and sets the line drawing to the correct starting position</description>
/// </item>
/// <item>
/// <term>OnCollisionStay</term>
/// <description>This triggers every time the player or the demo player comes close to the point, it will draw a line, and move the target to the next point</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>This class is just for the dotToDot game, it can be called by settings PlaneGame.started = true, it will run through the whole game, then increment the level counter</para>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class DoToDoCore : MonoBehaviour
{

    public GameObject textBox;

    public Material lineMaterial;


    private Vector3[] Star = new[] { new Vector3(0, 0, 4.39f), new Vector3(1.127f, 0, 1.09f), new Vector3(4.36f, 0, 1.13f),
                                     new Vector3(1.87f, 0, -0.926f), new Vector3(2.765f, 0, -4.057f), new Vector3(0, 0, -2.2f),
                                     new Vector3(-2.765f, 0, -4.057f), new Vector3(-1.87f, 0, -0.926f), new Vector3(-4.36f, 0, 1.13f),
                                     new Vector3(-1.127f, 0, 1.09f),new Vector3(0, 0, 4.39f)
                                     };

    // might want to delete this because it's different from all of the others
    private Vector3[] Cat = new[] { new Vector3(-0.985f, 0, 1.531f), new Vector3(-1.55f, 0, -0.181f),
                                    new Vector3(-2.267f, 0, -2.64f), new Vector3(-1.5f, 0, -4.43f),
                                    new Vector3(1.102f, 0, -4.43f), new Vector3(2.07f, 0, -3),
                                    new Vector3(1.69f, 0, -0.83f), new Vector3(0.985f, 0, 1.531f),new Vector3(-0.985f, 0, 1.531f)
                                    };

    private Vector3[] Envelope = new[] { new Vector3(4f, 0, 3f), new Vector3(-4f, 0, 3f),
                                    new Vector3(-4f, 0, -3f), new Vector3(4f, 0, -3f),
                                    new Vector3(4f, 0, 3f), new Vector3(0f, 0, 0f),
                                    new Vector3(-4f, 0, 3f), new Vector3(4f, 0, 3f)
                                    };

    private Vector3[] Cube = new[] { new Vector3(1f, 0, 1f), new Vector3(-3f, 0, 1f),
                                    new Vector3(-3f, 0, -3f), new Vector3(1f, 0, -3f),
                                    new Vector3(1f, 0, 1f),
                                    new Vector3(3f, 0, 2f),
                                    new Vector3(-1f, 0, 2f),
                                    new Vector3(-1f, 0, -2f),
                                    new Vector3(3f, 0, -2f),
                                    new Vector3(3f, 0, 2f),
                                    new Vector3(-1f, 0, 2f),
                                    new Vector3(-3f, 0, 1f),
                                    new Vector3(-3f, 0, -3f),
                                    new Vector3(-1f, 0, -2f),
                                    new Vector3(3f, 0, -2f),
                                    new Vector3(1f, 0, -3f),
                                    new Vector3(1f, 0, 1f),
                                    new Vector3(1f, 0, 1f),

                                    };

    private Vector3[] Pyramid = new[] {

                                    new Vector3(-2f, 0, -2f),
                                    new Vector3(2f, 0, -2f),
                                    new Vector3(0f,0,3f),
                                    new Vector3(2f, 0, -2f),
                                    new Vector3(3f, 0, -1f),
                                    new Vector3(0f,0,3f),
                                    new Vector3(3f, 0, -1f),
                                    new Vector3(-1f, 0, -1f),
                                    new Vector3(0f,0,3f),
                                    new Vector3(-1f, 0, -1f),

                                    new Vector3(-1f, 0, -1f),
                                    new Vector3(-2f, 0, -2f),
                                    new Vector3(0f,0,3f),
                                    new Vector3(-2f, 0, -2f),



    };

    // might want to make the leaf bigger for the apple one
    private Vector3[] Apple = new[] {
                                    new Vector3(0, 0, 3f),
                                    new Vector3(1.5f, 0, 3.5f),
                                    new Vector3(3f, 0, 3.2f),
                                    new Vector3(4f, 0, 2f),
                                    new Vector3(4.3f, 0, 0f),
                                    new Vector3(4f, 0, -1f),
                                    new Vector3(3f, 0, -2.2f),
                                    new Vector3(1.5f, 0, -2.8f),
                                    new Vector3(0f,0,-3f),
                                    new Vector3(-1.5f, 0, -2.8f),
                                    new Vector3(-3f, 0, -2.2f),
                                    new Vector3(-4f, 0, -1f),
                                    new Vector3(-4.3f, 0, 0f),
                                    new Vector3(-4f, 0, 2f),
                                    new Vector3(-3f, 0, 3.2f),
                                    new Vector3(-1.5f, 0, 3.5f),
                                    new Vector3(0, 0, 3f),
                                    new Vector3(0.8f, 0, 4.3f),
                                    new Vector3(0.7f, 0, 4.3f),
                                    new Vector3(0.6f, 0, 4.2f),
                                    new Vector3(0.4f, 0, 4.1f),
                                    new Vector3(0.2f, 0, 3.325f),
                                    new Vector3(0.4f, 0, 3.325f),
                                    new Vector3(0.6f, 0, 3.425f),
                                    new Vector3(0.7f, 0, 3.625f),
                                    new Vector3(0.8f, 0, 4.3f),
                                    new Vector3(0f, 0, 3f),

                                     };


    // not sure which one this is
    private Vector3[] Tree = new[] {
                                     new Vector3(0, 0, -2f) * 1.5f,
                                     new Vector3(0, 0, -0.5f)* 1.5f,
                                     new Vector3(1.5f, 0, -0.5f)* 1.5f,
                                     new Vector3(0.7f, 0, 1f)* 1.5f,
                                     new Vector3(1f, 0, 1f)* 1.5f,
                                     new Vector3(0.4f, 0, 2f)* 1.5f,
                                     new Vector3(0.6f, 0, 2f)* 1.5f,
                                     new Vector3(0f, 0, 3f)* 1.5f,
                                     new Vector3(0f, 0, 3f)* 1.5f,
                                     new Vector3(-0.6f, 0, 2f)* 1.5f,
                                     new Vector3(-0.4f, 0, 2f)* 1.5f,
                                     new Vector3(-1f, 0, 1f)* 1.5f,
                                     new Vector3(-0.7f, 0, 1f)* 1.5f,
                                     new Vector3(-1.5f, 0, -0.5f)* 1.5f,
                                     new Vector3(0, 0, -0.5f)* 1.5f,
                                     new Vector3(0, 0, -2f)* 1.5f,

                                     };


    private Vector3[] Leaf = new[] {

                                    new Vector3(-2.62f,0,-3.58f),
                                    new Vector3(-2.06f,0,-3.26f),
                                    new Vector3(-1.44f,0,-2.59f),
                                    new Vector3(-0.58f,0,-2.62f),
                                    new Vector3(0.62f,0,-2.38f),
                                    new Vector3(1.54f,0,-1.64f),
                                    new Vector3(2.05f,0,-0.61f),
                                    new Vector3(2.25f,0,0.36f),
                                    new Vector3(2.3f,0,1.21f),
                                    new Vector3(2.34f,0,1.99f),
                                    new Vector3(2.52f,0,2.64f),
                                    new Vector3(1.77f,0,2.41f),
                                    new Vector3(0.83f,0,2.2f),
                                    new Vector3(0.01f,0,1.89f),
                                    new Vector3(-0.7f,0,1.46f),
                                    new Vector3(-1.14f,0,0.78f),
                                    new Vector3(-1.32f,0,0.03f),
                                    new Vector3(-1.36f,0,-0.66f),
                                    new Vector3(-1.4f,0,-1.3f),
                                    new Vector3(-1.4f,0,-1.98f),
                                    new Vector3(-1.44f,0,-2.58f),
                                    new Vector3(-2.06f,0,-3.27f),
                                    new Vector3(-2.63f,0,-3.58f),
                                    new Vector3(-2.62f,0,-3.59f),

    };


    private Vector3[] fish = new[]
    {
                                    new Vector3(4.14f,0,-3.32f),
                                    new Vector3(-2.18f,0,3.7f),
                                    new Vector3(-2.88f,0,2.55f),
                                    new Vector3(-2.84f,0,1.07f),
                                    new Vector3(-1.89f,0,-0.29f),
                                    new Vector3(-2.02f,0,-1.78f),
                                    new Vector3(-2.42f,0,-2.67f),
                                    new Vector3(-2.36f,0,-3.14f),
                                    new Vector3(-2.06f,0,-3.28f),
                                    new Vector3(-2.03f,0,-3.51f),
                                    new Vector3(-1.66f,0,-3.12f),
                                    new Vector3(-1.81f,0,-3.06f),
                                    new Vector3(-1.85f,0,-2.77f),
                                    new Vector3(-2.21f,0,-2.69f),
                                    new Vector3(-2.43f,0,-2.68f),
                                    new Vector3(-2.04f,0,-1.78f),
                                    new Vector3(-1.88f,0,-0.31f),
                                    new Vector3(-2.83f,0,1.05f),
                                    new Vector3(-2.89f,0,2.52f),
                                    new Vector3(-2.19f,0,3.72f),
                                    new Vector3(2.84f,0,-1.88f),
                                    new Vector3(2.68f,0,-1.87f),
                                    new Vector3(2.56f,0,-1.93f),
                                    new Vector3(2.56f,0,-2.09f),
                                    new Vector3(2.69f,0,-2.19f),
                                    new Vector3(2.84f,0,-2.09f),
                                    new Vector3(2.89f,0,-1.9f),
                                    new Vector3(4.13f,0,-3.32f),
    };


    private Vector3[] house = new[] {

                                    new Vector3(0.0f,0,-4.0f),
                                    new Vector3(3.0f,0,-4.0f),
                                    new Vector3(3.0f,0,1.0f),
                                    new Vector3(4.0f,0,1.0f),
                                    new Vector3(0.0f,0,4.0f),
                                    new Vector3(-4.0f,0,1.0f),
                                    new Vector3(-3.0f,0,1.0f),
                                    new Vector3(-3.0f,0,-4.0f),
                                    new Vector3(0.0f,0,-4.0f),
                                    new Vector3(0.0f,0,-1.0f),
                                    new Vector3(2.0f,0,-1.0f),
                                    new Vector3(2.0f,0,-4.0f),
                                    new Vector3(-3.0f,0,-4.0f),

    };

    // a bit crap
    private Vector3[] spiral = new[] {


                                    new Vector3(-0.44f,0,-0.32f),
                                    new Vector3(0.32f,0,-0.32f),
                                    new Vector3(0.32f,0,0.44f),
                                    new Vector3(-1.2f,0,0.44f),
                                    new Vector3(-1.2f,0,-1.08f),
                                    new Vector3(1.08f,0,-1.08f),
                                    new Vector3(1.08f,0,1.2f),
                                    new Vector3(-1.96f,0,1.2f),
                                    new Vector3(-1.96f,0,-1.84f),
                                    new Vector3(1.84f,0,-1.84f),
                                    new Vector3(1.84f,0,1.96f),
                                    new Vector3(-2.72f,0,1.96f),
                                    new Vector3(-2.72f,0,-2.6f),
                                    new Vector3(2.6f,0,-2.6f),
                                    new Vector3(2.6f,0,1.96f),
                                    new Vector3(2.6f,0,-2.6f),
                                    new Vector3(-2.72f,0,-2.6f),
                                    new Vector3(-2.72f,0,1.96f),
                                    new Vector3(1.84f,0,1.96f),
                                    new Vector3(1.84f,0,-1.84f),
                                    new Vector3(-1.96f,0,-1.84f),
                                    new Vector3(-1.96f,0,1.2f),
                                    new Vector3(1.08f,0,1.2f),
                                    new Vector3(1.08f,0,-1.08f),
                                    new Vector3(-1.2f,0,-1.08f),
                                    new Vector3(-1.2f,0,0.44f),
                                    new Vector3(0.32f,0,0.44f),
                                    new Vector3(0.32f,0,-0.32f),
                                    new Vector3(-0.44f,0,-0.32f),

    };

    private Vector3[] mountain = new[] {

                                    new Vector3(-4.75f,0,-1.5f),
                                    new Vector3(-4.5f,0,-1.25f),
                                    new Vector3(-4.25f,0,-1.25f),
                                    new Vector3(-4.0f,0,-1.25f),
                                    new Vector3(-3.75f,0,-1.0f),
                                    new Vector3(-3.5f,0,-1.0f),
                                    new Vector3(-3.25f,0,-0.75f),
                                    new Vector3(-3.0f,0,-0.75f),
                                    new Vector3(-2.75f,0,-0.5f),
                                    new Vector3(-2.5f,0,-0.5f),
                                    new Vector3(-2.25f,0,-0.25f),
                                    new Vector3(-2.0f,0,-0.0f),
                                    new Vector3(-1.75f,0,-0.0f),
                                    new Vector3(-1.5f,0,0.25f),
                                    new Vector3(-1.25f,0,0.5f),
                                    new Vector3(-1.0f,0,0.5f),
                                    new Vector3(-0.75f,0,0.75f),
                                    new Vector3(-0.5f,0,1.0f),
                                    new Vector3(-0.25f,0,1.25f),
                                    new Vector3(0.0f,0,1.25f),
                                    new Vector3(0.25f,0,1.0f),
                                    new Vector3(0.5f,0,1.0f),
                                    new Vector3(0.75f,0,1.25f),
                                    new Vector3(1.0f,0,1.5f),
                                    new Vector3(1.25f,0,1.75f),
                                    new Vector3(1.5f,0,1.75f),
                                    new Vector3(1.75f,0,2.0f),
                                    new Vector3(2.0f,0,2.25f),
                                    new Vector3(2.25f,0,2.5f),
                                    new Vector3(2.5f,0,2.25f),
                                    new Vector3(2.75f,0,2.0f),
                                    new Vector3(3.0f,0,1.75f),
                                    new Vector3(3.25f,0,1.75f),
                                    new Vector3(3.5f,0,1.5f),
                                    new Vector3(3.75f,0,1.25f),
                                    new Vector3(4.0f,0,1.0f),
                                    new Vector3(4.25f,0,0.75f),
                                    new Vector3(4.5f,0,0.5f),
                                    new Vector3(4.75f,0,0.25f),
                                    new Vector3(4.75f,0,-1.5f),
                                    new Vector3(-4.75f,0,-1.5f),
                                    new Vector3(-4.75f,0,4.5f),
                                    new Vector3(4.75f,0,4.5f),
                                    new Vector3(4.75f,0,-1.5f),
                                    new Vector3(-4.75f,0,-1.5f),

    };


    private Vector3[] bird = new[]
    {
                                    new Vector3(1.75f,0,1.5f),
                                    new Vector3(2.5f,0,1.5f),
                                    new Vector3(1.75f,0,1.25f),
                                    new Vector3(1.5f,0,1.0f),
                                    new Vector3(1.25f,0,0.75f),
                                    new Vector3(0.75f,0,0.75f),
                                    new Vector3(0.25f,0,1.0f),
                                    new Vector3(0.75f,0,0.75f),
                                    new Vector3(1.25f,0,0.75f),
                                    new Vector3(1.25f,0,0.25f),
                                    new Vector3(1.0f,0,-0.75f),
                                    new Vector3(0.5f,0,-1.5f),
                                    new Vector3(0.0f,0,-2.0f),
                                    new Vector3(-0.5f,0,-2.25f),
                                    new Vector3(-0.5f,0,-3.25f),
                                    new Vector3(0.0f,0,-3.5f),
                                    new Vector3(-0.5f,0,-3.25f),
                                    new Vector3(0.0f,0,-3.25f),
                                    new Vector3(-0.5f,0,-3.25f),
                                    new Vector3(-0.5f,0,-3.5f),
                                    new Vector3(-0.5f,0,-3.25f),
                                    new Vector3(-0.5f,0,-2.25f),
                                    new Vector3(-1.25f,0,-2.25f),
                                    new Vector3(-2.0f,0,-2.25f),
                                    new Vector3(-2.75f,0,-2.75f),
                                    new Vector3(-3.5f,0,-3.25f),
                                    new Vector3(-3.25f,0,-3.0f),
                                    new Vector3(-3.75f,0,-3.25f),
                                    new Vector3(-3.25f,0,-2.75f),
                                    new Vector3(-4.0f,0,-3.0f),
                                    new Vector3(-3.5f,0,-2.5f),
                                    new Vector3(-3.0f,0,-2.0f),
                                    new Vector3(-2.75f,0,-1.75f),
                                    new Vector3(-2.5f,0,-1.25f),
                                    new Vector3(-2.25f,0,-0.5f),
                                    new Vector3(-2.0f,0,-0.0f),
                                    new Vector3(-1.5f,0,0.5f),
                                    new Vector3(-1.0f,0,1.0f),
                                    new Vector3(-0.5f,0,1.25f),
                                    new Vector3(-0.25f,0,1.25f),
                                    new Vector3(0.25f,0,1.0f),
                                    new Vector3(-0.25f,0,1.25f),
                                    new Vector3(0.0f,0,1.75f),
                                    new Vector3(0.25f,0,2.0f),
                                    new Vector3(0.75f,0,2.25f),
                                    new Vector3(1.0f,0,2.25f),
                                    new Vector3(1.25f,0,2.25f),
                                    new Vector3(1.5f,0,2.0f),
                                    new Vector3(1.75f,0,1.75f),
                                    new Vector3(1.75f,0,1.5f),
    };

    // this one doesn't work
    private Vector3[] pencil = new[]
    {

            new Vector3(-2.05f,0,2.7f),
            new Vector3(-1.83f,0,2.72f),
            new Vector3(-1.61f,0,2.8f),
            new Vector3(-1.47f,0,2.95f),
            new Vector3(-1.42f,0,3.13f),
            new Vector3(-1.6f,0,3.19f),
            new Vector3(-1.85f,0,3.15f),
            new Vector3(-2.07f,0,3.02f),
            new Vector3(-2.2f,0,2.87f),
            new Vector3(-2.06f,0,2.7f),
            new Vector3(0.86f,0,-1.54f),
            new Vector3(1.79f,0,-2.09f),
            new Vector3(1.5f,0,-1.24f),
            new Vector3(1.37f,0,-1.29f),
            new Vector3(1.2f,0,-1.34f),
            new Vector3(1.07f,0,-1.42f),
            new Vector3(0.96f,0,-1.48f),
            new Vector3(0.87f,0,-1.55f),
            new Vector3(0.96f,0,-1.47f),
            new Vector3(1.08f,0,-1.39f),
            new Vector3(1.2f,0,-1.33f),
            new Vector3(1.41f,0,-1.28f),
            new Vector3(1.5f,0,-1.26f),
            new Vector3(-1.41f,0,3.12f),
            new Vector3(-1.61f,0,3.18f),
            new Vector3(-1.85f,0,3.15f),
            new Vector3(-2.07f,0,3.02f),
            new Vector3(-2.2f,0,2.86f),
    };

    private Vector3[] iceCream = new[]
    {

                                    new Vector3(-2.41f,0,-3.88f),
                                    new Vector3(2.1f,0,-0.57f),
                                    new Vector3(1.29f,0,-0.34f),
                                    new Vector3(0.63f,0,0.02f),
                                    new Vector3(0.08f,0,0.47f),
                                    new Vector3(-0.28f,0,1.07f),
                                    new Vector3(-0.49f,0,1.74f),
                                    new Vector3(-0.14f,0,2.32f),
                                    new Vector3(0.57f,0,2.64f),
                                    new Vector3(1.41f,0,2.58f),
                                    new Vector3(1.8f,0,2.24f),
                                    new Vector3(2.21f,0,1.7f),
                                    new Vector3(2.3f,0,1.32f),
                                    new Vector3(2.27f,0,0.94f),
                                    new Vector3(2.28f,0,0.59f),
                                    new Vector3(2.37f,0,0.47f),
                                    new Vector3(4.45f,0,0.82f),
                                    new Vector3(4.37f,0,1.22f),
                                    new Vector3(4.29f,0,1.66f),
                                    new Vector3(2.31f,0,1.34f),
                                    new Vector3(2.27f,0,0.93f),
                                    new Vector3(2.29f,0,0.59f),
                                    new Vector3(2.37f,0,0.49f),
                                    new Vector3(2.41f,0,0.12f),
                                    new Vector3(2.45f,0,-0.19f),
                                    new Vector3(2.27f,0,-0.49f),
                                    new Vector3(2.1f,0,-0.56f),
                                    new Vector3(-2.42f,0,-3.87f),
                                    new Vector3(-0.52f,0,1.75f),
                                    new Vector3(-2.41f,0,-3.89f),

    };


    private Vector3[] wierdPentagon = new[]
    {


                new Vector3(-4.38f,0,-1.25f),
                new Vector3(-1.57f,0,0.2f),
                new Vector3(4.2f,0,-0.0f),
                new Vector3(2.47f,0,-1.87f),
                new Vector3(-4.4f,0,-1.27f),
                new Vector3(-3.47f,0,-0.76f),
                new Vector3(-3.58f,0,0.31f),
                new Vector3(-2.47f,0,0.87f),
                new Vector3(-2.22f,0,-0.15f),
                new Vector3(-1.58f,0,0.2f),
                new Vector3(4.21f,0,-0.01f),
                new Vector3(3.62f,0,-0.59f),
                new Vector3(3.62f,0,0.41f),
                new Vector3(2.58f,0,-0.5f),
                new Vector3(2.69f,0,-1.61f),
                new Vector3(2.46f,0,-1.86f),
                new Vector3(-0.86f,0,-1.59f),
                new Vector3(0.79f,0,0.12f),
                new Vector3(-1.58f,0,0.2f),
                new Vector3(-4.39f,0,-1.27f),
    };

    private Vector3[] moon = new[]
    {
                                    new Vector3(-1.98f,0,3.49f),
                                    new Vector3(-1.98f,0,3.49f),
                                    new Vector3(-0.67f,0,3.6f),
                                    new Vector3(0.6f,0,3.33f),
                                    new Vector3(1.88f,0,2.47f),
                                    new Vector3(2.55f,0,0.62f),
                                    new Vector3(2.55f,0,-0.58f),
                                    new Vector3(2.05f,0,-1.96f),
                                    new Vector3(0.93f,0,-3.15f),
                                    new Vector3(-0.35f,0,-3.53f),
                                    new Vector3(0.4f,0,-2.49f),
                                    new Vector3(1.06f,0,-1.57f),
                                    new Vector3(1.33f,0,-0.51f),
                                    new Vector3(1.19f,0,0.72f),
                                    new Vector3(0.73f,0,1.7f),
                                    new Vector3(-0.27f,0,2.34f),
                                    new Vector3(-1.43f,0,2.84f),
                                    new Vector3(-2.98f,0,2.71f),
                                    new Vector3(-1.98f,0,3.49f),
    };

    private Vector3[] flower = new[]
    {
                                    new Vector3(0.06f,0,2.7f),
                                    new Vector3(0.25f,0,2.66f),
                                    new Vector3(0.39f,0,2.47f),
                                    new Vector3(0.21f,0,2.29f),
                                    new Vector3(-0.04f,0,2.33f),
                                    new Vector3(-0.24f,0,2.47f),
                                    new Vector3(-0.16f,0,2.66f),
                                    new Vector3(0.05f,0,2.71f),
                                    new Vector3(0.11f,0,2.99f),
                                    new Vector3(0.3f,0,3.23f),
                                    new Vector3(0.67f,0,3.42f),
                                    new Vector3(0.95f,0,3.48f),
                                    new Vector3(0.98f,0,3.16f),
                                    new Vector3(0.68f,0,2.89f),
                                    new Vector3(0.51f,0,2.78f),
                                    new Vector3(0.24f,0,2.65f),
                                    new Vector3(0.49f,0,2.77f),
                                    new Vector3(0.71f,0,2.85f),
                                    new Vector3(1.07f,0,2.87f),
                                    new Vector3(1.31f,0,2.8f),
                                    new Vector3(1.13f,0,2.62f),
                                    new Vector3(0.83f,0,2.54f),
                                    new Vector3(0.37f,0,2.45f),
                                    new Vector3(0.89f,0,2.41f),
                                    new Vector3(1.21f,0,2.28f),
                                    new Vector3(1.32f,0,2.15f),
                                    new Vector3(1.09f,0,2.01f),
                                    new Vector3(0.63f,0,2.05f),
                                    new Vector3(0.21f,0,2.26f),
                                    new Vector3(0.44f,0,1.99f),
                                    new Vector3(0.43f,0,1.59f),
                                    new Vector3(0.3f,0,1.46f),
                                    new Vector3(0.02f,0,1.61f),
                                    new Vector3(-0.1f,0,1.88f),
                                    new Vector3(-0.04f,0,2.33f),
                                    new Vector3(-0.27f,0,1.85f),
                                    new Vector3(-0.63f,0,1.75f),
                                    new Vector3(-0.86f,0,1.75f),
                                    new Vector3(-0.83f,0,2.05f),
                                    new Vector3(-0.59f,0,2.34f),
                                    new Vector3(-0.26f,0,2.46f),
                                    new Vector3(-0.73f,0,2.44f),
                                    new Vector3(-1.14f,0,2.58f),
                                    new Vector3(-1.28f,0,2.85f),
                                    new Vector3(-1.06f,0,2.98f),
                                    new Vector3(-0.43f,0,2.89f),
                                    new Vector3(-0.14f,0,2.66f),
                                    new Vector3(-0.43f,0,2.89f),
                                    new Vector3(-0.83f,0,3.14f),
                                    new Vector3(-0.9f,0,3.41f),
                                    new Vector3(-0.62f,0,3.42f),
                                    new Vector3(-0.4f,0,3.23f),
                                    new Vector3(-0.14f,0,3.19f),
                                    new Vector3(0.05f,0,2.71f),
                                    new Vector3(0.22f,0,2.66f),
                                    new Vector3(0.36f,0,2.47f),
                                    new Vector3(0.24f,0,2.29f),
                                    new Vector3(-0.02f,0,2.31f),
                                    new Vector3(-0.11f,0,1.89f),
                                    new Vector3(-0.25f,0,0.91f),
                                    new Vector3(-0.37f,0,-0.96f),
                                    new Vector3(-0.38f,0,-2.65f),
                                    new Vector3(-0.1f,0,-2.01f),
                                    new Vector3(0.32f,0,-1.57f),
                                    new Vector3(0.79f,0,-1.45f),
                                    new Vector3(0.88f,0,-1.85f),
                                    new Vector3(0.58f,0,-2.36f),
                                    new Vector3(-0.09f,0,-2.71f),
                                    new Vector3(-0.37f,0,-2.67f),
                                    new Vector3(-0.41f,0,-2.65f),
                                    new Vector3(-0.81f,0,-1.83f),
                                    new Vector3(-1.23f,0,-1.45f),
                                    new Vector3(-1.59f,0,-1.37f),
                                    new Vector3(-1.61f,0,-1.69f),
                                    new Vector3(-1.32f,0,-2.23f),
                                    new Vector3(-0.71f,0,-2.67f),
                                    new Vector3(-0.43f,0,-2.67f),
                                    new Vector3(-0.59f,0,-2.29f),
                                    new Vector3(-0.59f,0,-0.9f),
                                    new Vector3(-0.55f,0,0.26f),
                                    new Vector3(-0.37f,0,1.36f),
                                    new Vector3(-0.29f,0,1.85f),
                                    new Vector3(-0.06f,0,2.33f),
                                    new Vector3(-0.27f,0,2.46f),
    };


    private float startingLineWidth;

    private Vector3[] DotToDot;

    private Vector3[][] DotToDots = new Vector3[18][]; // to contain all of the individual dot to dot levels

    public GameObject demoTulip;

    private int PlaceInArray = 0; 

    private Color[] coloursArray = new Color[] { Color.red, Color.cyan, Color.green, Color.blue, Color.yellow };


    public int timerSeconds = 120; // controls how long the game is in seconds

    private LineRenderer line;
    private bool DrawLine = true;

    public GameObject Picture;
    public Material CatImage;
    public Material backgroundImage;

    public static bool DemoDotToDot = true;

    public GameObject[] players;

    private bool endGame = false;

    /// <summary>
    /// puts all of the dot to dots in the same array and sets where the line starts     
    /// </summary>
    void Start()
    {

        
        DotToDots[0] = Cat;
        DotToDots[1] = Star;
        DotToDots[2] = Envelope;
        DotToDots[3] = Cube;
        DotToDots[4] = Pyramid;
        DotToDots[5] = Apple;
        DotToDots[6] = Tree;
        DotToDots[7] = Leaf;
        DotToDots[8] = fish;
        DotToDots[9] = house;
        DotToDots[10] = spiral;
        DotToDots[11] = mountain;
        DotToDots[12] = bird;
        DotToDots[13] = pencil;
        DotToDots[14] = iceCream;
        DotToDots[15] = wierdPentagon;
        DotToDots[16] = moon;
        DotToDots[17] = flower;

        StartCoroutine(StartNewLevel());

        line = gameObject.GetComponent<LineRenderer>();
        line.material = lineMaterial;

        startingLineWidth = line.startWidth;

        if (DemoDotToDot) { players[0].SetActive(true); }
        else { players[1].SetActive(true); StartCoroutine(RecordLocation()); }
    }

    IEnumerator StartNewLevel()
    {
        if(DemoDotToDot)
        {
            textBox.SetActive(true);
            demoTulip.SetActive(true);
            textBox.transform.GetChild(1).GetComponent<Text>().text = "Welcome to the Demo!";
            yield return new WaitForSeconds(5);
            textBox.transform.GetChild(1).GetComponent<Text>().text = "To play, move to the glowing area to draw the pattern";
            yield return new WaitForSeconds(5);
            textBox.transform.GetChild(1).GetComponent<Text>().text = "Keep tracing until the time is up.";
            yield return new WaitForSeconds(5);
            textBox.transform.GetChild(1).GetComponent<Text>().text = "After the demo, you can try yourself.";
            yield return new WaitForSeconds(5);
            textBox.SetActive(false);
        }
        int ran = Random.Range(0, 18);
        DotToDot = DotToDots[ran];
        
        if (ran == 0)
        {
            Renderer rend = Picture.GetComponent<Renderer>();
            rend.material = CatImage; // will put a cat image down if it is the cat level
        }
        else
        {
            Renderer rend = Picture.GetComponent<Renderer>();
            rend.material = backgroundImage;
        }

        yield return new WaitForSeconds(2);

        transform.position = DotToDot[PlaceInArray];
        line.enabled = true;

        line.positionCount = PlaceInArray + 1;
        line.SetPosition(PlaceInArray, DotToDot[PlaceInArray]); // puts the first point of the line down

        yield return new WaitForSeconds(1);

        PlaceInArray += 1;
        yield return new WaitForSeconds(1);

        if (!DemoDotToDot)
        {
            OverScenesManager.PlayLabView = true; // turn the assistance on
        }
        else
        {
            OverScenesManager.PauseLabView = true; // turn the assistance on
        }
        StartCoroutine(Timer()); // starts the timer that will end after two minutes
    }

    void OnTriggerStay(Collider other)
    {

        if ((other.name == "playerDemo" || other.name == "player") && PlaceInArray > 0)
        {


            int i = Random.Range(0, 5);

            lineMaterial.color = coloursArray[i];

            if (PlaceInArray >= DotToDot.Length)
            {
                line.loop = true;
                PlaceInArray = 0;
                DrawLine = false;
                line.startWidth += 0.05f; // makes the line thicker after each time going aroudn the loop
                line.endWidth = line.startWidth;
            }
            if (DrawLine)
            {
                line.positionCount = PlaceInArray + 1;
                line.SetPosition(PlaceInArray, DotToDot[PlaceInArray - 1]);
            }

            transform.position = DotToDot[PlaceInArray];
            PlaceInArray += 1;
        }
    }

    /// <summary>
    /// Will count up the seconds until the game is over, then display messages at the end for how well a player has donex  
    /// </summary>
    /// <returns></returns>
    IEnumerator Timer()
    {
        yield return new WaitForSeconds(1.5f);
        int i = 0;

        while (i <= timerSeconds)
        {
            yield return new WaitForSeconds(1);
            i += 1;
            if(DemoDotToDot)
            {
                i += 4; // this is  a bit of a bad way of doing it, but it does the job doesn't it?!
                // it just makes the demo not take as long compared to the real game. I'm just trying to keep 
                // the people happy, is that a sin?
            }
        }

        endGame = true;

        if (DemoDotToDot)
        {
            textBox.SetActive(true);
            textBox.transform.GetChild(1).GetComponent<Text>().text = "Now time for you to have a go";
            yield return new WaitForSeconds(4);

            demoTulip.SetActive(false);

            players[0].SetActive(false);
            players[1].SetActive(true);
            DemoDotToDot = false;
            textBox.SetActive(false);
            PlaceInArray = 0;
            line.positionCount = 0;
            line.loop = false;
            DrawLine = true;
            line.startWidth = startingLineWidth;
            line.endWidth = startingLineWidth;
            StartCoroutine(StartNewLevel());

        }
        else
        {
       
            textBox.SetActive(true);
            textBox.transform.GetChild(1).GetComponent<Text>().text = "Well done!";
            PointSystem.ScoreValue += 1000;
            yield return new WaitForSeconds(3);
            textBox.transform.GetChild(1).GetComponent<Text>().text = "You completed the dot to dot!";
            textBox.SetActive(false);
            PlaceInArray = 0;
            // goes back to the gardening game
            if (OverScenesManager.gameMode)
            {
                OverScenesManager.gameMode = false;
            }
            else
            {
                OverScenesManager.level += 1;
            }
            OverScenesManager.NextScene = "Gamegardening";
            OverScenesManager.NextPlayerName = "Test player";
            OverScenesManager.NextTargetName = "Target";
            OverScenesManager.LoadOverScene = true;
            Movement.MoveToPostion = new Vector3(2.28f, 10.8f, 10.6f);
            Movement.MoveToRotation = new Vector3(12f, 48f, 0f);
        }

        
    }

    IEnumerator RecordLocation()
    {
        yield return new WaitForSeconds(0.75f);

        OverScenesManager.coordinatesList.Reset();
        OverScenesManager.coordinatesList.SetGameName("DotToDot");
        int i = 0;
        while (!endGame && i < 600)
        {
            OverScenesManager.coordinatesList.AddToList(transform.position.x, transform.position.z,
                                            players[1].transform.position.x, players[1].transform.position.z,
                                            transform.position.x, transform.position.z);
            yield return new WaitForSeconds(0.1f);
            i++;
        }

        OverScenesManager.coordinatesList.WriteToCSV();
    }

}
