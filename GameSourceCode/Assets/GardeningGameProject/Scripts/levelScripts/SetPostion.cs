﻿using UnityEngine;

/// <summary>
/// This <c>setPostion</c> class.
/// Sets default postion for the fishing game
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class SetPostion : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.position = new Vector3(-63.22f,1.21f,351.33f);
	}
}
