﻿using UnityEngine;


/// <summary>
/// This main <c>PlaneAnimation</c> class.
/// Animations for the tools and other objects uses this.
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class PlaneAnimation : MonoBehaviour
{
    // declare transform
    private Transform tr;
    public Vector3 defaultRotation = new Vector3(0, -15, 6);


    private Quaternion previousRotation;

    public bool rotationFlip = false; // this is to change is the rotation is in the X,Z plane or the X,Y Plane
    private Vector3 prevPos;

    public float turnSpeed = 10f;

    private Vector3 relativePos;
    private Quaternion rotation;
    private Renderer myRenderer;
    public bool drawGame = false;

    /// <summary>
    ///  Use this for initialization
    /// </summary>
    void Start()
    {
        // get transform, as we will need to set the position from labview
        tr = transform;

        prevPos = tr.position;
        if (tr.GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetComponent<Renderer>();
        }
        else if (tr.GetChild(0).GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetChild(0).GetComponent<Renderer>();
        }
        else if (tr.GetChild(1).GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetChild(1).GetComponent<Renderer>();
        }
    }


    /// <summary>
    ///  Update is called once per frame
    ///  Called by a fixed time to ensure safe aniamations
    /// </summary>
    void FixedUpdate()
    {
        if (myRenderer.isVisible || drawGame)
        {

            // controlling the plane of rotation, so it works on side to side ones or up and down ones
            if (rotationFlip)
            {
                relativePos = -new Vector3(tr.position.x - prevPos.x, 0, tr.position.z - prevPos.z);
            }
            else
            {
                relativePos = -new Vector3(tr.position.x - prevPos.x, tr.position.y - prevPos.y, 0);
            }


            if (relativePos != Vector3.zero)
            {
                rotation = Quaternion.LookRotation(relativePos);
            }
            else
            {

                if (rotationFlip)
                {
                    rotation = previousRotation;
                }
                else
                {
                    rotation = Quaternion.Euler(new Vector3(defaultRotation.x, defaultRotation.y, 0));
                }

            }

            tr.rotation = Quaternion.Slerp(tr.rotation, rotation, Time.fixedDeltaTime * turnSpeed);
            previousRotation = rotation;
            prevPos = tr.position;
        }
    }
}
