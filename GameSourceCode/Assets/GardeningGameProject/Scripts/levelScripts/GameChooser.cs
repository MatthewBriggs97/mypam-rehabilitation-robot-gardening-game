﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This <c>gameChooser</c> class.
/// This script is not used any more, but works. It gives the user a chose to play what game they want. 
/// Acts like a bounce between. Like ping pong.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class GameChooser : MonoBehaviour
{

    /// <summary>
    ///  blue orb location
    /// change level value to go to
    /// is it on or not
    /// </summary>
    private Vector3 initialCameraLocation = new Vector3(-152.88f, 133.2f, 119.71f);
    private Vector3 initialCameraRotation = new Vector3(49.16f, 103.56f, 0.001f);
    public Vector3[] indicatorLocations; // this will be the position that the blue orb travels to to highlight where the player will be going
    public int[] relativeLevels; // this will be a list of where every location relates to in terms of 'NextLevel'
    public string[] levelNames; // this will display in the right hand side of the game and say what each level is
    public GameObject player;
    public static bool started = false;
    public float delayBetweenChanges = 2f;
    private int levelChosen = 1; // default value, used to pick what level the player goes to next
    public GameObject textToDisplay;

    /// <summary>
    /// Checks if its been enabled
    /// </summary>
    void Update()
    {

        if (started)
        {
            // moves the camera and starts a co routine to display all the levels the player can choose to play
            Movement.MoveToPostion = initialCameraLocation;
            Movement.MoveToRotation = initialCameraRotation;
            MoveLogic.inDemo = true;
            textToDisplay.SetActive(true);

            StartCoroutine(DisplayLevelChoices());
            started = false;
        }
    }

    /// <summary>
    /// Shows the user al the choices
    /// </summary>
    private IEnumerator DisplayLevelChoices()
    {
        while (MoveLogic.inDemo == true)
        {

            for (int i = 0; i < indicatorLocations.Length; i++)
            {
                // moves the main halo to hover over the area they will advance to
                player.transform.position = indicatorLocations[i];
                textToDisplay.transform.GetChild(1).GetComponent<Text>().text = levelNames[i];
                yield return new WaitForSeconds(delayBetweenChanges);

                if (MoveLogic.inDemo == false)
                {
                    levelChosen = i;
                    break;
                }
            }
        }
        EndGame(levelChosen);
    }

    /// <summary>
    /// plays the leve chosen
    /// </summary>
    /// <param name="levelChosen">The level they wanna play</param>
    void EndGame(int levelChosen)
    {
        LevelChoice.NextLevel = relativeLevels[levelChosen];
        
        textToDisplay.SetActive(false);
    }
}
