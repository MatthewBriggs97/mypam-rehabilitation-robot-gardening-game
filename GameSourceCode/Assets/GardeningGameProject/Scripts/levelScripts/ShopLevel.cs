﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This <c>ShopLevel</c> class.
/// This is the script that controls the user progression
/// this, like many others can just be called with 
/// ShopLevel.started = true;
/// this will start the game, it moves the camera the storage building
/// view, and then unlocks the 'next' item
/// controlled by the 'unlockableItems' list
/// to add a new item, place it in the scene, disable it,
/// and add it as a child of shopLevel;
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class ShopLevel : MonoBehaviour
{

    public GameObject finalDispalyText;
    public static bool started = false;

    private int howManyThingsYouHaveUnocked = 0;  

    public GameObject mainCamera;
    public Transform explosionPosition;

    public ParticleSystem playerExplosion;


    private string[] unlockableItems = new string[] { "A Brand New Shed", "A Comfortable Bench", "A Brand New Shed",
                                                    "Another Shed", "Some Nice Decking", "Some Nice Flowers",
                                                    "Some More Flowers", "Some Cactuses"};


 

    private float maxSizeX = 1;
    private float maxSizeY = 1;
    private float maxSizeZ = 1; // this is used because some items have un uniform sclaes, 
                                // so the max size is set depending on the item

    // Use this for initialization
    void Start()
    {
        howManyThingsYouHaveUnocked =  PlayerPrefs.GetInt("howManyThingsYouHaveUnocked", 0);
        for (int i = 0; i < howManyThingsYouHaveUnocked; i++)
        {
            gameObject.transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// checks when started has been turned to true and start the coroutine
    /// </summary>
    void Update()
    {
        if (started && howManyThingsYouHaveUnocked < 8) // you will have to increase this 8 if you want to have more items to unlock
        {
            Movement.MoveToPostion = new Vector3(-146.28f, 12.172f, 105.14f); // just a hard set position, just somewhere where the camera looks nice
            Movement.MoveToRotation = new Vector3(4.985f, 55.52f, 0f);
            started = false;
            StartCoroutine(UnlockNewItem()); // co routines are just used to allow for time delays to be used
        }
    }


    
    /// <summary>
    /// goes to storage area,
    /// says player is doing well
    /// then goes to where the item is
    /// small explosion
    /// followed by it growing in scale
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator UnlockNewItem()
    {
        yield return new WaitForSeconds(1f);

        
        // this section is just a small animation that plays 
        // it will slowly pan around the storage area then move to
        // where the specific item is placed
        Movement.MoveToPostion = new Vector3(-148.1f, 10.34f, 112.4f);
        Movement.MoveToRotation = new Vector3(2.235f, 61.6f, 0f);
        yield return new WaitForSeconds(5f);
        finalDispalyText.SetActive(true);
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "Welcome to the shop!";
        Movement.MoveToPostion = new Vector3(-112.2f, 10.3f, 87.31f);
        Movement.MoveToRotation = new Vector3(4.1f, 21.5f, 0f);
        yield return new WaitForSeconds(5f);
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "You're doing so well";
        Movement.MoveToPostion = new Vector3(-69.3f, 20.1f, 92.0f);
        Movement.MoveToRotation = new Vector3(6.88f, -23.3f, 0f);
        
        yield return new WaitForSeconds(5f);
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "You've unlocked " + unlockableItems[howManyThingsYouHaveUnocked];

        Movement.MoveToPostion = new Vector3(gameObject.transform.GetChild(howManyThingsYouHaveUnocked).transform.position.x,
                                             gameObject.transform.GetChild(howManyThingsYouHaveUnocked).transform.position.y + 5f,
                                             gameObject.transform.GetChild(howManyThingsYouHaveUnocked).transform.position.z - 7f); // the 5 and 7 are just so the camera is a bit away from the obecject

        Vector3 relativePos;

        //explosionPosition.transform = new Vector3(3f,3f,3f);

        relativePos = gameObject.transform.GetChild(howManyThingsYouHaveUnocked).transform.position - Movement.MoveToPostion;
        Quaternion rotation = Quaternion.LookRotation(relativePos);
        Movement.MoveToRotation = rotation.eulerAngles;
        yield return new WaitForSeconds(7f); // wait for the camera to get there
        // do an explosion
        explosionPosition.transform.position = gameObject.transform.GetChild(howManyThingsYouHaveUnocked).transform.position;
        playerExplosion.Play(true);

        Transform temp = gameObject.transform.GetChild(howManyThingsYouHaveUnocked).transform;

        temp.gameObject.SetActive(true); // then the item appears

        maxSizeX = temp.localScale.x;
        maxSizeY = temp.localScale.y;
        maxSizeZ = temp.localScale.z;

        temp.localScale = new Vector3(0f, 0f, 0f);

        for (int i = 0; i < 100; i++) // 100 just seemed enough to make the animation smooooth
        {

            temp.localScale += new Vector3(maxSizeX / 100, maxSizeY / 100, maxSizeZ / 100);
            yield return new WaitForSeconds(0.02f);

        }

        for(int i = 0; i < howManyThingsYouHaveUnocked; i++)
        {
            gameObject.transform.GetChild(howManyThingsYouHaveUnocked).gameObject.SetActive(true);
        }

        finalDispalyText.SetActive(false);
        yield return new WaitForSeconds(2f); // wait for the camera to get there
                                             // overScenesManager.StopLabView = true;

        howManyThingsYouHaveUnocked += 1; // so that the next thing you unlock is new
        OverScenesManager.level += 1;

        PlayerPrefs.SetInt("howManyThingsYouHaveUnocked", howManyThingsYouHaveUnocked);
}

}
