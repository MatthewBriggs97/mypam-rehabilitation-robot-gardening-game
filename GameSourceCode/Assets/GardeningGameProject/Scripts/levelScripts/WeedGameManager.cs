﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This <c>weedGameManager</c> class.
/// Is in full control over the deweeding level. Holds all methods in charge of stopping and starting the level.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class WeedGameManager : MonoBehaviour
{
    /// <summary>
    /// The HUD for welcoming the user
    /// </summary>
    public GameObject IntroHUD;

    /// <summary>
    /// when set to trrue start the level
    /// </summary>
    public static bool StartOfDeWeed = false;

    /// <summary>
    /// When this is true it show th end of the level
    /// </summary>
    public static bool EndOfDeWeed = false;

    /// <summary>
    /// The whole crops/weeds
    /// </summary>
    private GameObject crops;

    public GameObject Target;

    public GameObject LastWeed;
    private bool Begin = true;

    /// <summary>
    /// The time that is spent on this level
    /// </summary>
    public int TimeOfLevel = 60;

 //   public GameObject helper;

    /// <summary>
    /// Called every frame, checks if the game has started or ended
    /// </summary>
    void Update()
    {
        if (StartOfDeWeed)
        {
            if (Begin)
            {
                StartCoroutine(SetUp());
                Begin = false;
            }
            else if (EndOfDeWeed)
            {
                StartCoroutine(OutTro());
                EndOfDeWeed = false;
            }
        }
    }

    /// <summary>
    /// The setup of the dewweding level, it will enable all needed colliders and renders.
    /// </summary>
    IEnumerator SetUp()
    {
        crops = LastWeed.transform.parent.gameObject;

        for (int i = 0; i < 5; i++)
        {
            crops.transform.GetChild(i).GetComponent<WeedGrowth>().growFactor = 0.1f;//grows all weed fully
        }

        transform.GetComponent<BoxCollider>().enabled = false;//off for the demoscene

        transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);
        //its mesh parts
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);


    //

        //lets player now play the game
        transform.GetComponent<BoxCollider>().enabled = true;

        //setting up the can
        transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), true, null);
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);
        Target.GetComponent<TargetMovement>().UseDefaultPosition = true;
        
        Transform temp = LastWeed.transform.parent.GetChild(0);

        TargetMovement.TargetMoveto = temp.position;

        temp.GetComponent<MeshCollider>().enabled = true;
        temp.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(temp.GetComponent("Halo"), true, null);

        yield return new WaitForSeconds(0.5f);

        transform.GetComponent<BoxCollider>().enabled = true;

        //
        // SET UP COMPLETE
        //
        StartCoroutine(Timer());//time for the game to finish 
    }

    /// <summary>
    /// Called at the start of the level, is used to end the game after a chosen time.
    /// </summary>
    IEnumerator Timer()
    {
        yield return new WaitForSeconds(0.5f);
        int i = 0;

        while (i <= TimeOfLevel)
        {
            yield return new WaitForSeconds(1);
            i++;
        }

        LastWeed.GetComponent<NextWeed>().NextObject = Target;
    }

    /// <summary>
    /// Called when the game finishes to tell the player welldone and goes to next level
    /// </summary>
    IEnumerator OutTro()
    {
        transform.GetComponent<BoxCollider>().enabled = false;//stop player from playing

        //makes the can have no halo
        transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);

        for (int j = 0; j < 5; j++)
        {//used to shrink all the weeds
            crops.transform.GetChild(j).GetComponent<WeedGrowth>().shrinkingAll = true;
            yield return new WaitForSeconds(1.5f);//wait between each
        }
        yield return new WaitForSeconds(2.5f);

        IntroHUD.SetActive(true);
        OverScenesManager.PauseLabView = true;//stop the assist from moving

        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "Well Done, you killed all the weeds!";

        yield return new WaitForSeconds(4);
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "";
        
        IntroHUD.SetActive(false);

        OverScenesManager.level += 1;//lets the game pllay next level
        yield return new WaitForSeconds(1);

        StartOfDeWeed = false;
        Begin = true;
    }
}
