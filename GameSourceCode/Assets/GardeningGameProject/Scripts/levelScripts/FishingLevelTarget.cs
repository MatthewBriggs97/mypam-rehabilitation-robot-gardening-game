﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This <c>fishingLevelTarget</c> class.
/// The script fully incharge of the fishing game.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class FishingLevelTarget : MonoBehaviour
{
    public GameObject AccualPlayer;
    /// <summary>
    /// The target of where the assist will be drawn towards.
    /// </summary>
    public GameObject Target;

    /// <summary>
    /// The rod to be animated to make it look like a fish was caught.
    /// </summary>
    public GameObject rod;

    /// <summary>
    /// The location on the map where the game is located, to be set as an offset.
    /// </summary>
    private Vector3 StartingPostion = new Vector3(-62.32f, 1.63f, 349.68f);

    private Vector3 Place = new Vector3(-62.32f, 1.56f, 345.95f);

    /// <summary>
    /// When set to true will start the fishing game.
    /// </summary>
    public static bool fishingLevel = false;

    /// <summary>
    /// Sped of the movement of the fish.
    /// </summary>
    public float Speed = 2f;

    /// <summary>
    /// When the fish is moving this will be set the true.
    /// </summary>
    private bool FishingMovementCalled = false;

    /// <summary>
    /// The place of the camera where to be set the start the game.
    /// </summary>
    private Vector3 CamPlace = new Vector3(-62.77771f, 9.5f, 346.8f);

    /// <summary>
    /// Rotaion of the camera.
    /// </summary>
    private Quaternion Rotation = Quaternion.Euler(74.8f, 0, 0);

    /// <summary>
    /// The location of where the rod if located.
    /// </summary>
    private Vector3 RodPlace;

    /// <summary>
    /// When in the animation this will be set to true.
    /// </summary>
    private bool InAnimation = false;

    /// <summary>
    /// Gets set to false when not to move the fish in update.
    /// </summary>
    private bool MoveFish = true;

    /// <summary>
    /// The starting postion of the rod.
    /// </summary>
    private Vector3 RodStartingPos;

    /// <summary>
    /// The starting rotation of the rod.
    /// </summary>
    private Quaternion RodStartingRota;

    /// <summary>
    /// To write text on to welcome the user.
    /// </summary>
    public GameObject IntroHUD;

    /// <summary>
    /// change to change the length of the game in minutes.
    /// </summary>
    public float LengthMin = 1;

    /// <summary>
    /// Counts the ammount of fish that have been cuaght.
    /// </summary>
    private int CaughtFish = 0;

    /// <summary>
    /// Holds the fishinggames high score.
    /// </summary>
    private int fishingGameHighScore = 0;

    /// <summary>
    /// When set to true starts the fishing game demo.
    /// </summary>
    public static bool DemoFishing = false;

    /// <summary>
    /// the end of the fishing rod, to be used for the fish been hooked upto it.
    /// </summary>
    private GameObject FishHook;

    /// <summary>
    /// The demo mascot.
    /// </summary>
    public GameObject helper;

    /// <summary>
    /// The timer for when the game should end.
    /// </summary>
    public int timerSeconds = 60;

    /// <summary>
    /// This is true when the play/rod aim is over the fish.
    /// </summary>
    private bool touchingFish = true;

    /// <summary>
    /// When the fish is been caught this is set to true.
    /// </summary>
    private bool catchingFish = false;

    public GameObject water;
    public GameObject lowPolyWater;
    private bool endGame = false;

    /// <summary>
    /// Called on start up, finds the rods aim,
    /// the play, also find the high score and starting position.
    /// </summary>
    void Start()
    {
        FishHook = transform.parent.Find("rodAim").gameObject;

        fishingGameHighScore = PlayerPrefs.GetInt("fishingGameHighScore", 0);

        RodStartingPos = rod.transform.position;
        RodStartingRota = rod.transform.rotation;
    }

    /// <summary>
    /// Called everyframe, checks where its in the demo or playing the game
    /// </summary>
    void Update()
    {
        if (fishingLevel)
        {//checking where its in the fishing game
            if (!FishingMovementCalled)
            {//called once, just gets things together
                lowPolyWater.SetActive(false);
                water.SetActive(true);

                StartCoroutine(Intro());
                FishingMovementCalled = true;

                StartCoroutine(Timer());//starting the timer
            }
            else
            {
                if (Vector3.Distance(transform.position, Place) > 0.1f && MoveFish == true && touchingFish)
                {//checking the distance of the target to the new place, and where it should be moved
                    float Step = Speed * Time.deltaTime;//speed of steps

                    transform.transform.position = Vector3.MoveTowards(transform.transform.position, Place, Step);
                }
                if (InAnimation == false || catchingFish)
                {//setting the offset of the target, so that it works with the LabVIEW workspace
                    Target.transform.position = transform.position - StartingPostion;
                }
                else if (InAnimation)
                {//setting the postion when not needing to catch a fish
                    Target.transform.position = new Vector3(0, 0, -3);
                }
            }
        }
        else if (DemoFishing == true)
        {//all same as above but for the fishing demo
            if (FishingMovementCalled == false)
            {
                helper.SetActive(true);
                LevelSupport.Level = 6;//makes the right demo tile appear
                StartCoroutine(IntroDemo());
                FishingMovementCalled = true;

                StartCoroutine(Timer());
            }
            else if (Vector3.Distance(transform.position, Place) > 0.1f && MoveFish == true)
            {
                float Step = Speed * Time.deltaTime;

                transform.transform.position = Vector3.MoveTowards(transform.transform.position, Place, Step);
            }

            if (FishingMovementCalled && Vector3.Distance(FishHook.transform.position, transform.position) > 0.1f && !InAnimation)
            {
                float Step = Time.deltaTime * 0.75f;

                FishHook.transform.position = Vector3.MoveTowards(new Vector3(FishHook.transform.position.x, transform.position.y, FishHook.transform.position.z),
                                                                  transform.position, Step);
            }
        }
    }

    /// <summary>
    /// When the user/fish aim is over the fish this is called
    /// </summary>
    /// <param name="other">the aim rod/player</param>
    void OnTriggerStay(Collider other)
    {
        if (other.name == "rodAim" && FishingMovementCalled == true && InAnimation == false)
        {//if the fishing movement is true and itsnt not in the rodanimation this will be called
            touchingFish = true;//its touching the fish
            StartCoroutine(RodAnimation());//the animation for moving the rod is called
        }
    }

    /// <summary>
    /// when the user entres the fish/target it will run this
    /// </summary>
    /// <param name="other">the aim rod/player</param>
    void OnTriggerEnter(Collider other)
    {
        if (DemoFishing)
        {//if demo state is true
            touchingFish = true;
            FishHook.transform.position = StartingPostion;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="other">the aim rod/player</param>
    void OnTriggerExit(Collider other)
    {
        if (other.name == "rodAim")
        {
            touchingFish = false;
        }
        if (DemoFishing)
        {
            touchingFish = false;
        }
    }

    /// <summary>
    /// The method called for the introduction of the fishing game
    /// </summary>
    IEnumerator Intro()
    {
        InAnimation = true;
        IntroHUD.SetActive(true);
        CaughtFish = 0;//resets the amount of fish
        //Moves the camera around the fishing area
        Movement.MoveToPostion = new Vector3(-82f, 21.7f, 396.2f);
        Movement.MoveToRotation = new Vector3(13.6f, 145f, 0f);

        //diffiernt texts to show the user 
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "Welcome to the fishing game";
        yield return new WaitForSeconds(3f);
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "Have fun fishing on the lake!";
        yield return new WaitForSeconds(3f);
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "The smaller the fish the more its worth!";
        yield return new WaitForSeconds(3f);
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "Move the arm over the fish to catch it";
        yield return new WaitForSeconds(3f);
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = ("Your current hi score is " + fishingGameHighScore);

        transform.transform.position = new Vector3(-62.32f, 1.56f, 345.95f);

        Movement.MoveToPostion = new Vector3(-110f, 18.5f, 335.5f);
        Movement.MoveToRotation = new Vector3(15.643f, 79.6f, 0f);

        yield return new WaitForSeconds(4);

        Movement.MoveToPostion = CamPlace;//final camera postion and rotation
        Movement.MoveToRotation = Quaternion.Euler(74.8f, 0, 0).eulerAngles;

        IntroHUD.SetActive(false);
        OverScenesManager.PlayLabView = true;//start assist
        InAnimation = false;
    }

    /// <summary>
    /// The main animation for the rod when catching a fish and staying on the fish
    /// </summary>
    /// <returns></returns>
    IEnumerator RodAnimation()
    {
        InAnimation = true;
        catchingFish = true;

        //calulating the score by fish size, as smaller is harder
        float WhightOfFish = Mathf.RoundToInt(transform.localScale.magnitude * 10);

        if (Mathf.Abs((77 - (WhightOfFish * 2))) <= 16)
        {//anything too big breaks the game, hardsettng bigger fishes weight
            WhightOfFish = 29;
        }


        /*
         * 
         * START GOT FISH
         * 
         * Full of animations moving the rod, fish and assist, as well as rotations. most in while loops
         */

        //moving rod
        Rotation = Quaternion.Euler(-18.68f, -79.35f, 0);
        RodPlace = new Vector3(-55.03861f, 4.502608f, 349.2736f);

        while (Vector3.Distance(rod.transform.position, RodPlace) > 0.05f || (rod.transform.eulerAngles.x - Rotation.eulerAngles.x) > 0.05f)
        {
            float step2 = 2 * 42 * Time.deltaTime;
            float step = 2 * 8 * Time.deltaTime;

            rod.transform.position = Vector3.MoveTowards(rod.transform.position, RodPlace, step);
            rod.transform.eulerAngles = Quaternion.RotateTowards(rod.transform.rotation, Quaternion.Euler(Rotation.eulerAngles), step2).eulerAngles;

            yield return new WaitForSeconds(0.01f);//letting the other scripts run and waits a small amount to animate the rod

            if (!touchingFish)
            {//if the user isnt touching the fish with his/her aim
                RestAnimation();
                yield break;//breaks outof the method
            }
        }
        yield return new WaitForSeconds(0.02f);

        //moving rod
        Rotation = Quaternion.Euler(-24.8f, rod.transform.eulerAngles.y, rod.transform.eulerAngles.z);
        RodPlace = rod.transform.position;

        while ((rod.transform.eulerAngles.x - Rotation.eulerAngles.x) > 0.05f)
        {
            float step = 26 * Time.deltaTime;

            rod.transform.eulerAngles = Quaternion.RotateTowards(rod.transform.rotation, Quaternion.Euler(Rotation.eulerAngles), step).eulerAngles;

            yield return new WaitForSeconds(0.01f);//letting the other scripts run and waits a small amount to animate the rod

            if (!touchingFish)
            {//if the user isnt touching the fish with his/her aim
                RestAnimation();
                yield break;//breaks outof the method
            }
        }

        yield return new WaitForSeconds(0.02f);

        //moving rod
        Rotation = Quaternion.Euler(-5, rod.transform.eulerAngles.y, rod.transform.eulerAngles.z);
        RodPlace = rod.transform.position;

        while ((Rotation.eulerAngles.x - rod.transform.eulerAngles.x) > 0.05f)
        {
            float step = 2 * (77 * Time.deltaTime);

            rod.transform.eulerAngles = Quaternion.RotateTowards(rod.transform.rotation, Quaternion.Euler(Rotation.eulerAngles), step).eulerAngles;

            yield return new WaitForSeconds(0.01f);//letting the other scripts run and waits a small amount to animate the rod

            if (!touchingFish)
            {//if the user isnt touching the fish with his/her aim
                RestAnimation();
                yield break;//breaks outof the method
            }
        }

        Place = new Vector3(rod.transform.GetChild(1).position.x, Place.y, rod.transform.GetChild(1).position.z);

        if (!touchingFish)
        {//if the user isnt touching the fish with his/her aim
            RestAnimation();
            yield break;//breaks outof the method
        }

        yield return new WaitForSeconds(3);


        if (!touchingFish)
        {//if the user isnt touching the fish with his/her aim
            RestAnimation();
            yield break;//breaks outof the method
        }

        MoveFish = false;
        catchingFish = false;
        transform.GetComponent<BoxCollider>().enabled = false;

        //makinig the score,big fish worth less
        if (DemoFishing == false)
        {
            PointSystem.ScoreValue += 150 - (Mathf.RoundToInt(transform.localScale.magnitude * 20));

            CaughtFish += 1;
        }

        /*
         * 
         * END GOT FISH
         * 
         */
        //sorting fish

        transform.GetComponent<Animation>().enabled = false;

        transform.GetChild(0).GetComponent<Animator>().speed = 8;

        transform.parent = rod.transform.GetChild(1);
        //moving rod
        Rotation = Quaternion.Euler(-24.8f, rod.transform.eulerAngles.y, rod.transform.eulerAngles.z);
        transform.rotation = Quaternion.Euler(0, 0, -90);
        while ((rod.transform.eulerAngles.x - Rotation.eulerAngles.x) > 0.05f)
        {
            float step = Mathf.Abs((77 - (WhightOfFish * 2))) * Time.deltaTime;//weight of fish to slow animation

            rod.transform.eulerAngles = Quaternion.RotateTowards(rod.transform.rotation, Quaternion.Euler(Rotation.eulerAngles), step).eulerAngles;

            yield return new WaitForSeconds(0.01f);//letting the other scripts run and waits a small amount to animate the rod
        }

        yield return new WaitForSeconds(1);

        Rotation = Quaternion.Euler(rod.transform.eulerAngles.x, -134, rod.transform.eulerAngles.z);

        //moving rod
        while ((rod.transform.eulerAngles.y - Rotation.eulerAngles.y) > 0.05f)
        {
            float step = Mathf.Abs((85 - (WhightOfFish * 2))) * Time.deltaTime;//weight of fish to slow animation

            rod.transform.eulerAngles = Quaternion.RotateTowards(rod.transform.rotation, Quaternion.Euler(Rotation.eulerAngles), step).eulerAngles;

            yield return new WaitForSeconds(0.01f);//letting the other scripts run and waits a small amount to animate the rod
        }

        yield return new WaitForSeconds(2);

        //ReSettings fish and rod defualt posistions

        transform.parent = rod.transform.parent.parent;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        transform.position = new Vector3(-62.32f, 1.56f, 359.85f);

        rod.transform.position = RodStartingPos;
        rod.transform.rotation = RodStartingRota;

        transform.GetChild(0).GetComponent<Animator>().speed = 1;//stops fish from freaking out
        transform.GetComponent<Animation>().enabled = true;

        Place = new Vector3(Random.Range(-67.76f, -58.72f), 1.56f, Random.Range(347.016f, 354.5f));
        MoveFish = true;
        transform.localScale = new Vector3(Random.Range(0.5f, 3), Random.Range(0.5f, 3), Random.Range(0.5f, 3));


        yield return new WaitForSeconds(3.5f);
        transform.GetComponent<BoxCollider>().enabled = true;
        InAnimation = false;
    }

    /// <summary>
    /// This is used for the length of the game played.
    /// </summary>
    IEnumerator Timer()
    {
        yield return new WaitForSeconds(1.5f);
        int i = 0;
        int backUp = 125;
        if (DemoFishing)
        {
            backUp = timerSeconds;
            timerSeconds = timerSeconds / 4;
        }
        else
        {
            StartCoroutine(RecordLocation());
        }
        while (i <= timerSeconds)
        {
            yield return new WaitForSeconds(1);
            i += 1;
        }
        endGame = true;
        if (DemoFishing)
        {
            timerSeconds = backUp; // so it's normal again for the proper game
        }
        RestingFish();
        //if demo or not
        if (DemoFishing) { StartCoroutine(OutTroDemo()); }
        else { StartCoroutine(OutTro()); }
    }

    /// <summary>
    /// Resets the fish for use when replaying the game
    /// </summary>
    void RestingFish()
    {
        //ReSettings fish and rod

        InAnimation = true;

        transform.parent = rod.transform.parent.parent;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        transform.position = StartingPostion;

        rod.transform.position = RodStartingPos;
        rod.transform.rotation = RodStartingRota;

        transform.GetChild(0).GetComponent<Animator>().speed = 1;
        transform.GetComponent<Animation>().enabled = true;

        transform.localScale = new Vector3(Random.Range(0.5f, 3), Random.Range(0.5f, 3), Random.Range(0.5f, 3));

    }

    /// <summary>
    /// Resets the animation of the rod.
    /// </summary>
    void RestAnimation()
    {
        InAnimation = false;
        rod.transform.position = RodStartingPos;
        rod.transform.rotation = RodStartingRota;

        Place = new Vector3(Random.Range(-67.76f, -58.72f), 1.56f, Random.Range(347.016f, 354.5f));
        MoveFish = true;

        InAnimation = false;
    }

    /// <summary>
    /// The outtro of the fishing game, saying weldone and such
    /// </summary>
    IEnumerator OutTro()
    {
        fishingLevel = false;//closing the game bool
        InAnimation = true;

        IntroHUD.SetActive(true);
        //displlays the score
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "Well Done, you Caught: " + CaughtFish + " Fish";
        yield return new WaitForSeconds(3f);
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "";

        if (CaughtFish > PlayerPrefs.GetInt("fishingGameHighScore", 0))
        {//checks high score
            PlayerPrefs.SetInt("fishingGameHighScore", CaughtFish);
        }

        IntroHUD.SetActive(false);

        OverScenesManager.PauseLabView = true;
        if (OverScenesManager.gameMode)
        {
            OverScenesManager.endGameMode = true;
        }
        else
        {
            OverScenesManager.level += 1;
        }

        yield return new WaitForSeconds(1);

        FishingMovementCalled = false;
        water.SetActive(false);
        lowPolyWater.SetActive(true);
    }

    /// <summary>
    /// same as the normal one but for the demo scene
    /// </summary>
    IEnumerator IntroDemo()
    {
        InAnimation = true;
        IntroHUD.SetActive(true);

        CaughtFish = 0;

        Movement.MoveToPostion = new Vector3(-82f, 21.7f, 396.2f);
        Movement.MoveToRotation = new Vector3(13.6f, 145f, 0f);
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "Welcome to the fishing game demo";
        yield return new WaitForSeconds(3f);

        transform.transform.position = new Vector3(-62.32f, 1.56f, 345.95f);

        Movement.MoveToPostion = new Vector3(-110f, 18.5f, 335.5f);
        Movement.MoveToRotation = new Vector3(15.643f, 79.6f, 0f);

        yield return new WaitForSeconds(4);

        Movement.MoveToPostion = CamPlace;
        Movement.MoveToRotation = Rotation.eulerAngles;
        IntroHUD.SetActive(false);
        InAnimation = false;

        StartCoroutine(RodAnimation());
    }

    /// <summary>
    /// same as the normal one but differnt text for demo
    /// </summary>
    IEnumerator OutTroDemo()
    {

        helper.SetActive(false);
        InAnimation = true;

        IntroHUD.SetActive(true);

        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "Well Done, you Caught " + CaughtFish + " Fish";
        yield return new WaitForSeconds(4);
        IntroHUD.transform.GetChild(1).GetComponent<Text>().text = "";

        CaughtFish = 0;

        IntroHUD.SetActive(false);

        fishingLevel = false;

        if (LevelChoice.NextLevel == 11)
        {
            DemoFishing = false;
        }
        else
        {
            FishingMovementCalled = false;
            fishingLevel = false;
            OverScenesManager.PauseLabView = true;
            DemoFishing = false;

            OverScenesManager.level += 1;
        }
    }


    IEnumerator RecordLocation()
    {
        yield return new WaitForSeconds(0.75f);

        OverScenesManager.coordinatesList.Reset();
        OverScenesManager.coordinatesList.SetGameName("FishingGame");
        int i = 0;
        while (!endGame && i < 600)
        {
            OverScenesManager.coordinatesList.AddToList(Target.transform.position.x, Target.transform.position.z,
                                            AccualPlayer.transform.position.x, AccualPlayer.transform.position.z,
                                            (Place - StartingPostion).x, (Place - StartingPostion).z);
            yield return new WaitForSeconds(0.1f);
            i++;
        }
        OverScenesManager.coordinatesList.WriteToCSV();
    }
}


