﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This <c>WateringLevel</c> class.
/// The main script in charge of the watering game.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class WateringLevel : MonoBehaviour
{
    /// <summary>
    /// The rotating level tree object.
    /// </summary>
    public GameObject LevelThreeMoveMent;

    public GameObject GhostWateringCan;

    private Vector3 DefualtPosition;

    public bool skipLevel = false;

    public float speed = 3;

    /// <summary>
    /// values for the MoveToFunction.
    /// </summary>
    private GameObject ToMoveObject;
    private Vector3 ToMoveVector;

    private GameObject Target;

    private bool LevelThreeStart = false;

    public static bool onlyShowDemo = false;
    private bool hasOnlyShownTheDemo = false;
    private GameObject Player;

   // public GameObject helper;

    /// <summary>
    /// Call at the start up, Finds the target and the defualt posistion of the object.
    /// </summary>
    void Start()
    {
        Target = GameObject.Find("Target");
        DefualtPosition = transform.position; // remember where to put it back
        Player = GameObject.Find("Test player");
    }

    /// <summary>
    /// Called every frame, checking where its ready to start the demo or normal game.
    /// </summary>
    void Update()
    {
        if (LevelThreeStart == true)
        {
            LevelThreeMove(ToMoveObject.transform);
        }
        if (onlyShowDemo == true && hasOnlyShownTheDemo == false)
        {
            StartCoroutine(LevelThree(GhostWateringCan));
            hasOnlyShownTheDemo = true;
        }
    }

    /// <summary>
    /// Setting the target to follow a circular movement.
    /// </summary>
    /// <param name="movement"></param>
    void LevelThreeMove(Transform movement)
    {
        movement.position = LevelThreeMoveMent.transform.GetChild(0).position;
    }

    /// <summary>
    /// When it is hit by the player it will fully start al movements.
    /// </summary>
    /// <param name="other">Other</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Test player")
        {
            if (skipLevel == true)
            {
                MoveLogic.inDemo = true;
                OverScenesManager.level += 1;
            }
            else
            {
                other.GetComponent<BoxCollider>().enabled = false;
                PointSystem.ScoreValue += 5;

                transform.parent = other.transform;
                transform.position = other.transform.position;

                transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);

                StartCoroutine(LevelThree(GhostWateringCan));
            }

        }
    }

    /// <summary>
    /// Uses delays to make the target objects move to the right postions; circular movement.
    /// </summary>
    /// <param name="WateringCan"></param>
    /// <returns>A timed movement for the third level</returns>
    IEnumerator LevelThree(GameObject WateringCan)
    {
        MoveLogic.inDemo = true;
        if(Movement.MoveToPostion != new Vector3(0.3f, 7.34f, -4.5f))
        {
            Movement.MoveToPostion = new Vector3(0.3f, 7.34f, -4.5f);
            Movement.MoveToRotation = new Vector3(64.55f, 0, 0);
        }

        yield return new WaitForSeconds(0.75f);

        if(onlyShowDemo == false && WateringCan.name == "GhostWateringCan")
        {
            StartCoroutine(LevelThree(Target));
        }
        else
        {
            if (WateringCan.name == "GhostWateringCan" && MoveLogic.inDemo)
            {//turns off the halos
                LevelSupport.Level = 3;
                Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), false, null);
                Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), false, null);

                SetActiveObject(false, gameObject);
                LevelSupport.Level = 3;
                WateringCan.SetActive(true);
                OverScenesManager.PauseLabView = true; // turn the assistance on
            }
            else
            {
                LevelSlideSelector.SlideLevel = 3;
                OverScenesManager.PlayLabView = true; // turn the assistance on
                SetActiveObject(true, gameObject);
                LevelSupport.Level = 0;
            }
            ToMoveObject = WateringCan;
            LevelThreeMoveMent.SetActive(true);


            yield return new WaitForSeconds(0.02f);
            LevelThreeStart = true;

            for (int i = 0; i < 15; i++)
            {//how long it will run the level
                if (MoveLogic.inDemo) { yield return new WaitForSeconds(1); }
            }
            if (WateringCan.name == "WateringCanPrefab")
            {
                WateringCan.transform.GetChild(1).gameObject.SetActive(false);

                WateringCan.transform.GetChild(1).GetComponent<ParticleSystem>().Pause();

            }
            if (WateringCan.name == "GhostWateringCan")
            {//ends demo
                WateringCan.SetActive(false);
                Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), true, null);
                Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), true, null);

                if (hasOnlyShownTheDemo == false)
                {//starts the real game
                    StartCoroutine(LevelThree(Target));
                }
                else
                {
                    OverScenesManager.level++;
                }
                LevelSupport.Level = 0;
                onlyShowDemo = false;
                gameObject.transform.GetChild(0).gameObject.SetActive(true);


            }
            else
            {//end the player playing the watering game
                LevelSlideSelector.SlideLevel = 0;
                yield return new WaitForSeconds(2);
                LevelThreeStart = false;
                yield return new WaitForSeconds(0.05f);

                MoveLogic.inDemo = true;
                transform.GetComponent<BoxCollider>().enabled = false;
                Player.GetComponent<BoxCollider>().enabled = true;

                transform.parent = GameObject.Find("Tools").transform;
                transform.position = DefualtPosition;

                MoveLogic.inDemo = true;
                gameObject.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(gameObject.GetComponent("Halo"), false, null);
                OverScenesManager.level += 1;//moves to next level
                transform.GetChild(1).GetComponent<ParticleSystem>().Pause();//stops the water animation
                LevelSupport.Level = 0;
                hasOnlyShownTheDemo = false;

                if (WateringCan.name == "WateringCanPrefab")
                {
                    WateringCan.transform.GetChild(1).gameObject.SetActive(false);

                    WateringCan.transform.GetChild(1).GetComponent<ParticleSystem>().Pause();

                }

            }
        }
    
    }

    /// <summary>
    /// sets the choosen object active or unactive.
    /// </summary>
    /// <param name="OffOn"></param>
    /// <param name="Object"></param>
    void SetActiveObject(bool OffOn, GameObject Object)
    {
        Object.GetComponent<BoxCollider>().enabled = OffOn;
        Object.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Object.GetComponent("Halo"), OffOn, null);


        if (OffOn)
        {
            Object.transform.GetChild(0).gameObject.SetActive(OffOn);
            if(Object.name == "WateringCanPrefab")
            {
                Object.transform.GetChild(1).gameObject.SetActive(true);
                Object.transform.GetChild(1).GetComponent<ParticleSystem>().Play();

            }
        }
        else
        {
            

            Object.transform.GetChild(0).gameObject.SetActive(OffOn);
            if (Object.name == "WateringCanPrefab")
            {
                Object.transform.GetChild(1).gameObject.SetActive(false);

                Object.transform.GetChild(1).GetComponent<ParticleSystem>().Pause();

            }
        }
    }
}
