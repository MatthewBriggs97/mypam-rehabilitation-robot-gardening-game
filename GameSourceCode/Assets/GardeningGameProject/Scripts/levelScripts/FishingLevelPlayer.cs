﻿using UnityEngine;

/// <summary>
/// This <c>FishingLevelPlayer</c> class.
/// Makes the target follow the fish with the relevent level is played
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class FishingLevelPlayer : MonoBehaviour
{
    /// <summary>
    /// The main player/the aim of the rod
    /// </summary>
    public GameObject Player;

    /// <summary>
    /// where the main start was of the player
    /// </summary>
    private Vector3 startingpoint;

    /// <summary>
    /// the chosen to run this script
    /// </summary>
    public int level;

    private void Start()
    {
        startingpoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelChoice.NextLevel == level)
        {
            transform.position = startingpoint + new Vector3(Player.transform.position.x, 0, Player.transform.position.z);
        }
    }
}
