﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This <c>WeedGrowth</c> class.
/// This is used to make the weeds grow slowly over time also used to make them shrink.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class WeedGrowth : MonoBehaviour
{
    /// <summary>
    /// The maxium size the weeds will grow, can be changed
    /// </summary>
    public float maxSize;

    /// <summary>
    /// How fast the weeds grow, this is a very small factor 0.1-0.0001, fast to slow
    /// </summary>
    public float growFactor;

    /// <summary>
    /// True when they are growing
    /// </summary>
    private bool Growing = true;

    /// <summary>
    /// true when they are shrinking
    /// </summary>
    private bool Shrinking = false;

    /// <summary>
    /// When true makes all weeds shirnk
    /// </summary>
    public bool shrinkingAll = false;

    /// <summary>
    /// when true makes weeds grow
    /// </summary>
    private bool GrowthOfWeed = false;

    private WaitForSeconds delay;

    void Start()
    {
        delay = new WaitForSeconds(0.02f);
    }

    /// <summary>
    /// Called every frame, checks if they are growning, or if all need to shrink
    /// </summary>
    void Update()
    {
        if (Growing == true && !shrinkingAll && !GrowthOfWeed)
        {
            StartCoroutine(Growth());
            Growing = false;
        }
        if (shrinkingAll)
        {
            Growing = false;
            if (!GrowthOfWeed)
            {
                StartCoroutine(Shrink());
                Shrinking = true;
            }
        }
    }

    /// <summary>
    /// A method to make the crop grow.
    /// </summary>
    /// <returns></returns>
    IEnumerator Growth()
    {
        GrowthOfWeed = true;
        while (maxSize > transform.localScale.y)
        {
            transform.localScale += Vector3.one * Time.deltaTime * growFactor;
            yield return delay;
        }
        GrowthOfWeed = false;
    }

    /// <summary>
    /// When the can hits the weeds makes them shrink
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Deodorant" && Shrinking == false)
        {//checks if they should shirnk and if they aready are
            StartCoroutine(Shrink());
            Shrinking = true;
        }
    }

    /// <summary>
    /// Called When the weed needs to shirnk
    /// </summary>
    IEnumerator Shrink()
    {
        while (0.01 < transform.gameObject.GetComponent<Transform>().localScale.y)
        {
            transform.gameObject.GetComponent<Transform>().localScale -= Vector3.one * Time.deltaTime * growFactor * 2;
            yield return delay;
        }

        Shrinking = false;

        if (shrinkingAll)
        {
            shrinkingAll = false;

            growFactor = 0.0004f;//very slow
        }
        else
        {
            Growing = true;
        }
    }
}
