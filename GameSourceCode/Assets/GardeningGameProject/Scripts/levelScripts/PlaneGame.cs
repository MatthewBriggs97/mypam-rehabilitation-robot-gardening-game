﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This major <c>PlaneGame</c> class.
/// Contains all the methods used for the plane game
/// <list type="bullet">
/// <item>
/// <term>Start</term>
/// <description>Reads the hi score of the previous games</description>
/// </item>
/// <item>
/// <term>Update</term>
/// <description>Moves the camera to the correct starting position, will control the demo version of the plane by moving it to the correct position and contorls the camera so it is behind the plane</description>
/// </item>
/// <item>
/// <term>SpawnRingsAndFly</term>
/// <description>Will spawn a new ring within a random range whenever is passed behind the player by a certain distance, it will do this 30 times. At the end it will save the score and tell the player if they got a new high score</description>
/// </item>
/// <item>
/// <term>OnCollisionEnter</term>
/// <description>Controls when the player gets more points, it will aslo spawn a new ring as soon as the player hits it</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>This class is just for the plane game, it can be called by settings PlaneGame.started = true, it will run through the whole game, then increment the level coutner</para>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
/// 


public class PlaneGame : MonoBehaviour
{

    /// <summary>
    /// by setting started to 'true' the game will start
    /// </summary>
    public static bool started = false;     
    private bool startPlayerMoving = false;
    public Transform cameraLocation;
    /// <summary>
    /// camera control variables, sets the end position of the plane game
    /// </summary>
    public Vector3 cameraStartLocation;
    public Vector3 cameraStartRotation;
    public Vector3 cameraEndLocation;
    public Vector3 cameraEndRotation;
    public GameObject player;

    public GameObject target;
    public float factor;
    public int gameLength = 20; 
    private int numberOfHoopsHit = -1;
    public GameObject finalDispalyText;

    public GameObject helper;

    public GameObject planeTarget;

    private float randomX;
    public static bool inThePlaneGame = false;
    private float randomY;

    private int planeGameHiScore = 0;

    private BasicController basicController;
    private Vector3 offSet;
    public static bool demoPlane = false;
    private bool startedDemo = false;
    private bool endGame = false;

    private void Start()
    {
        ///<summary>
        /// this reads in the hi score so it can tell the player if they beat it, will be updated if they do better
        ///</summary>
        planeGameHiScore = PlayerPrefs.GetInt("planeGameHiScore", 0); 
    }

    void Update()
    {
        if (started)
        {
            Movement.speed = 100;
            Movement.MoveToPostion = cameraStartLocation;
            Movement.MoveToRotation = cameraStartRotation;
            OverScenesManager.PlayLabView = true;
            started = false;
            planeTarget.transform.position = cameraStartLocation;
            StartCoroutine(SpawnRingsAndFly());
        }
        if (startPlayerMoving)
        {
            target.transform.position = new Vector3(-(planeTarget.transform.position.x - cameraLocation.position.x), 1, planeTarget.transform.position.y - cameraLocation.position.y); // starts the first target in a  random position in front of the plane so it can collect it

            transform.position = cameraLocation.position + new Vector3(-player.transform.position.x, player.transform.position.z, -5); // the -5 just makes the camera be behind the plane so everything looks right
        }

        if (demoPlane)
        {
            if (!startedDemo)
            {
                helper.SetActive(true);
                Movement.speed = 100;
                Movement.MoveToPostion = cameraStartLocation;
                Movement.MoveToRotation = cameraStartRotation;
                startedDemo = true;

                StartCoroutine(SpawnRingsAndFly());  // this starts a loop that will spawn a new hoop every time the player hits one or it passes by the plane
            }

            if ((Vector3.Distance(transform.position, planeTarget.transform.position) > 0.1f))
            {
                // this is the game logic for the demo section of the plane, it will just try to move the plane to the correct X,Z coordinates of the target which they have to reach
                float Step = Time.deltaTime * 2.5f;
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(planeTarget.transform.position.x, planeTarget.transform.position.y, transform.position.z), Step);
            }
            // just puts the camera to a position just behind the plane, so the user can see the plane but it does not take up all of the screen
            transform.position = new Vector3(transform.position.x, transform.position.y, cameraLocation.position.z - 5);
        }
    }

    private IEnumerator SpawnRingsAndFly()
    {
        transform.GetChild(3).gameObject.SetActive(true); // makes the plane visible
        yield return new WaitForSeconds(5f); // allows time for the camera to move to the start of the game
        finalDispalyText.SetActive(true); // enables the text that appears on the screen and makes it say "Move the arm to fly into the blue targets"
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "Move the arm to fly into the blue targets ";
        yield return new WaitForSeconds(3f);
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "Your High Score is " + planeGameHiScore.ToString(); // shows the user their high score from the prior game
        yield return new WaitForSeconds(3f);
        finalDispalyText.SetActive(false);
        inThePlaneGame = true; // allows 3 seconds to pass then starts the game up


        if (!demoPlane)
        {
            startPlayerMoving = true; // this makes is so that the user can only control the plane if they are not in the demo

            LevelSupport.Level = 0;
        }
        else
        {
            transform.position = cameraLocation.position; // if they are in the demo won't let the user move

            LevelSupport.Level = 7;
        }

        Movement.MoveToPostion = cameraEndLocation;
        Movement.MoveToRotation = cameraEndRotation;
        Movement.speed = 5; // this starts the camera moving in a straight line towards the end of the stage, it goes slowly so that it has time to go through all of the rings

        for (int i = 0; i < 30; i++) // meaning there are 30 loops to go through
        {
            Movement.speed = 5;
            yield return new WaitForSeconds(5f - (i / 10));

            randomX = Random.Range(-3.2f, 3.2f); // the range is how spread out the rings are 
            randomY = Random.Range(-3.2f, 3.2f);

            planeTarget.transform.position = new Vector3(cameraLocation.position.x + randomX, cameraLocation.position.y + randomY, cameraLocation.position.z - 15f); // the -15 is the distance away from the player the ring spawns

        }
        endGame = true;
        LevelSupport.Level = 0;
        inThePlaneGame = false;
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "You hit " + numberOfHoopsHit.ToString() + " targets";
        finalDispalyText.SetActive(true);
        yield return new WaitForSeconds(3f);

        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "The demo has finished";

        // basically this will either show the user they got a new high score, or tell them what their old one was.
        if (demoPlane == false)
        {
            if (numberOfHoopsHit > planeGameHiScore)
            {
                PlayerPrefs.SetInt("planeGameHiScore", numberOfHoopsHit);
                finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "New High Score, you hit " + numberOfHoopsHit.ToString() + " targets";
            }
            else
            {
                finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "Your High Score is still " + PlayerPrefs.GetInt("planeGameHiScore").ToString() + " targets";
            }
        }

        finalDispalyText.SetActive(true);
        yield return new WaitForSeconds(3f);
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "Thanks for playing";
        yield return new WaitForSeconds(3f);
        finalDispalyText.SetActive(false);
        numberOfHoopsHit = 0;
        startPlayerMoving = false;

        // this bit will automatically put the user onto the real game if they are playing the main game intro
        if (demoPlane)
        {
            if (LevelChoice.NextLevel == 11)
            {
                demoPlane = false;
            }
            else
            {
                demoPlane = false;
                OverScenesManager.level += 1;
                helper.SetActive(false);
            }
            startedDemo = false;
        }
        else if (OverScenesManager.gameMode)
        {
            OverScenesManager.PauseLabView = true;
            OverScenesManager.endGameMode = true;
        }
        else
        {
            OverScenesManager.PauseLabView = true;
            OverScenesManager.level += 1;
        }
        
        


    }

    // whenever the user hits into a ring, it will give the user points (so long as they are not in a demo)
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "planeTarget")
        {
            if(demoPlane == false)
            {
                PointSystem.ScoreValue += 100;
            }
            
            planeTarget.transform.position = new Vector3(cameraLocation.position.x + randomX, cameraLocation.position.y + randomY, cameraLocation.position.z + 15f);
            numberOfHoopsHit++;
        }
    }

    IEnumerator RecordLocation()
    {
        yield return new WaitForSeconds(0.75f);

        OverScenesManager.coordinatesList.Reset();
        OverScenesManager.coordinatesList.SetGameName("FishingGame");
        int i = 0;
        while (!endGame && i < 600)
        {
            OverScenesManager.coordinatesList.AddToList(target.transform.position.x, target.transform.position.z,
                                            player.transform.position.x, player.transform.position.z,
                                            target.transform.position.x, target.transform.position.z);
            yield return new WaitForSeconds(0.1f);
            i++;
        }
        endGame = false;
        OverScenesManager.coordinatesList.WriteToCSV();
    }
}
