﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This major <c>PlantSeedLevel</c> class.
/// Contains all the methods used for the seed sack level
/// this level is for planting the seeds, it has a demo version
/// where the user will watch a ghost version of the seed sack
/// move around hte screen and then another version 
/// where the user plays it themselves 
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
/// 

public class PlantSeedLevel : MonoBehaviour
{
    private int ArrayCounter = 0;

    public GameObject Crops;
    public GameObject GhostSeedSack;

    private int numberOfTimesToGoOverRows = 7; // this is used for how many times the rows of planting spots move up and down

    public bool skipLevel = false; // this bool is so the user can click and skip the dmeo

    public GameObject Tools;

    private Vector3 DefualtPosition;
    private float[] Place = new float[] { 2.25f, +0, -2.25f };
    private Vector3 ToMoveVector;
    private GameObject ToMoveObject;
    private bool ToMoveFunction = false;

    public float speed = 3;

    private GameObject Target;
    private GameObject TargetOne;
    private GameObject TargetTwo;


    private bool PickedUp = false;


    public static bool onlyShowDemo = false;
    private bool hasOnlyShownTheDemo = false;


    // Use this for initialization
    void Start()
    {
        Target = GameObject.Find("Target");
        DefualtPosition = transform.position; // remember where to put it back
        TargetOne = GameObject.Find("TargetOne");
        TargetTwo = GameObject.Find("TargetTwo");
    }

    // Update is called once per frame
    void Update()
    {
        if (ToMoveFunction == true)
        {
            float step = speed * Time.deltaTime;
            ToMove(step);
        }
        if (Objectives.TimeHit == 2 && PickedUp == true)
        {
            StartCoroutine(MoveCrops());
            Objectives.TimeHit = 0;

        }
        if (onlyShowDemo == true && hasOnlyShownTheDemo == false)
        {
            StartCoroutine(LevelTwoDemo(GhostSeedSack));
            hasOnlyShownTheDemo = true;
        }

    }

    /// <summary>
    /// will move an object to a position at a speed 
    /// </summary>
    /// <param name="step">how fast the movement will be </param>
    void ToMove(float step)
    {
        if (ToMoveObject.transform.position != ToMoveVector)
        {
            ToMoveObject.transform.position = Vector3.MoveTowards(ToMoveObject.transform.position, ToMoveVector, step);
        }
        else
        {
            ToMoveFunction = false;
        }

    }

    /// <summary>
    /// determines what to do when the player moves to the seed sack, 
    /// so it will start the game unless the option to skip has been selected from the editor
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Test player")
        {
            if (skipLevel == true)
            {
                OverScenesManager.level++; // increments the level if the user would like to skip by ticking the bool in the editor
            }
            else
            {
                PickedUp = true;
                PointSystem.ScoreValue += 5; 
                transform.parent = other.transform;
                transform.position = other.transform.position;

                transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(other.GetComponent("Halo"), false, null);

                other.GetComponent<BoxCollider>().enabled = false;

                StartCoroutine(ReadyLevelTwo());
            }

        }
    }

    /// <summary>
    /// Enables the correct object, since sometimes the ghost seed back is needed,
    /// but for the user section the normal seed bag is needed
    /// </summary>
    /// <param name="OffOn">Determines if you are setting the item to be inactive, or setting it to be removed from the users control</param>
    /// <param name="Object">The object in question</param>
    void SetActiveObject(bool OffOn, GameObject Object)
    {
        Object.GetComponent<BoxCollider>().enabled = OffOn;
        Object.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Object.GetComponent("Halo"), OffOn, null);
        Object.GetComponent<MeshRenderer>().enabled = OffOn;

        Object.transform.GetChild(0).gameObject.SetActive(OffOn);

        if (OffOn)
        {
            Object.transform.GetChild(0).GetComponent<ParticleSystem>().Play();
        }
        else
        {
            Object.transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
        }
    }

    /// <summary>
    /// Runs the logic for the part of the game where you watch the demo plant the seeds
    /// </summary>
    /// <param name="SeedSack">The game object that is being moved around</param>
    /// <returns></returns>
    IEnumerator LevelTwoDemo(GameObject SeedSack)
    {
        if (Movement.MoveToPostion != new Vector3(0.3f, 7.34f, -4.5f))
        {
            Movement.MoveToPostion = new Vector3(0.3f, 7.34f, -4.5f);
            Movement.MoveToRotation = new Vector3(64.55f, 0, 0);
        }

        //finds the player and sets his halo state
        GameObject Player = GameObject.Find("Test player");
        Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), false, null);

        Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), false, null);
        LevelSupport.Level = 2;
        MoveLogic.inDemo = true;
        if(Movement.MoveToPostion != new Vector3(0.3f, 7.34f, -4.5f))
        {

            Movement.MoveToPostion = new Vector3(0.3f, 7.34f, -4.5f);
            Movement.MoveToRotation = new Vector3(64.55f, 0, 0);
        }

        if (SeedSack.name == "GhostSeedSack" && MoveLogic.inDemo)
        {
            SeedSack.SetActive(true);

            SetActiveObject(false, gameObject);

            speed = OverScenesManager.Speed;
        }

        ToMoveObject = SeedSack;

        ToMoveVector = new Vector3(-1.2f, 0.5f, -3.5f);

        yield return new WaitForSeconds(0.02f);

        ToMoveFunction = true;
        yield return new WaitForSeconds(1.5f);

        yield return new WaitForSeconds(0.05f);
        ToMoveObject = SeedSack;
        int ArrayCounter = 0;

        yield return new WaitForSeconds(1.5f);
        if (MoveLogic.inDemo)
        {
            for (int i = 0; i < numberOfTimesToGoOverRows; i++)
            {
              //  print("Normal i value: " + i);
               // print("i mod 2: " + i % 2);

                if (MoveLogic.inDemo == false) {  break; }
                if (ToMoveObject.transform.position != new Vector3(-1.2f, 0.5f, 3.5f))
                {
                  //  print("In the if for moving to top of seed sack\n");
                    ToMoveObject = SeedSack;
                    ToMoveVector = new Vector3(-1.2f, 0.5f, 3.5f);
                }
                else
                {
                   // print("Should be moving crop");
                    if ((i % 2 == 1) && i != 0)
                    {
                       // print("Moving crops");

                        ToMoveObject = Crops;
                        ToMoveVector = new Vector3(Place[ArrayCounter], Crops.transform.position.y, Crops.transform.position.z);

                        yield return new WaitForSeconds(0.05f);
                        ArrayCounter++;

                        ToMoveFunction = true;
                        yield return new WaitForSeconds(2);
                    }
                    ToMoveObject = SeedSack;
                    ToMoveVector = new Vector3(-1.2f, 0.5f, -3.5f);
                }

                yield return new WaitForSeconds(0.05f);

                ToMoveFunction = true;

                yield return new WaitForSeconds(3);


                transform.GetChild(0).GetComponent<ParticleSystem>().Stop();

            }
        }

        yield return new WaitForSeconds(0.05f);

        ToMoveObject = Crops;
        ToMoveVector = new Vector3(0, Crops.transform.position.y, Crops.transform.position.z);
        ToMoveFunction = true;
        yield return new WaitForSeconds(3);

        SeedSack.SetActive(false);
        //  helper.SetActive(false);

        ArrayCounter = 0;
        if (hasOnlyShownTheDemo == false)
        {
            SeedSack.SetActive(true);
            StartCoroutine(ReadyLevelTwo()); // so if you are doing the demo, it will automatically play the level but now so the user can play it
        }
        onlyShowDemo = false;
        LevelSupport.Level = 0;
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), true, null);

        Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), true, null);
        OverScenesManager.level += 1;


    }

    /// <summary>
    /// sets up the targets at opposite ends of the garden, so you move between them
    /// </summary>
    /// <returns></returns>
    IEnumerator ReadyLevelTwo()
    {
        LevelSlideSelector.SlideLevel = 2;
        LevelSupport.Level = 0;
        yield return new WaitForSeconds(0.5f);

        GameObject.Find("TargetOne").transform.position = new Vector3(-2.21f, 0, 3.96f);
        GameObject.Find("TargetTwo").transform.position = new Vector3(-2.21f, 0, -3.96f);

        Target.transform.position = GameObject.Find("TargetOne").transform.position;

        yield return new WaitForSeconds(0.5f);
        TargetMovement.TargetMoveto = GameObject.Find("TargetOne").transform.position;

        GameObject.Find("TargetOne").GetComponent<BoxCollider>().enabled = true;
        GameObject.Find("TargetTwo").GetComponent<BoxCollider>().enabled = true;

        Target.GetComponent<TargetMovement>().UseDefaultPosition = true;
        SetActiveObject(true, gameObject);

    }

    /// <summary>
    /// moves all the crops to the left and then to the right in a repeating pattern so the user
    /// can just move up and down but still be able to plant seeds in all of the locations
    /// </summary>
    /// <returns></returns>
    IEnumerator MoveCrops()
    {

        TargetOne.GetComponent<BoxCollider>().enabled = false;
        TargetTwo.GetComponent<BoxCollider>().enabled = false;

        yield return new WaitForSeconds(0.60f);//should change if this is too slow or too fast,
        // if you do change this value should be the same as the other in the objectives script under NextTarget method.

        TargetMovement.TargetMoveto = TargetOne.transform.position;
        Target.transform.position = TargetOne.transform.position;

        ToMoveObject = Crops;

        if (ArrayCounter == 3)
        {
            ToMoveVector = new Vector3(Place[1], Crops.transform.position.y, Crops.transform.position.z);
        }
        else if (ArrayCounter < 3)
        {
            ToMoveVector = new Vector3(Place[ArrayCounter], Crops.transform.position.y, Crops.transform.position.z);
        }

        if (ArrayCounter > 3)
        {
            TargetOne.GetComponent<BoxCollider>().enabled = false;
            TargetTwo.GetComponent<BoxCollider>().enabled = false;

            LevelSlideSelector.SlideLevel = 0;

            GhostSeedSack.SetActive(false); // stopping the ghost seed sack from being visible

            transform.GetComponent<Collider>().enabled = false;

            transform.parent = GameObject.Find("Tools").transform;
            transform.position = DefualtPosition;

            GameObject.Find("Test player").GetComponent<BoxCollider>().enabled = true;

            transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);
            Target.GetComponent<TargetMovement>().UseDefaultPosition = false;
            Target.transform.position = TargetOne.transform.position;

            PickedUp = false;

            transform.GetComponent<BoxCollider>().enabled = false;
            MoveLogic.inDemo = true;

            Objectives.TimeHit = 0;

            OverScenesManager.level += 1;

            ArrayCounter = 0;

            GameObject.Find("Test player").GetComponent("Halo").GetType().GetProperty("enabled").SetValue(GameObject.Find("Test player").GetComponent("Halo"), true, null);// gives the blue halo back to the player so they can see their position
            transform.GetChild(0).GetComponent<ParticleSystem>().Stop(); // turns off the particle system that disperses seeds from the seed sack

        }
        else
        {
            yield return new WaitForSeconds(0.05f);
            ArrayCounter += 1;

            ToMoveFunction = true;
            yield return new WaitForSeconds(2);
            TargetOne.GetComponent<BoxCollider>().enabled = true;
            TargetTwo.GetComponent<BoxCollider>().enabled = true;
            TargetMovement.TargetMoveto = TargetOne.transform.position;
        }
    }
}