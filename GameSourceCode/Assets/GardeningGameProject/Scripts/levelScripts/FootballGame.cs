﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This major <c>FootballGame</c> class.
/// Contains all the methods used for the football game
/// <list type="bullet">
/// <item>
/// <term>Start</term>
/// <description>Reads the High Score for the football game and sets the sttarting position of the football</description>
/// </item>
/// <item>
/// <term>Update</term>
/// <description>Puts the camera into the correct position to start the game and if the demo is playing it moves the player to where the ball will go</description>
/// </item>
/// <item>
/// <term>KickBallsAndReset</term>
/// <description>Does a nice for loop until the time runs out kicking balls in random directions</description>
/// </item>
/// <item>
/// </list>
/// </summary>
/// <remarks>
/// <para>This class is just for the football game, it can be called by settings footballGame.started = true, it will run through the whole game, then increment the level counter</para>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
/// 
public class FootballGame : MonoBehaviour
{
    public static bool started = false;

    public Rigidbody football;
    private Vector3 footballStartPosition = new Vector3(56f, 1f, 129f);
    private Vector3 kickForce = new Vector3(0f, 0f, 0f);
    public GameObject player;
    public int gameLength = 20;
    private int currentGameLength = 0;
    private int numberOfSaves = -1;
    public GameObject finalDispalyText;
    private float ballX = 0f;
    public float factor = 1f;
    public float constant = 0f;

    public GameObject helper;

    private int footballGameHiScore = 0;

    public GameObject Target;
    public GameObject TargetArm;

    private Vector3 StartingPostion;
    private bool footBallGameOn;

    public static bool DemoFootBall = false;
    private bool startedDemo = true;
    private bool endGame = false;

    void Start()
    {
        StartingPostion = Target.transform.position;

        footballGameHiScore = PlayerPrefs.GetInt("footballGameHiScore", 0);
    }


    /// <summary>
    /// this updates the game
    /// </summary>
    void Update()
    {
        if (started)
        {
            Movement.MoveToPostion = new Vector3(55.45f, 2.72f, 109.66f);
            Movement.MoveToRotation = new Vector3(1f, -0.8f, 0f);
            player.transform.position = new Vector3(55.64f, 1.2f, 115f);
            OverScenesManager.PlayLabView = true;
            started = false;
            LevelSupport.Level = 0;
            StartCoroutine(KickBallsAndReset());
            StartCoroutine(RecordLocation());
        }
        if (footBallGameOn == true)
        {
            TargetArm.transform.position = Target.transform.position - StartingPostion;
        }
        if (DemoFootBall)
        {
            if (startedDemo)
            {
                helper.SetActive(true);
                LevelSupport.Level = 5;
                Movement.MoveToPostion = new Vector3(55.45f, 2.72f, 109.66f);
                Movement.MoveToRotation = new Vector3(1f, -0.8f, 0f);
                player.transform.position = new Vector3(55.64f, 1.2f, 115f);
                startedDemo = false;
                gameLength = 10;
                StartCoroutine(KickBallsAndReset());
            }

            if (Vector3.Distance(player.transform.position, Target.transform.position) > 0.1f)
            {
                float Step = Time.deltaTime * OverScenesManager.Speed;

                player.transform.position = Vector3.MoveTowards(player.transform.position, Target.transform.position, Step);
            }
        }
    }

    /// <summary>
    /// this is the mainloop of the game, it will keep spawning and kicking footballs until the game runs out of time, it also shows messages at the start and end of the game
    /// </summary>
    /// <returns></returns>
    IEnumerator KickBallsAndReset()
    {
        transform.GetChild(3).gameObject.SetActive(true);
        yield return new WaitForSeconds(5f);
        finalDispalyText.SetActive(true);
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "Move the arm to save the Balls. ";
        yield return new WaitForSeconds(3f);
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "Your High Score is " + footballGameHiScore.ToString();
        yield return new WaitForSeconds(3f);
        finalDispalyText.SetActive(false);

        if(!DemoFootBall)
        {
            OverScenesManager.PlayLabView = true;
        }

        while (currentGameLength < gameLength)
        {

            kick(football);
            yield return new WaitForSeconds(4f);
            ballX = Random.Range(-4f, 3f);
            kickForce = new Vector3(ballX, Random.Range(-3f, 1f), -10f);
            moveUserTarget();
            if (football.transform.position.z >= 115)
            {
                if (numberOfSaves > -1)
                {
                    if (DemoFootBall == false)
                    {
                        PointSystem.ScoreValue += 100;
                    }

                }

                numberOfSaves++;
            }

            currentGameLength += 4;
            Reset(football);

        }
        endGame = true;
        gameLength = 20; // dodge
        footBallGameOn = false;
        // if they have not beaten their high score, it will just default to telling them how many they saved.
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "You saved " + numberOfSaves.ToString() + " balls";
        if (numberOfSaves > footballGameHiScore)
        {
            if (DemoFootBall == false)
            {
                // Shows the player that they have got a nice new high score
                PlayerPrefs.SetInt("footballGameHiScore", numberOfSaves);
                finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "New High Score, you saved " + numberOfSaves.ToString() + " balls";
            }

        }

        numberOfSaves = 0;//for reseting purposes
        finalDispalyText.SetActive(true);
        currentGameLength = 0;
        yield return new WaitForSeconds(3f);
        finalDispalyText.SetActive(false);

        if (DemoFootBall)
        {
            if (LevelChoice.NextLevel == 11)
            {
                DemoFootBall = false;
            }
            else
            {
                DemoFootBall = false;
                OverScenesManager.level += 1;
                LevelSupport.Level = 0;
                helper.SetActive(false);
            }
            startedDemo = true;
        }
        else if (OverScenesManager.gameMode)
        {
            OverScenesManager.PauseLabView = true;
            OverScenesManager.endGameMode = true;
        }
        else
        {
            OverScenesManager.PauseLabView = true;
            OverScenesManager.level += 1;
        }
        transform.GetChild(3).gameObject.SetActive(false);
    }
    /// <summary>
    /// adds a random velocity to the football
    /// </summary>
    /// <param name="itemToKick"></param>
    void kick(Rigidbody itemToKick)
    {
        itemToKick.AddForce(kickForce, ForceMode.Impulse); // kicks the ball in a random direction , gravity is turned off to make things a bit slower
    }

    /// <summary>
    /// this just stops the ball, so that it won't just keep flying off forever
    /// </summary>
    /// <param name="itemToReset"></param>
    void Reset(Rigidbody itemToReset)
    {
        itemToReset.velocity = new Vector3(0f, 0f, 0f); // stops the football, then resets the position
        itemToReset.position = footballStartPosition;
    }


    /// <summary>
    /// trys to guess where the football is going to be, and moves the assist to that location, so that when the demo is being played it will go to the orrect location.
    /// </summary>
    void moveUserTarget()
    {
        Target.transform.position = new Vector3(55.64f + (ballX * factor) + constant, 1.2f, 115f);
        footBallGameOn = true; // this little bit moves the target to where the ball is expeccted to pass the goal line
    }


    IEnumerator RecordLocation()
    {
        yield return new WaitForSeconds(0.75f);

        OverScenesManager.coordinatesList.Reset();
        OverScenesManager.coordinatesList.SetGameName("FishingGame");
        int i = 0;
        GameObject ArmPlayer = player.GetComponent<FishingLevelPlayer>().Player;
        while (!endGame && i < 600)
        {
            OverScenesManager.coordinatesList.AddToList(TargetArm.transform.position.x, TargetArm.transform.position.z,
                                            ArmPlayer.transform.position.x, ArmPlayer.transform.position.z,
                                            TargetArm.transform.position.x, TargetArm.transform.position.z);
            yield return new WaitForSeconds(0.1f);
            i++;
        }
        endGame = false;
        OverScenesManager.coordinatesList.WriteToCSV();
    }

}
