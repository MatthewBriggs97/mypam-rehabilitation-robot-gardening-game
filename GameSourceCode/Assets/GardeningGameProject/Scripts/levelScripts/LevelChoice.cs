﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// <c>LevelChoice</c> class.
/// Contains the level array which controls
/// which order all of the levels are played
/// for each number in the levelOrdering array
/// a corresponding statement is needed
/// the large else if chain
/// is needed to that every game can be started
/// with "FootballGame.started = true;"
/// for example. This should probably be done
/// with polymorhpism... but else if does the job
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class LevelChoice : MonoBehaviour
{
    public static int NextLevel = 0;

    private GameObject[] allChildren;
    private float zCoordinate;
    public GameObject levelEndingText;
    public GameObject player;

    private GameObject Target;
    //private int[] levelOrdering = { 12,12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 18, 18, 18, 18, 18, 18, 18, 2, 9 };



    // 1 = rake garden
    // 2 = plant seeds
    // 3 = water plants
    // 4 = clear weeds
    // 5 = football game
    // 7 = plane game
    // 8 = fishing game
    // 9 = space game
    // 11 = intro
    // 12 = dot to dot game
    // 14 = day night cycle
    // 15 = shop level  (unlocks new shed or whatever)
    // 16 = unlocks new game  (show this one beforea  new game works)
    // 17 = snake game (caterpillar game)
    // 18 = shows you have unlocked a new plant




    //private int[] levelOrdering = { 14,14,4, 4, 4, 4, 4, 2, 5, -12, 4, 16, 12, 16, 8, 16, 5, 16, 7, 16, 9, 15, 14, 18, 1, 2, 3, 4, 15, 5, 7, 9, 15, 1, 2, 3, 4 };
    private int[] levelOrdering = { 11,-1,1,-2,2,-3,3,16,-12,14,
                                -3,3,18,-1,1,-2,2,-3,3,15,-12,16,-5,5,14,
                                -3,3,16,-17,17,3,-12,14,
                                -4,4,3,-5,5,14,3,18,1,2,3,12,14,
                                15,16,-8,8,-3,3,14,
                                -4,4,5,-17,17,14,
                                3,-8,8,12,5,3,15,14,
                                16,-7,7,5,12,3,18,14,
                                1,2,3,4,12,8,17,3,14
                                -7,7,5,15,8,3,14,
                                12,5,12,17,3,14,
                                15,16,-9,9,7,8,14,
                                3,4,5,12,7,12,14,
                                3,-9,9,5,12,17,7,14,
                                3,12,5,12,17,3,14,
                                5,3,8,9,12,15,12,18,14,
                                1,2,3,4,12,8,17,3,5,8,14,
                                5,7,9,17,8,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,      // just get lazy after this point
                                5,3,8,9,12,12,18,14,
                                1,2,3,4,12,8,17,3,5,8,14,
                                5,7,9,17,8,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                5,3,8,9,12,15,12,18,14,
                                1,2,3,4,12,8,17,3,5,8,14,
                                5,7,9,17,8,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                5,3,8,9,12,18,14,
                                1,2,3,4,12,8,17,3,5,8,14,
                                5,7,9,15,17,8,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                5,3,8,9,12,12,18,14,
                                1,2,3,4,12,8,17,3,5,8,14,
                                5,7,9,15,17,8,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                 5,3,8,9,12,18,14,
                                1,2,3,4,12,8,17,3,5,8,14,
                                5,7,9,17,8,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                5,3,8,9,4,12,14, // right here
                                3,12,8,17,3,7,8,14,
                                5,7,9,15,4,17,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                5,3,8,9,4,12,14, // right here
                                3,12,8,17,3,7,8,14,
                                5,7,9,5,4,17,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                5,3,8,9,4,12,14, // right here
                                3,12,8,17,3,7,8,14,
                                5,7,9,4,17,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                5,3,8,9,4,12,12,14, // right here
                                3,12,8,17,3,7,8,14,
                                5,7,9,4,17,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                5,3,8,9,4,12,12,14, // right here
                                3,12,8,17,3,7,8,14,
                                5,7,9,4,17,4,12,3,14,
                                3,4,5,12,7,12,9,17,14,
                                };
                                


    // this is the text that will display before you play a game
    // every game has the same text before you play it
    // when adding a new game make sure to include a suitable message
    // with the same indedx as the level number stored in 'levelOrdering'

    private string[] endOfLevelText = { "Click to begin when you are ready",
                                        "Click to start raking",
                                        "Click to plant seeds",
                                        "Click to start watering plants",
                                        "Click to start deweeding!",
                                        "Click to start the football game",
                                        "Click",
                                        "Click to fly away",
                                        "Click to start fishing",
                                        "Click to start the space game!",
                                        "Click",
                                        "Click",
                                        "Click to play dot to dot",
                                        "Click",
                                        "Click for a new day of Gardening Action!",
                                        "Click to unlock a new Item!",
                                        "Click to unlock a new Game!",
                                        "Oh No! Catterpillars Are Eating Your Plants!",
                                        "Click to unlock new seeds!",
                                        "Click",
    };

    private int levelBackUp = -1;

    private bool checkingPlayerInput = true;
    private bool replayLevel = false;
    private int i = 5;


    private bool gameModePlay = false;
    // Use this for initialization

    void Start()
    {

        // adding all the possible items the player can control into one array
        allChildren = new GameObject[5];

        allChildren[0] = transform.Find("HoePrefab").gameObject;
        allChildren[1] = transform.Find("SeedSack").gameObject;
        allChildren[2] = transform.Find("WateringCanPrefab").gameObject;
        allChildren[3] = transform.Find("Deodorant").gameObject;

        FishingLevelTarget.fishingLevel = false;
        Target = GameObject.Find("Target");

        gameModePlay = OverScenesManager.gameMode;
        if (!gameModePlay)
        {
            StartCoroutine(FinishedLevel());
        }
        else
        {
            NextLevel = OverScenesManager.gameModeLevel;

            if (NextLevel == 5)
            {
                FootballGame.started = true;
            }
            else if (NextLevel == 7)
            {
                PlaneGame.started = true;
            }
            else if (NextLevel == 8)
            {
                FishingLevelTarget.fishingLevel = true;
            }
            else if (NextLevel == 9)
            {
                SpaceGame.started = true;
                SpaceGameDemo.SpaceDemo = false;
            }
            else if (NextLevel == 12)
            {
                DotToDotGame.started = true;
            }
            else if (NextLevel == 17)
            {
                SnakeGameScript.started = true;
            }
        }
    }

    // Update is called once per frame
    /// <summary>
    /// Runs when the player wants to run a new game
    /// cancels the prior one 
    /// and starts the new script, passing control
    /// over to the new one
    /// </summary>
    void Update()
    {
        // this if statement is ran if another file changes what level is on, by doing overScenesMangaer.level++
        if (!gameModePlay)
        {
            if ((OverScenesManager.level != levelBackUp) && !checkingPlayerInput)
            {
                StartCoroutine(FinishedLevel()); // quits out of the previously played level and waits for the user to click before continuing

                if ((MoveLogic.inDemo == false) && !checkingPlayerInput)
                {
                    MoveLogic.inDemo = true;

                    levelBackUp = OverScenesManager.level;


                    levelEndingText.SetActive(false);

                    // the first four levels have a lot of things in common with how they are set up, so it is all done in here
                    if (NextLevel > 0 && NextLevel < 5)
                    {
                        OverScenesManager.PlayLabView = true; // turn the assistance on

                        // moves the camera into the correct position for the start of the level
                        Movement.MoveToPostion = new Vector3(0.3f, 7.34f, -4.5f);
                        Movement.MoveToRotation = new Vector3(64.55f, 0, 0);

                        // turns off all of the halos
                        allChildren[0].GetComponent("Halo").GetType().GetProperty("enabled").SetValue(allChildren[0].GetComponent("Halo"), false, null);
                        allChildren[1].GetComponent("Halo").GetType().GetProperty("enabled").SetValue(allChildren[1].GetComponent("Halo"), false, null);
                        allChildren[2].GetComponent("Halo").GetType().GetProperty("enabled").SetValue(allChildren[2].GetComponent("Halo"), false, null);
                        allChildren[3].GetComponent("Halo").GetType().GetProperty("enabled").SetValue(allChildren[3].GetComponent("Halo"), false, null);

                        allChildren[NextLevel - 1].GetComponent("Halo").GetType().GetProperty("enabled").SetValue(allChildren[NextLevel - 1].GetComponent("Halo"), true, null); // puts a halo on

                        // turns off all of the colliders that are not going to be used since they are for different levels
                        allChildren[0].GetComponent<Collider>().enabled = false;
                        allChildren[1].GetComponent<Collider>().enabled = false;
                        allChildren[2].GetComponent<Collider>().enabled = false;
                        allChildren[3].GetComponent<Collider>().enabled = false;
                        allChildren[NextLevel - 1].GetComponent<Collider>().enabled = true;
                        zCoordinate = -2.5f;
                        if (NextLevel == 1) { zCoordinate = -1.5f; player.transform.position = new Vector3(0.7f, 1.03f, -3.5f); }
                        else if (NextLevel == 2) { zCoordinate = -3.7f; }
                        else if (NextLevel == 3) { zCoordinate = 0.5f; }
                        else if (NextLevel == 4) { zCoordinate = 2.9f; }

                        Target.GetComponent<TargetMovement>().transform.position = new Vector3(4.8f, 0.5f, zCoordinate);
                    }

                    // from here on down it is really just a case of setting the variable of started to true, since all of the levels have the same way of starting 
                    else if (NextLevel == 11 && !MainGameIntro.playing)
                    {
                        MainGameIntro.started = true;
                    }
                    else if (NextLevel == 5)
                    {
                        FootballGame.started = true;
                    }
                    else if (NextLevel == 6)
                    {
                        GameChooser.started = true;
                    }
                    else if (NextLevel == 7)
                    {
                        PlaneGame.started = true;
                    }
                    else if (NextLevel == 8)
                    {
                        FishingLevelTarget.fishingLevel = true;
                    }
                    else if (NextLevel == 9)
                    {
                        SpaceGame.started = true;
                        SpaceGameDemo.SpaceDemo = false;
                    }
                    else if (NextLevel == 10)
                    {
                        CameraTestLevel.started = true;
                    }
                    else if (NextLevel == -8)
                    {
                        FishingLevelTarget.DemoFishing = true;
                    }
                    else if (NextLevel == -5)
                    {
                        FootballGame.DemoFootBall = true;
                    }
                    else if (NextLevel == -7)
                    {
                        PlaneGame.demoPlane = true;
                    }
                    else if (NextLevel == -9)
                    {
                        SpaceGameDemo.SpaceDemo = true;

                        SpaceGame.started = true;
                    }
                    else if (NextLevel == 12)
                    {
                        DotToDotGame.started = true;
                    }
                    else if (NextLevel == -12)
                    {
                        DotToDotGame.started = true;
                        DoToDoCore.DemoDotToDot = true;
                    }
                    else if (NextLevel == -3)
                    {
                        WateringLevel.onlyShowDemo = true;
                    }
                    else if (NextLevel == -2)
                    {
                        PlantSeedLevel.onlyShowDemo = true;
                    }
                    else if (NextLevel == -4)
                    {
                        DemoDeWeed.DemoOfDeWeed = true;
                    }
                    else if (NextLevel == -1)
                    {
                        RakingLevel.onlyShowDemo = true;
                    }
                    else if (NextLevel == 14)
                    {
                        DayNightCycle.started = true;
                    }
                    else if (NextLevel == 15)
                    {
                        ShopLevel.started = true;
                    }
                    else if (NextLevel == 16)
                    {
                        UnlockNewLevelScript.started = true;
                    }
                    else if (NextLevel == 17)
                    {
                        SnakeGameScript.started = true;
                    }
                    else if (NextLevel == -17)
                    {
                        SnakeGameScript.started = true;
                        SnakeDemo.doSnakeDemo = true;
                    }
                    else if (NextLevel == 18)
                    {
                        CropManager.cropUpgrade = true;
                    }

                    i = 0;
                    replayLevel = false; // this bool can be set to true again when the game asks the player if they would like to play the game again
                }
            }
        }
    }


    /// <summary>
    /// Gives an onsceen message asking if the player would like to replay the level,
    /// resets the crops if need be
    /// and display the relevent text to proceed to the next level
    /// and awaits a users mouse click before doing so
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator FinishedLevel()
    {
        checkingPlayerInput = true;

        if ((NextLevel > 0 && i == 0 && checkingPlayerInput) && (NextLevel != levelBackUp) && (levelOrdering[levelBackUp] != 11 && levelOrdering[levelBackUp] != 14 && levelOrdering[levelBackUp] != 15 && levelOrdering[levelBackUp] != 15 && levelOrdering[levelBackUp] != 16 && levelOrdering[levelBackUp] != 18 && levelOrdering[levelBackUp] > -1))
        {
            levelEndingText.transform.GetChild(1).GetComponent<Text>().text = "Click now to replay the level";

            levelEndingText.SetActive(true);
            WaitForSeconds delay = new WaitForSeconds(0.125f);
            while (i < 5)
            {
                if (MoveLogic.inDemo)
                {
                    yield return delay;
                }
                else if(Input.GetKeyDown(KeyCode.R))
                {
                    i = 40;
                }
                else
                {
                    replayLevel = true; // the user has clicked, so doesn't waste time if they just want to replay the level
                    i = 40;
                }
                i += 1;
            }
            MoveLogic.inDemo = true;
            if (replayLevel)
            {
                OverScenesManager.level -= 1;
                levelBackUp = -5;
            }
        }


        /*
         * END OF ARRAY FOR LOOP AFFECT
         * 
         */
        if (levelOrdering.Length <= OverScenesManager.level)
        {
            GameObject.Find("TheCrops").GetComponent<CropManager>().RestCrops();

            PlayerPrefs.SetInt("mainGameScore", 1);

            OverScenesManager.level = 0;

            NextLevel = levelOrdering[OverScenesManager.level];
        }
        else
        {
            NextLevel = levelOrdering[OverScenesManager.level];
        }



        Target.GetComponent<TargetMovement>().transform.position = new Vector3(0.9f, 0f, -1.5f);

        if (NextLevel == 11)
        {
            levelEndingText.transform.GetChild(1).GetComponent<Text>().text = endOfLevelText[0];
        }
        else
        {
            levelEndingText.transform.GetChild(1).GetComponent<Text>().text = endOfLevelText[Mathf.Abs(NextLevel)];
        }

        levelEndingText.SetActive(true);

        checkingPlayerInput = false;

    }
}