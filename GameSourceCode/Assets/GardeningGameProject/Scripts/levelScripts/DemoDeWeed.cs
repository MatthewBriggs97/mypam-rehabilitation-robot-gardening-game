﻿using System.Collections;
using UnityEngine;


/// <summary>
/// This <c>DemoDeWeed</c> class.
/// Script which will summon the ghost deoderant can to move aroudn 
/// follwing the weed targets in a pentagram
/// before the player plays the game themselves
/// 
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class DemoDeWeed : MonoBehaviour
{
    public static bool DemoOfDeWeed = false;

    public GameObject Target;
    public  GameObject Player;
    public  GameObject UnusedTarget;

    private float time;
    private float minutes;
    public GameObject LastWeed;
    public static bool Begin = true;

    public GameObject FirstWeed;
    public int timerSeconds = 10;

    // Update is called once per frame
    void Update()
    {
        if (DemoOfDeWeed)
        {
            if (Begin)
            {
                LevelSupport.Level = 4;     
                SetUp();
                Begin = false;
            }

            if (Vector3.Distance(transform.position, Target.transform.position) > 0.1f)
            {
                float Step = Time.deltaTime * OverScenesManager.Speed;

                transform.position = Vector3.MoveTowards(transform.position, new Vector3(Target.transform.position.x, transform.position.y, Target.transform.position.z), Step);
            }

        }


    }

    /// <summary>
    /// sets the assist target to the first weed
    /// once each weed is touched, it changes the assist 
    /// position to the next one, just going in a big loop
    /// </summary>
    void SetUp()
    {
        GameObject PlayerUser = GameObject.Find("Test player");
        GameObject TargetUser = GameObject.Find("Target");
        PlayerUser.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(PlayerUser.GetComponent("Halo"), false, null);
        TargetUser.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(TargetUser.GetComponent("Halo"), false, null);



        // moves the camera into the correct position for the start of the level
        Movement.MoveToPostion = new Vector3(0.3f, 7.34f, -4.5f);
        Movement.MoveToRotation = new Vector3(64.55f, 0, 0);
        Transform temp = LastWeed.transform.parent.GetChild(0);

        temp.GetComponent<MeshCollider>().enabled = true;
        temp.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(temp.GetComponent("Halo"), true, null);


        transform.GetComponent<BoxCollider>().enabled = true;

        transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;

        FirstWeed.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(FirstWeed.GetComponent("Halo"), true, null);

        Target.transform.position = FirstWeed.transform.position;
        TargetMovement.TargetMoveto = FirstWeed.transform.position;
        Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), true, null);
        Target.GetComponent<TargetMovement>().UseDefaultPosition = true;


        StartCoroutine(Timer());
    }


    /// <summary>
    /// this runs while the whole game is playing
    /// since all of the targets are done on other scripts, 
    /// this timer keeps track of how long the game has been running and will
    /// end it after the correct amount of time.
    /// </summary>
    /// <returns></returns>
    IEnumerator Timer()
    {
        yield return new WaitForSeconds(1.5f);
        int i = 0;
        while (i <= timerSeconds && MoveLogic.inDemo)
        {
            yield return new WaitForSeconds(1);
            i += 1;
        }

        MoveLogic.inDemo = true;

        LastWeed.GetComponent<NextWeed>().NextObject = Target;


        
        Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), false, null);


       // yield return new WaitForSeconds(12);

        yield return new WaitForSeconds(1.5f);

    }
}
