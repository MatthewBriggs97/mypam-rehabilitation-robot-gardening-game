﻿using UnityEngine;

/// <summary>
/// This <c>KillWeeds</c> class.
/// This is what will start the deweedinng level.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class KillWeeds : MonoBehaviour
{
    /// <summary>
    /// This is the target assist
    /// </summary>
    public GameObject Target;

    /// <summary>
    /// The first weed in the game.
    /// </summary>
    public GameObject FirstWeed;

    /// <summary>
    /// When the player collids with this object, the deweeding can.
    /// </summary>
    /// <param name="other">The player</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Test player")
        {
            FirstWeed.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(FirstWeed.GetComponent("Halo"), true, null);

            transform.position = other.transform.position;

            transform.parent = other.transform;

            WeedGameManager.StartOfDeWeed = true;//starts the level

            Target.transform.position = FirstWeed.transform.position;
            TargetMovement.TargetMoveto = FirstWeed.transform.position;

            Target.GetComponent<TargetMovement>().UseDefaultPosition = true;
        }
    }
}
