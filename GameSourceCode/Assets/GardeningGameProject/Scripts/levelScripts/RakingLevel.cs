﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This <c>RakingLevel</c> class.
/// The main script in charge of the raking game.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class RakingLevel : MonoBehaviour
{
    /// <summary>
    /// Where we are in the array counter
    /// </summary>
    private int ArrayCounter = 0;

    /// <summary>
    /// checks if the demo was played
    /// </summary>
  //  private bool DemoPlayed = false;

 //   public GameObject helper;

    /// <summary>
    /// If this is true it will skip this level
    /// </summary>
    public bool skipLevel = false;

    public GameObject GhostHoe;

    public GameObject Crops;

    private Vector3 DefualtPosition;

    public float speed = 3;

    /// <summary>
    ///     values for the MoveToFunction
    /// </summary>
    private bool ToMoveFunction = false;
    private GameObject ToMoveObject;
    private Vector3 ToMoveVector;

    public int numberOfTimesToGoOverRows = 8;

    /// <summary>
    ///     differnt places for the levels 1 and 2
    /// </summary>
    private float[] Place = new float[] { 2.25f, +0, -2.25f };

    private GameObject Target;
    private GameObject TargetOne;
    private GameObject TargetTwo;

    private bool PickedUp = false;

    private GameObject[] arrayOfGameObjects;


    public static bool onlyShowDemo = false;

    private bool hasOnlyShownTheDemo = false;

    /// <summary>
    /// Called at start out and finds the target and the two targets that are used to "bump" off of.
    /// </summary>
    void Start()
    {
        Target = GameObject.Find("Target");
        DefualtPosition = transform.position; // remember where to put it back
        TargetOne = GameObject.Find("TargetOne");
        TargetTwo = GameObject.Find("TargetTwo");

    }

    /// <summary>
    /// Called everyframe, checks if the move function should be called and if the crops need to move,
    /// as well as he demo game start.
    /// </summary>
    void Update()
    {
        if (ToMoveFunction == true)
        {
            float step = speed * Time.deltaTime;
            ToMove(step);
        }
        if (Objectives.TimeHit == 2 && PickedUp == true)
        {
            StartCoroutine(MoveCrops());
            Objectives.TimeHit = 0;
        }
        if (onlyShowDemo == true && hasOnlyShownTheDemo == false)
        {
            StartCoroutine(LevelOneDemo(GhostHoe));


            Movement.MoveToPostion = new Vector3(0.3f, 7.34f, -4.5f);
            Movement.MoveToRotation = new Vector3(64.55f, 0, 0);
            hasOnlyShownTheDemo = true;
        }
    }

    /// <summary>
    /// moves one object closer to another
    /// </summary>
    /// <param name="step">the step between movements</param>
    void ToMove(float step)
    {
        if (ToMoveObject.transform.position != ToMoveVector)
        {
            ToMoveObject.transform.position = Vector3.MoveTowards(ToMoveObject.transform.position, ToMoveVector, step);
        }
        else
        {
            ToMoveFunction = false;
        }

    }

    /// <summary>
    /// when the object has been entred by another, it will skip the level or 
    /// </summary>
    /// <param name="other">The the player</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Test player")
        {
            if (!skipLevel)
            {//if the level is not to be skipped
                PickedUp = true;
                // set state
                PlayTheGame();
                //point for following
                PointSystem.ScoreValue += 5;

                transform.eulerAngles = new Vector3(180, -15, 6);

                transform.parent = other.transform;

                transform.position = other.transform.position;

                transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);

                other.transform.GetComponent<BoxCollider>().enabled = false;


                StartCoroutine(ReadyLevelOne());
            }
        }
    }

    /// <summary>
    /// This is used to enable a object without unsetting its active state. 
    /// </summary>
    /// <param name="OffOn">Should it turn the give object on or off?</param>
    /// <param name="Object">The object that wishs to be set on or off.</param>
    void SetActiveObject(bool OffOn, GameObject Object)
    {
        Object.GetComponent<BoxCollider>().enabled = OffOn;
        Object.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Object.GetComponent("Halo"), OffOn, null);
        Object.transform.GetChild(0).gameObject.SetActive(OffOn);
        Object.transform.GetChild(1).gameObject.SetActive(OffOn);
    }

    /// <summary>
    /// the main powerhouse method for making the level work. ONLY USED FOR DEMO
    /// </summary>
    /// <param name="Hoe">This is the tool, it can be the ghost or the normal rake</param>
    IEnumerator LevelOneDemo(GameObject Hoe)
    {
        //finds the player and sets his halo state
        GameObject Player = GameObject.Find("Test player");
        Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), false, null);

        Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), false, null);
        LevelSupport.Level = 1;
        MoveLogic.inDemo = true; // resets the demo setting  to true, so that all the scenes do not skip

        if (Hoe.name == "GhostHoe" && MoveLogic.inDemo)
        {//if its in the demo state
            SetActiveObject(false, gameObject);

            Hoe.SetActive(true);

            speed = OverScenesManager.Speed;
        }

        yield return new WaitForSeconds(0.05f);

        Hoe.transform.position = new Vector3(1.8f, 0.5f, 0);
        ToMoveObject = Hoe;
        int ArrayCounter = 0;
        yield return new WaitForSeconds(1);

        if (MoveLogic.inDemo)
        {
            for (int i = 0; i < numberOfTimesToGoOverRows; i++)
            {
                if (MoveLogic.inDemo == false) {// Debug.Log("Breaking from loop");
                    break; }

                if (ToMoveObject.transform.position != new Vector3(1.8f, 0.5f, 0))
                {
                    ToMoveObject = Hoe;
                    ToMoveVector = new Vector3(1.8f, 0.5f, 0);
                }
                else
                {
                    if (i % 2 == 0 && i != 0)
                    {//moves the crops along every other
                        ToMoveObject = Crops;
                        ToMoveVector = new Vector3(Crops.transform.position.x, Crops.transform.position.y, Place[ArrayCounter]);

                        yield return new WaitForSeconds(0.05f);
                        ArrayCounter++;

                        ToMoveFunction = true;
                        yield return new WaitForSeconds(3);
                    }
                    ToMoveObject = Hoe;
                    ToMoveVector = new Vector3(-4.5f, 0.5f, 0);
                }

                yield return new WaitForSeconds(0.05f);

                ToMoveFunction = true;

                yield return new WaitForSeconds(3);
                ToMoveObject = Hoe;
            }
        }

        yield return new WaitForSeconds(0.05f);

        ToMoveObject = Crops;
        ToMoveVector = new Vector3(Crops.transform.position.x, Crops.transform.position.y, 0);
        yield return new WaitForSeconds(0.02f);
        ToMoveFunction = true;
        yield return new WaitForSeconds(3);

        Hoe.SetActive(false);
        yield return new WaitForSeconds(0.5f);

        ArrayCounter = 0;
        //turning halos on
        Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), true, null);
        Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), true, null);
      //  helper.SetActive(false); // turning off the part of the screen which tells you it's in a demo
        onlyShowDemo = false;
        if (hasOnlyShownTheDemo == false)
        {//if shown demo start the level
            StartCoroutine(ReadyLevelOne());
        }
        else
        {//if demo hasnt been end all
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            gameObject.transform.GetChild(1).gameObject.SetActive(true);

            hasOnlyShownTheDemo = false;
            LevelSupport.Level = 0;
            OverScenesManager.level += 1;
        }

    }

    /// <summary>
    /// The new updated way of the main rake level to get started, ONLY USED BY USER
    /// </summary>
    IEnumerator ReadyLevelOne()
    {
        GameObject Player = GameObject.Find("Test player");
        Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), false, null);

        yield return new WaitForSeconds(0.5f);

        LevelSlideSelector.SlideLevel = 1;
        LevelSupport.Level = 0;
        Target.transform.position = TargetOne.transform.position;

        yield return new WaitForSeconds(0.5f);
        TargetMovement.TargetMoveto = TargetTwo.transform.position;

        TargetOne.GetComponent<BoxCollider>().enabled = true;
        TargetTwo.GetComponent<BoxCollider>().enabled = true;

        Target.GetComponent<TargetMovement>().UseDefaultPosition = true;
        SetActiveObject(true, gameObject);

    }

    /// <summary>
    /// The new updated way of the main rake level to play, ONLY USED BY USER
    /// Passes the target back and fourth whilst moving crops.
    /// </summary>
    IEnumerator MoveCrops()
    {
        TargetOne.GetComponent<BoxCollider>().enabled = false;
        TargetTwo.GetComponent<BoxCollider>().enabled = false;

        yield return new WaitForSeconds(0.60f);//should change if this is too slow or too fast,
        // if you do change this value should be the same as the other in the objectives script under NextTarget method.

        TargetMovement.TargetMoveto = TargetOne.transform.position;
        Target.transform.position = TargetOne.transform.position;

        ToMoveObject = Crops;

        if (ArrayCounter == 3)
        {
            ToMoveVector = new Vector3(Crops.transform.position.x, Crops.transform.position.y, Place[1]);
        }
        else if (ArrayCounter < 3)
        {
            ToMoveVector = new Vector3(Crops.transform.position.x, Crops.transform.position.y, Place[ArrayCounter]);
        }

        if (ArrayCounter > 3)
        {//starts ending the rake

            TargetOne.GetComponent<BoxCollider>().enabled = false;
            TargetTwo.GetComponent<BoxCollider>().enabled = false;

            LevelSlideSelector.SlideLevel = 0;

            GhostHoe.SetActive(false);

            transform.GetComponent<Collider>().enabled = false;

            transform.parent = GameObject.Find("Tools").transform;
            transform.position = DefualtPosition;

            //stops all colliders
            TargetOne.GetComponent<BoxCollider>().enabled = false;
            TargetTwo.GetComponent<BoxCollider>().enabled = false;
            transform.GetComponent<BoxCollider>().enabled = false;
            GameObject.Find("Test player").GetComponent<BoxCollider>().enabled = true;


            transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);
            Target.GetComponent<TargetMovement>().UseDefaultPosition = false;
            Target.transform.position = TargetOne.transform.position;

            MoveLogic.inDemo = true;
            PickedUp = false;
            ArrayCounter = 0;
            Objectives.TimeHit = 0;

            GameObject Player = GameObject.Find("Test player");
            Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), true, null);
            OverScenesManager.level += 1;//ends the game
        }
        else
        {
            yield return new WaitForSeconds(0.05f);
            ArrayCounter += 1;

            ToMoveFunction = true;
            yield return new WaitForSeconds(2);
            TargetOne.GetComponent<BoxCollider>().enabled = true;
            TargetTwo.GetComponent<BoxCollider>().enabled = true;
            TargetMovement.TargetMoveto = TargetOne.transform.position;
        }
    }

    /// <summary>
    /// these last three methods are for initeracting with labview
    /// </summary>
    void PlayTheGame()
    {
        OverScenesManager.PlayLabView = true;
    }
}