﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This <c>NextWeed</c> class.
/// When the object with this script attached his triggered it will move the target to the other.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class NextWeed : MonoBehaviour
{
    /// <summary>
    /// The next place to pass the assist around
    /// </summary>
    private Vector3 NextPostion;

    /// <summary>
    /// The next object where the assist will go
    /// </summary>
    public GameObject NextObject;

    /// <summary>
    /// Called on start of scene, sets its next postion
    /// </summary>
    void Start()
    {
        NextPostion = NextObject.transform.position;
    }

    /// <summary>
    /// Called when the object (Weed) is hit, Move the assist to other or ends the game
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        //if its a ghost or player
        if ((other.name == "DeodorantGhost" || other.name == "Deodorant") && transform.name != "" && (NextObject.name != "Target" && NextObject.name != "TargetDemo"))
        {
            StartCoroutine(NextTarget());//moves to next target
        }
        else if ((NextObject.name == "Target" || NextObject.name == "TargetDemo") && (other.name == "DeodorantGhost" || other.name == "Deodorant"))
        {//If its the last weed, its next object will be target
            //Starts to shut down this level
            NextObject.transform.position = new Vector3(1.99f, 1, 0);

            transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);

            NextObject.GetComponent<TargetMovement>().UseDefaultPosition = false;
            other.transform.parent = GameObject.Find("Tools").transform;
            other.transform.position = new Vector3(3.901f, 0.617f, 3.672f);

            if (DemoDeWeed.DemoOfDeWeed)
            {//if its in demo state
                DemoDeWeed.DemoOfDeWeed = false;
                OverScenesManager.level += 1;
                DemoDeWeed.Begin = true;
                GameObject PlayerUser = GameObject.Find("Test player");
                GameObject TargetUser = GameObject.Find("Target");
                PlayerUser.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(PlayerUser.GetComponent("Halo"), true, null);
                TargetUser.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(TargetUser.GetComponent("Halo"), true, null);

                LevelSupport.Level = 0;
                //makes the demo can invisable
                other.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                other.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;

                transform.GetComponent<MeshCollider>().enabled = false;

                NextObject = transform.parent.GetChild(0).gameObject;

                other.transform.GetComponent<BoxCollider>().enabled = false;
            }
            else
            {//If it isnt a demo
                other.transform.GetComponent<BoxCollider>().enabled = false;
                transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);
                WeedGameManager.EndOfDeWeed = true;
                transform.GetComponent<MeshCollider>().enabled = false;

                NextObject = transform.parent.GetChild(0).gameObject;
            }
        }
    }

    /// <summary>
    /// When called it will move the target to the net postion
    /// </summary>
    IEnumerator NextTarget()
    {
        transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(transform.GetComponent("Halo"), false, null);

        if (NextObject.name != "Target" && NextObject.name != "TargetDemo")
        {//turns the halo off of the current and on, for the next object
            NextObject.transform.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(NextObject.transform.GetComponent("Halo"), true, null);
            NextObject.transform.GetComponent<MeshCollider>().enabled = true;
        }

        transform.GetComponent<MeshCollider>().enabled = false;

        yield return new WaitForSeconds(0.5f);//So the user has time to see where they move to next

        TargetMovement.TargetMoveto = NextPostion;
    }
}
