﻿using System.Collections;
using UnityEngine;



/// <summary>
/// This major <c>MoveLogic</c> class.
/// Contains all methods for moving the target and therefore the user.
/// <list type="bullet">
/// <item>
/// <term>Level One</term>
/// <description>Controls all movements for level one, left to right</description>
/// </item>
/// <item>
/// <term>Level Two</term>
/// <description>Movements and controls for level two, up and down</description>
/// </item>
/// <item>
/// <term>Level three</term>
/// <description>Movements and controls for level three, circular movements</description>
/// </item>
/// <item>
/// <term>Equips tools</term>
/// <description>When user picks up a tool this judges movements</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>This class controls major movments for the levels, equip tools and basic LabVIEW controls.</para>
/// <para>The level movements can be used for any objects, therefore used for the demos.</para>
/// </remarks>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class MoveLogic : MonoBehaviour
{

    public static bool inDemo = true;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(MouseClicked());
        }

    }
    IEnumerator MouseClicked()
    {
        inDemo = false;

        yield return new WaitForSeconds(0.2f);

        inDemo = true;
    }
}