﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This <c>CropManager</c> class.
/// Moves the object towords a new target.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class CropManager : MonoBehaviour
{
    private GameObject[] crops = new GameObject[3];

    public GameObject[] plants;
    public Material defualtMat;
    public static bool cropUpgrade = false;

    private int[] cropLevels = { 0, 0, 0 };

    public GameObject text;

    private bool inLevelUp = false;

    // Use this for initialization
    void Start()
    {

        OverScenesManager.plantLevel = PlayerPrefs.GetInt("plantLevel", 0);

        crops[1] = transform.GetChild(0).gameObject;
        crops[0] = transform.GetChild(1).gameObject;
        crops[2] = transform.GetChild(2).gameObject;

        StartCoroutine(UpdateAllCrops());

    }

    void Update()
    {
        if (cropUpgrade)
        {
            inLevelUp = true;
            NextLevel();
            cropUpgrade = false;
        }
        if (OverScenesManager.SceneChanging)
        {
            BackUPCropDetails();
            OverScenesManager.SceneChanging = false;
        }
    }

    public void NextLevel()
    {
        OverScenesManager.plantLevel += 1;

        PlayerPrefs.SetInt("plantLevel", OverScenesManager.plantLevel);
        StartCoroutine(UpdateAllCrops());
    }

    public void BackUPCropDetails()
    {
        for (int i = 0; i <= 2; i++)
        {
            for (int j = 0; j <= 5; j++)
            {
                Transform temp = crops[i].transform.GetChild(j);
                Transform temp2 = crops[i].transform.GetChild(j).GetChild(0);

                if (crops[i].transform.GetChild(j).GetChild(0).gameObject.activeSelf)
                {
                    OverScenesManager.theSeedPlanted[i, j] = 1;
                }
                else
                {
                    OverScenesManager.theSeedPlanted[i, j] = 0;
                }

                OverScenesManager.theDirtSize[i, j] = temp.localScale.x;
                OverScenesManager.theCropGrowth[i, j] = temp2.localScale.x;
            }
        }

        int[] theSeedPlantedZero = SetArrayindexInt(OverScenesManager.theSeedPlanted, 0);
        int[] theSeedPlantedOne = SetArrayindexInt(OverScenesManager.theSeedPlanted, 1);
        int[] theSeedPlantedTwo = SetArrayindexInt(OverScenesManager.theSeedPlanted, 2);

        float[] theDirtSizeZero = SetArrayindexFloat(OverScenesManager.theDirtSize, 0);
        float[] theDirtSizeOne = SetArrayindexFloat(OverScenesManager.theDirtSize, 1);
        float[] theDirtSizeTwo = SetArrayindexFloat(OverScenesManager.theDirtSize, 2);

        float[] theCropGrowthZero = SetArrayindexFloat(OverScenesManager.theCropGrowth, 0);
        float[] theCropGrowthOne = SetArrayindexFloat(OverScenesManager.theCropGrowth, 1);
        float[] theCropGrowthTwo = SetArrayindexFloat(OverScenesManager.theCropGrowth, 2);


        PlayerPrefsX.SetIntArray("theSeedPlantedZero", theSeedPlantedZero);
        PlayerPrefsX.SetIntArray("theSeedPlantedOne", theSeedPlantedOne);
        PlayerPrefsX.SetIntArray("theSeedPlantedTwo", theSeedPlantedTwo);

        PlayerPrefsX.SetFloatArray("theDirtSizeZero", theDirtSizeZero);
        PlayerPrefsX.SetFloatArray("theDirtSizeOne", theDirtSizeOne);
        PlayerPrefsX.SetFloatArray("theDirtSizeTwo", theDirtSizeTwo);

        PlayerPrefsX.SetFloatArray("theCropGrowthZero", theCropGrowthZero);
        PlayerPrefsX.SetFloatArray("theCropGrowthOne", theCropGrowthOne);
        PlayerPrefsX.SetFloatArray("theCropGrowthTwo", theCropGrowthTwo);

        PlayerPrefs.SetInt("plantLevel", OverScenesManager.plantLevel);
    }


    public void UpdateCropDetails()
    {

        int[,] theSeedPlanted = GetArrayInt("theSeedPlanted");

        float[,] theDirtSize = GetArrayFloat("theDirtSize");

        float[,] theCropGrowth = GetArrayFloat("theCropGrowth");

        OverScenesManager.theSeedPlanted = theSeedPlanted;
        OverScenesManager.theCropGrowth = theCropGrowth;
        OverScenesManager.theDirtSize = theDirtSize;

        for (int i = 0; i <= 2; i++)
        {
            for (int j = 0; j <= 5; j++)
            {

                float backUp = OverScenesManager.theDirtSize[i, j];
                float backUp2 = OverScenesManager.theCropGrowth[i, j];
                int backUp3 = OverScenesManager.theSeedPlanted[i, j];

                if (backUp3 == 1)
                {
                    crops[i].transform.GetChild(j).GetChild(0).gameObject.SetActive(true);
                }
                if (backUp > 0.2)
                {
                    crops[i].transform.GetChild(j).localScale = new Vector3(backUp, backUp, backUp);
                }
                if (backUp2 > 0.99f)
                {
                    StartCoroutine(crops[i].transform.GetChild(j).GetComponent<Crop>().Growth());
                }
            }
        }
    }

    public IEnumerator UpdateAllCrops()
    {
        GameObject Player = GameObject.Find("Test player");
        GameObject Target = GameObject.Find("Target");

        if (inLevelUp)
        {
            Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), false, null);
            Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), false, null);
            RestCrops();

            if (Movement.MoveToPostion != new Vector3(0.3f, 7.34f, -4.5f))
            {
                // moves the camera into the correct position for the start of the level
                Movement.MoveToPostion = new Vector3(0.3f, 7.34f, -4.5f);
                Movement.MoveToRotation = new Vector3(64.55f, 0, 0);

                yield return new WaitForSeconds(5);
            }
        }

        int temp = PlayerPrefs.GetInt("plantLevel", 0);

        if (temp >= 2)
        {
            cropLevels[0] = temp;
            cropLevels[1] = temp - 1;
            cropLevels[2] = temp - 2;
        }
        else if (temp == 0)
        {
            cropLevels[0] = 0;
            cropLevels[1] = 0;
            cropLevels[2] = 0;
        }
        else if (temp == 1)
        {
            cropLevels[0] = temp;
            cropLevels[1] = 0;
            cropLevels[2] = 0;
        }
        if (inLevelUp)
        {
            text.SetActive(true);
            text.transform.GetChild(1).GetComponent<Text>().text = "You've unlocked a " + plants[cropLevels[0]].name;
            yield return new WaitForSeconds(3);

            Movement.MoveToPostion = new Vector3(-4.820137f, 0.8113518f, 1.639133f);
            Movement.MoveToRotation = new Vector3(21.076f, 36.924f, 0);

            yield return new WaitForSeconds(3);
        }

        UpdateRow(0, cropLevels[0]);
        UpdateRow(1, cropLevels[1]);
        UpdateRow(2, cropLevels[2]);

        if (inLevelUp)
        {
            GameObject plant = crops[0].transform.GetChild(3).GetChild(0).gameObject;
            Vector3 backUp = crops[0].transform.GetChild(3).GetChild(0).localScale;
            float backUpGrowth = plant.transform.parent.GetComponent<Crop>().growFactor;

            plant.SetActive(true);

            plant.transform.parent.GetComponent<Crop>().growFactor = 0.75f;
            StartCoroutine(plant.transform.parent.GetComponent<Crop>().Growth());

            yield return new WaitForSeconds(7);

            Movement.MoveToPostion = new Vector3(0.3f, 7.34f, -4.5f);
            Movement.MoveToRotation = new Vector3(64.55f, 0, 0);

            yield return new WaitForSeconds(5);
            plant.transform.parent.GetComponent<Crop>().growFactor = backUpGrowth;
            plant.transform.localScale = backUp;
            plant.SetActive(false);

            Player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Player.GetComponent("Halo"), true, null);

            Target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(Target.GetComponent("Halo"), true, null);

            OverScenesManager.level += 1;
        }
        else
        {
            UpdateCropDetails();
        }
    }

    public void RestCrops()
    {
        for (int i = 0; i <= 2; i++)
        {
            for (int j = 0; j <= 5; j++)
            {
                Transform temp = crops[i].transform.GetChild(j);
                temp.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                temp.GetComponent<MeshRenderer>().material = defualtMat;
            }
        }
        //  UpdateAllCrops();
    }

    public void UpdateRow(int row, int newVeg)
    {

        if (newVeg < plants.Length)
        {
            for (int i = 0; i <= 5; i++)
            {
                Transform temp = crops[row].transform.GetChild(i).GetChild(0);

                Destroy(temp.gameObject);

                Transform partent = crops[row].transform.GetChild(i);

                GameObject newObject = Instantiate(plants[newVeg], partent) as GameObject;

                newObject.SetActive(false);
                newObject.transform.SetSiblingIndex(0);
            }
        }
    }

    float[] SetArrayindexFloat(float[,] info, int index)
    {
        float[] temp = new float[6];

        for (int i = 0; i < 6; i++)
        {
            temp[i] = info[index, i];
        }
        return temp;
    }

    int[] SetArrayindexInt(int[,] info, int index)
    {
        int[] temp = new int[6];

        for (int i = 0; i < 6; i++)
        {
            temp[i] = info[index, i];
        }
        return temp;
    }


    int[,] GetArrayInt(string key)
    {
        int[,] nothing = new int[3, 6] { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } };
        if (PlayerPrefsX.GetIntArray(key + "Zero", 0, 0).Length > 5)
        {
            int[] tempOne = PlayerPrefsX.GetIntArray(key + "Zero", 0,0);
            int[] tempTwo = PlayerPrefsX.GetIntArray(key + "One", 0, 0);
            int[] tempThree = PlayerPrefsX.GetIntArray(key + "Two", 0, 0);



            int[,] tempBackUp = new int[3, 6];
            tempBackUp[0, 0] = tempOne[0];
            tempBackUp[0, 1] = tempOne[1];
            tempBackUp[0, 2] = tempOne[2];
            tempBackUp[0, 3] = tempOne[3];
            tempBackUp[0, 4] = tempOne[4];
            tempBackUp[0, 5] = tempOne[5];

            tempBackUp[1, 0] = tempTwo[0];
            tempBackUp[1, 1] = tempTwo[1];
            tempBackUp[1, 2] = tempTwo[2];
            tempBackUp[1, 3] = tempTwo[3];
            tempBackUp[1, 4] = tempTwo[4];
            tempBackUp[1, 5] = tempTwo[5];

            tempBackUp[2, 0] = tempThree[0];
            tempBackUp[2, 1] = tempThree[1];
            tempBackUp[2, 2] = tempThree[2];
            tempBackUp[2, 3] = tempThree[3];
            tempBackUp[2, 4] = tempThree[4];
            tempBackUp[2, 5] = tempThree[5];

            nothing = tempBackUp;

        }
        return nothing;
    }

    float[,] GetArrayFloat(string key)
    {
        float[,] nothing = new float[3, 6] { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } };

        if (PlayerPrefsX.GetFloatArray(key + "Zero", 0, 0).Length > 5)
        {
            float[] tempOne = PlayerPrefsX.GetFloatArray(key + "Zero", 0, 0);
            float[] tempTwo = PlayerPrefsX.GetFloatArray(key + "One", 0, 0);
            float[] tempThree = PlayerPrefsX.GetFloatArray(key + "Two", 0, 0);

            float[,] tempBackUp = new float[3, 6];
            tempBackUp[0, 0] = tempOne[0];
            tempBackUp[0, 1] = tempOne[1];
            tempBackUp[0, 2] = tempOne[2];
            tempBackUp[0, 3] = tempOne[3];
            tempBackUp[0, 4] = tempOne[4];
            tempBackUp[0, 5] = tempOne[5];

            tempBackUp[1, 0] = tempTwo[0];
            tempBackUp[1, 1] = tempTwo[1];
            tempBackUp[1, 2] = tempTwo[2];
            tempBackUp[1, 3] = tempTwo[3];
            tempBackUp[1, 4] = tempTwo[4];
            tempBackUp[1, 5] = tempTwo[5];

            tempBackUp[2, 0] = tempThree[0];
            tempBackUp[2, 1] = tempThree[1];
            tempBackUp[2, 2] = tempThree[2];
            tempBackUp[2, 3] = tempThree[3];
            tempBackUp[2, 4] = tempThree[4];
            tempBackUp[2, 5] = tempThree[5];

            nothing = tempBackUp;

        }
        return nothing;
    }

    void OnApplicationQuit()
    {
        BackUPCropDetails();
    }

}
