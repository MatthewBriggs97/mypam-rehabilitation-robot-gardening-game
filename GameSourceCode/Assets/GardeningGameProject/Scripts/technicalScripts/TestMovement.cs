﻿using UnityEngine;


/// <summary>
/// This major <c>TestMovement</c> class.
/// No longer used but was used as a second option for the camera.
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class TestMovement : MonoBehaviour
{

    public GameObject player;
    public float cameraHeight = 20.0f;

    void Update()
    {
        Vector3 pos = player.transform.position;
        pos.y = cameraHeight;
        transform.position = pos;
    }
}

