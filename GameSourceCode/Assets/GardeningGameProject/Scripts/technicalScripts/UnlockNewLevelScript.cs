﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This <c>UnlockNewLevelScript</c> class.
/// This class should be ran before a new game
/// is played for the first time
/// it is called like many others by going 
/// UnlockNewLevelScript.started = true;
/// There is an array in the editor containing pictures
/// this should match up with the game name given 
/// by the unlockableItems array defined in the file.
/// These determine the order of games that are unlocked.
/// 
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class UnlockNewLevelScript : MonoBehaviour
{
    public static bool started = false;

    public GameObject finalDispalyText;

    public Transform explosionPosition;

    public ParticleSystem playerExplosion;

    public Texture[] images;

    private int rotateAmount = 0;
    private int startRotateAmount = 0;
    private int sum = 0;

    public GameObject newGameScreen;

    private int numberOfGamesUnlocked = 0;

    private string[] unlockableItems = new string[] { "The Dot To Dot Game", "The Football Game","The Catterpillar Game", "The Fishing Game",
                                                     "The Plane Game", "The Space Game"};


    private float maxSizeX = 1;
    private float maxSizeY = 1;
    private float maxSizeZ = 1;


    void Start()
    {
        numberOfGamesUnlocked = PlayerPrefs.GetInt("numberOfGamesUnlocked", 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (started)
        {

            /// moves the target to the new location
            Movement.MoveToPostion = new Vector3(-31.95f, 31.2f, 40.5f);
            Movement.MoveToRotation = new Vector3(27.33f, 45.125f, 0f);


            started = false;


            StartCoroutine(showNewLevel());
        }
    }

    /// <summary>
    /// this will set the text shown on the screen to be what you have unlocked
    /// and change the texture on the cube surface to be the picture in the array 
    /// in the editor, it will then spin it around really fast, slowing down
    /// until it comes to a stop exactly centred.
    /// </summary>
    /// <returns></returns>
    private IEnumerator showNewLevel()
    {
        
        yield return new WaitForSeconds(5);
        finalDispalyText.SetActive(true);
        finalDispalyText.transform.GetChild(1).GetComponent<Text>().text = "You've unlocked " + unlockableItems[numberOfGamesUnlocked];
        yield return new WaitForSeconds(3);

        // sets the explosion to where the item will appear from
        explosionPosition.transform.localPosition = new Vector3(103.38f, 12.6f, -80.2f);
        playerExplosion.Play(true);


        finalDispalyText.SetActive(false);
        newGameScreen.SetActive(true);
        // sets the picture from the editor array
        newGameScreen.GetComponent<Renderer>().material.mainTexture = images[numberOfGamesUnlocked];

        Transform temp = newGameScreen.transform;

        maxSizeX = temp.localScale.x;
        maxSizeY = temp.localScale.y;
        maxSizeZ = temp.localScale.z;

        temp.localScale = new Vector3(0, 0, 0);

        rotateAmount = 80;

        startRotateAmount = rotateAmount;


        for (int i = 0; i < startRotateAmount; i++)
        {
            // this just gradually scales the object up to normal size
            // and spins it quickly, slowing down until it stops
            // this is the only bit of decent maths
            // since you had to work out
            // (n(n-1))/2 = k    (where k is divisible by 360)
            // since we are doing a summation to find out if it will stop spinning
            // when it is looking at the camera
            // so that every time the rectangle finishes spinning it is facing the user
            
            temp.Rotate(Vector3.right * rotateAmount, Space.World);
            sum += rotateAmount;
            temp.localScale += new Vector3(maxSizeX / startRotateAmount, maxSizeY / startRotateAmount, maxSizeZ / startRotateAmount);
            yield return new WaitForSeconds(0.02f);
            rotateAmount -= 1;
        }
        yield return new WaitForSeconds(5);
        finalDispalyText.SetActive(false);
        newGameScreen.SetActive(false);

        numberOfGamesUnlocked++;
        PlayerPrefs.SetInt("numberOfGamesUnlocked", numberOfGamesUnlocked);

        OverScenesManager.level += 1;


    }
}