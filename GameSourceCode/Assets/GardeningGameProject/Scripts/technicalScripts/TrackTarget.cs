﻿using UnityEngine;


/// <summary>
/// This <c>TrackTarget</c> class.
/// This will make the object of what this script is attatched follow the target.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class TrackTarget : MonoBehaviour
{
    public GameObject target;

    /// <summary>
    /// called every frame and follows the target when it moves.
    /// </summary>
    void Update()
    {
        if (Vector3.Distance(transform.position, target.transform.position) > 0.1f && DoToDoCore.DemoDotToDot)
        {
            float Step = Time.deltaTime * OverScenesManager.Speed;

            transform.transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Step);
        }
    }
}
