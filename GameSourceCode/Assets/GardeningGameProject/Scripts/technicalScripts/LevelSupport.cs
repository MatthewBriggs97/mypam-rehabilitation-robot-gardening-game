﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This Support <c>LevelSupport</c> class.
/// has a slide show of images for the advice for the user.
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class LevelSupport : MonoBehaviour
{
    private int BackUpOfLevel = -1;
    public static int Level;
    private bool gameModePlay = false;
    private WaitForSeconds delay;

    void Start()
    {
        gameModePlay = OverScenesManager.gameMode;
        delay = new WaitForSeconds(0.01f);
    }

    void LateUpdate()
    {
        if (Level != BackUpOfLevel && !gameModePlay)
        {
            StartCoroutine(Details());
        }
    }


    /// <returns>the slideshow of text</returns>
    IEnumerator Details()
    {
            //is told when to show the aninmation by the MoveLogic
            if (Level >= 1)
            {
                transform.GetChild(Level - 1).gameObject.SetActive(true);

                for (int i = 0; i < (Level - 1); i++)
                {
                    if (transform.GetChild(i).gameObject.activeSelf == true)
                    {
                        transform.GetChild(i).gameObject.SetActive(false);
                    }
                }
                yield return delay;
            }
            else if (Level == 0)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).gameObject.activeSelf == true)
                    {
                        transform.GetChild(i).gameObject.SetActive(false);
                    }
                }
                yield return delay;
            }
            else
            {
                transform.gameObject.SetActive(false);
            }
            BackUpOfLevel = Level;
    }
}