﻿using System;
using UnityEngine;
using System.IO;
using System.Collections;

/// <summary>
/// This graphing <c>DataRecordGardeningGame</c> class.
/// No longer used, but can be used to store data to be graphed
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class DataRecordGardeningGame : MonoBehaviour
{
    public GameObject Target;

    private Transform TargetT;
    private string path;
    private float tempTime;

    private StreamWriter file;

    void Start()
    {
        TargetT = Target.transform;

        StartCoroutine(RecordLocation("1", 1000));
    }

    /// <summary>
    /// Records the player and targets locations for graphing.
    /// </summary>
    /// <param name="Level"></param>
    /// <param name="PlotAmounts"></param>
    /// <returns>file with the targets and players locations over time played</returns>
    IEnumerator RecordLocation(string Level, int PlotAmounts)
    {
        path = Application.dataPath + @"\..\" + Level + ".txt";
        if (!File.Exists(path))
        {
            // Create a file to write to.
            using (file = File.CreateText(path))
            {
                file.WriteLine("PatientX,PatientY,TargetX,TargetY");
            }
            file = new StreamWriter(path, true);

            for (int i = 0; i < PlotAmounts; i++)
            {
                file.WriteLine(Convert.ToString(transform.position.x) + "," + Convert.ToString(transform.position.z) + ","
                    + Convert.ToString(TargetT.position.x) + "," + Convert.ToString(TargetT.position.z));

                file.Flush();

                yield return new WaitForSeconds(0.01f);
            }
            file.Close();
        }

    }

}
