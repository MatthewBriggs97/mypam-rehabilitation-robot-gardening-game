﻿using UnityEngine;

/// <summary>
/// This <c>DayNightCycle</c> class.
/// This is a small animation that runs to show to the player a new day has 
/// passed, it does not actually do anyhthing to any in game variables
/// but is used to show that some time has passed so that the story can
/// progress and some of the plants an grow further
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class DayNightCycle : MonoBehaviour
{


    public static bool started = false; // just set started = true to start the animation

    public GameObject creatorNames;

    private int counter = 9999; // when at 9999 none of the animation will run, only when the counter is at 0 it will run

    public Material[] mats; // this is an array in the editor, it contains all of the materials that are used for the skybox
                            // some are repeated to match how the sun moves in the game, or else the sky would move 
                            // too fast for the sunlight


    // Update is called once per frame
    /// <summary>
    /// moves the sun round in a circle to simulate the day and the night , also changing the skybox to match the darkness
    /// </summary>
    void FixedUpdate()
    {

        if (started)
        {
            
            Movement.MoveToPostion = new Vector3(213.55f, 21.6f, 343.2f); // this shows the whole map and is used to get a good view of the shadows
            Movement.MoveToRotation = new Vector3(5.3f, -132.4f, 0f);

            counter = 0; // sets the coutner to 0 to begin the animation

            started = false;

        }
        if (counter < 1200)
        {
            counter++;
        }

        if(counter == 800)
        {
            // now the creator names should show up when it is dark, so they look like they are fading in.
            creatorNames.SetActive(true);
        }
        if (counter < 1200 && counter > 500)
        {
            
            transform.Rotate(Vector3.left * Time.deltaTime * 30f, Space.World); // rotates the point light which acts as the sun
            

            RenderSettings.skybox = mats[(counter-500) / 40]; // this just means that the image will change every 40 fixed frames, this nicely syncs with the sun
        }
        if (counter == 1200)
        {
            transform.eulerAngles = new Vector3(-251.25f,311.3f,0f); // this puts the sun exactly back to the rotation it started on
                                                                     // this is to avoid, after lots of cycles the sun being mis aligned
                                                                     // since if it only rotated 359 degrees instead of 360, after a lot 
                                                                     // of time it would change all of the shadows and things
            OverScenesManager.level++;
            counter = 9999;
            creatorNames.SetActive(false);

        }




    }



}