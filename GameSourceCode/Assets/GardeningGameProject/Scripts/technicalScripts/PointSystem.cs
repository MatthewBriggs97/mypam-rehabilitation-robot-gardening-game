﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// <c>PointSystem</c> class.
/// Displays the points on the screen.
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class PointSystem : MonoBehaviour
{
    public static int ScoreValue = 0;

    private Text Score;

    private int BackUpScore = 0;

    private float maxSize;
    private bool Growing = false;

    public float GrowFactor = 2;

    private int Interval = 3;

    private int mainGameScore;

    void Start()
    {
        Score = GetComponent<Text>();
        mainGameScore = PlayerPrefs.GetInt("mainGameScore", 0);
        ScoreValue = mainGameScore;         
        maxSize = (transform.parent.gameObject.GetComponent<Transform>().localScale.x) + 0.5f;

    }
    /// <summary>
    /// updates the score after everyother calulation is down to not affect performce or anaimations
    /// </summary>
    void LateUpdate()
    {
        if (ScoreValue != BackUpScore && (Time.frameCount % Interval) == 0)
        {
            Score.text = "Points: " + ScoreValue.ToString();
            if (Growing == false)
            {
                PlayerPrefs.SetInt("mainGameScore", ScoreValue); // updates the score so that it is always saved over scenes
                Growing = true;
                StartCoroutine(Growth());
            }
            BackUpScore = ScoreValue;
        }
    }


    void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("mainGameScore", ScoreValue);
    }


    /// <summary>
    /// A method to make the crop grow.
    /// </summary>
    /// <returns></returns>
    IEnumerator Growth()
    {
        WaitForSeconds delay = new WaitForSeconds(0.02f);
        while (maxSize > transform.parent.gameObject.GetComponent<Transform>().localScale.x)
        {
            transform.parent.gameObject.GetComponent<Transform>().localScale += Vector3.one * Time.deltaTime * GrowFactor;
            yield return delay;
        }
        StartCoroutine(Shrink());
    }



    IEnumerator Shrink()
    {
        WaitForSeconds delay = new WaitForSeconds(0.02f);
        while (1 < transform.parent.gameObject.GetComponent<Transform>().localScale.y)
        {
            transform.parent.gameObject.GetComponent<Transform>().localScale -= Vector3.one * Time.deltaTime * GrowFactor * 1.5f;
            yield return delay;
        }

        Growing = false;
    }
}

