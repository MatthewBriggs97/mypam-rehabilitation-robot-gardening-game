﻿using UnityEngine;

/// <summary>
/// This <c>LevelSlideSelector</c> class.
/// This selects what side should be displayed for the level been played.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class LevelSlideSelector : MonoBehaviour {

    public static int SlideLevel = 0;


	// Update is called once per frame
	void Update () {

        for (int i = 0; i < transform.childCount; i++)
        {
            if(i == (SlideLevel - 1))
            {
                if (transform.GetChild(i).gameObject.activeSelf == false)
                {
                    transform.GetChild(i).gameObject.SetActive(true);
                }
            }
            else if(transform.GetChild(i).gameObject.activeSelf == true && i != (SlideLevel - 1))
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }

    }
}
