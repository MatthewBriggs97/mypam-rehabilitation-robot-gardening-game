﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This major <c>Movement</c> class.
/// Contains all methods for moving the camera around from the intro to all over locations.
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class Movement : MonoBehaviour
{
    public int startLevel = 0;

    public static float speed = 400;
    public float rotationFactor = 2;

    public static Vector3 MoveToPostion = new Vector3(0, 75.2f, -17);
    public static Vector3 MoveToRotation = new Vector3(54.4f, 0, 0);

    private Quaternion initialRotation;
    private float distanceForCameraJourney;
    private float timeForCameraJourney;
    private Vector3 lastTarget;
    private float timeAtStart;


    public GameObject Welcome;

    public static bool started = false;

    public GameObject PlayerInstruction1;

    public GameObject ShowTools;
    public GameObject secondInfomation;

    public bool skipIntro = false;

    public GameObject wateringCan;
    public GameObject seedBag;
    public GameObject rake;
    public GameObject weedKiller;



    /// <summary>
    /// Checks when the level is -1, therefore the end.
    /// </summary>
    /// 
    void Update()
    {
        Move();


        if (started)
        {
            started = false;
            StartCoroutine(Helper());
        }

    }
    /// <summary>
    /// A method to move a object to a given space.
    /// </summary>
    void Move()
    {
        if (MoveToPostion != lastTarget)
        {
            initialRotation = transform.rotation;
            distanceForCameraJourney = Vector3.Distance(transform.position, MoveToPostion);
            if (PlaneGame.inThePlaneGame == false)
            {
                speed = distanceForCameraJourney / 5;
            }
            timeForCameraJourney = distanceForCameraJourney / speed;
            timeAtStart = Time.time;

        }
        lastTarget = MoveToPostion;

        if (Vector3.Distance(transform.position, MoveToPostion) > 0.1f || (transform.eulerAngles.x - Quaternion.Euler(MoveToRotation).x) > 0.1f)
        {
            float step = speed * Time.deltaTime;

            if (skipIntro == true)
            {
                step = 10;
            }
            transform.position = Vector3.MoveTowards(transform.position, MoveToPostion, step);
            transform.rotation = Quaternion.Slerp(initialRotation, Quaternion.Euler(MoveToRotation), ((Time.time - timeAtStart) / timeForCameraJourney));
        }
    }

    /// <summary>
    /// Called at the start of the game to move the camera to the right postions.
    /// </summary>
    /// <returns>Camera to the right postion</returns>
    IEnumerator Helper()
    {

        if (true) // the previous conditions were a bit meaningless so I got rid of them
        {
            Welcome.SetActive(true);




            if (skipIntro == false)
            {
                OverScenesManager.PlayLabView = true;

                Welcome.transform.GetChild(1).GetComponent<Text>().text = "Here are all the tools you need to use";
                if (MoveLogic.inDemo) { yield return new WaitForSeconds(6f); }
                Welcome.SetActive(false);

                ShowTools.SetActive(true);

                seedBag.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(seedBag.GetComponent("Halo"), true, null);
                ShowTools.transform.GetChild(1).GetComponent<Text>().text = "Seed Bag";

                if (MoveLogic.inDemo) { yield return new WaitForSeconds(3f); }
                ShowTools.transform.GetChild(1).GetComponent<Text>().text = "Rake";
                ShowTools.transform.GetComponent<RectTransform>().anchoredPosition += new Vector2(-10, 100);
                rake.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(rake.GetComponent("Halo"), true, null);
                seedBag.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(seedBag.GetComponent("Halo"), false, null);

                if (MoveLogic.inDemo) { yield return new WaitForSeconds(3f); }
                rake.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(rake.GetComponent("Halo"), false, null);
                wateringCan.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(wateringCan.GetComponent("Halo"), true, null);
                ShowTools.transform.GetChild(1).GetComponent<Text>().text = "Watering Can";
                ShowTools.transform.GetComponent<RectTransform>().anchoredPosition += new Vector2(-10, 70);

                if (MoveLogic.inDemo) { yield return new WaitForSeconds(3f); }
                ShowTools.transform.GetChild(1).GetComponent<Text>().text = "Weed Killer";
                weedKiller.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(weedKiller.GetComponent("Halo"), true, null);
                wateringCan.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(wateringCan.GetComponent("Halo"), false, null);
                ShowTools.transform.GetComponent<RectTransform>().anchoredPosition += new Vector2(-10, 60);

                if (MoveLogic.inDemo) { yield return new WaitForSeconds(3f); }
                ShowTools.transform.GetChild(1).GetComponent<Text>().text = "";
                weedKiller.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(weedKiller.GetComponent("Halo"), false, null);


                ShowTools.SetActive(false);

                if (MoveLogic.inDemo) { yield return new WaitForSeconds(3f); }
                ShowTools.SetActive(false);

                GameObject player = GameObject.Find("Test player");
                GameObject target = GameObject.Find("Target");
                player.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(player.GetComponent("Halo"), true, null);
                target.GetComponent("Halo").GetType().GetProperty("enabled").SetValue(target.GetComponent("Halo"), true, null);

                PlayerInstruction1.SetActive(true);

                if (MoveLogic.inDemo) { yield return new WaitForSeconds(3f); }


                PlayerInstruction1.SetActive(false);


            }

        }
        skipIntro = false;
        OverScenesManager.level += 1;
        MoveLogic.inDemo = true;
    }

}
