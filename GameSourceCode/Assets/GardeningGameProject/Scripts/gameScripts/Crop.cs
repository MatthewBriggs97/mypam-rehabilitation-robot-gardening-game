﻿using System.Collections;
using UnityEngine;



/// <summary>
/// This point <c>Crop</c> class.
/// Controls the points from colliding with a crop.
/// </summary>
/// <remarks>
/// <para>This will chck what tool collides then give a relevent change, such as growning</para>
/// </remarks>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class Crop : MonoBehaviour
{

    public Material WithSeeds;

    public float maxSize;
    public float growFactor;

    private float scale = 0;
    private float VegScale;
    private bool Growing;

    /// <summary>
    /// When the tool hits the crop.
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "HoePrefab")
        {
            scale = GetComponent<Transform>().localScale.x;
            if (scale < 0.7)
            {
                GetComponent<Transform>().localScale *= 2;
            }
            PointSystem.ScoreValue += 1;
            StartCoroutine(Affect());
            if (PointSystem.ScoreValue == 25)
            {
                PointSystem.ScoreValue += 5;
            }
        }
        else if (other.name == "SeedSack")
        {
            transform.GetChild(0).gameObject.SetActive(true);
            PointSystem.ScoreValue += 1 * ((int)GetComponent<Transform>().localScale.x);
        }
        else if (other.name == "WateringCanPrefab")
        {
            if (GetComponent<Renderer>().material.name != "With_Seed")
            {
                GetComponent<Renderer>().material = WithSeeds;
            }
            if (Growing == false && transform.GetChild(0).gameObject.activeSelf == true)
            {
                StartCoroutine(Growth());
                Growing = true;
            }
        }
    }
    /// <summary>
    /// A method to make the crop grow.
    /// </summary>
    /// <returns></returns>
    public IEnumerator Growth()
    {
        float timer = 0;

        while (maxSize > transform.GetChild(0).transform.localScale.x)
        {
            timer += Time.deltaTime;
            transform.GetChild(0).transform.localScale += new Vector3(1, 1, 1) * Time.deltaTime * growFactor;
            yield return new WaitForSeconds(0.02f);
            if (LevelChoice.NextLevel == 3)
            {
                PointSystem.ScoreValue += 1;
            }
        }
    }

    IEnumerator Affect()
    {
        transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        transform.GetChild(1).gameObject.SetActive(false);
    }
}
