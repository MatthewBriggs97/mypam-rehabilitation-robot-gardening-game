﻿using UnityEngine;


/// <summary>
/// <c>Rotate</c> class.
/// Rotates a object at a set speed
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class Rotate : MonoBehaviour
{

    public float speed = 2f;

    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * speed);
    }
}
