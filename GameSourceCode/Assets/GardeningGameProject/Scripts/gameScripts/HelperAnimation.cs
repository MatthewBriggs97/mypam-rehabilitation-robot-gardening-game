﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This major <c>HelperAnimation</c> class.
/// Used to active picutures one after the other. Can be used for a lot of affects.
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class HelperAnimation : MonoBehaviour
{
    public float WaitTime = 2f;

    public int LevelSlide;

    void Start()
    {
        StartCoroutine(ImageRotate());
    }

    /// <returns>roll like slide show of images</returns>
    IEnumerator ImageRotate()
    {
        while (true)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.activeSelf == true)
                {
                    transform.GetChild(i).gameObject.SetActive(false);

                    if (i == transform.childCount - 1)
                    {
                        transform.GetChild(0).gameObject.SetActive(true);
                    }
                    else
                    {
                        transform.GetChild(i + 1).gameObject.SetActive(true);
                    }

                }
                yield return new WaitForSeconds(WaitTime);

            }
        }
    }
}
