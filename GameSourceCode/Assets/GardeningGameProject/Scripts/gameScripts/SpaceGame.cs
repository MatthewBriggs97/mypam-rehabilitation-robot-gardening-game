﻿using System.Collections;
using UnityEngine;


/// <summary>
/// This <c>SpaceGame</c> class.
/// This simplle script ensure the starting of the space game. 
/// Acts like a bounce between. Like ping pong.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class SpaceGame : MonoBehaviour
{
    public static bool started = false;

    /// <summary>
    /// Called everyframe and checks if its time to play the space game.
    /// </summary>
    void Update()
    {
        if (started)
        {
            Movement.MoveToPostion = new Vector3(305f, 27.23f, 398.07f);
            Movement.MoveToRotation = new Vector3(3.78f, -129.75f, 0f);
            started = false;


            StartCoroutine(StartSpaceGame());
        }
    }
    /// <summary>
    /// When called it will get the camera to position and play the game.
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartSpaceGame()
    {

        yield return new WaitForSeconds(6);
        Movement.MoveToPostion = new Vector3(236.8f, 3.24f, 371.03f);
        Movement.MoveToRotation = new Vector3(6.7f, -106.2f, 0f);
        yield return new WaitForSeconds(3);


        OverScenesManager.NextScene = "Falling Game";
        OverScenesManager.NextPlayerName = "SpaceshipFighter";
        OverScenesManager.NextTargetName = "Target";

        OverScenesManager.LoadOverScene = true;

    }
}