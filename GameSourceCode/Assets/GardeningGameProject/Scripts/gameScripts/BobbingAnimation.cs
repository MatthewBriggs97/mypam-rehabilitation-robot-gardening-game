﻿using UnityEngine;

/// <summary>
/// This <c>bobbingAnimation</c> class.
/// Makes the fishing boat bob up and down in the water.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class BobbingAnimation : MonoBehaviour
{
    /// <summary>
    /// All values in this are for setting it to work with our water lay out.
    /// </summary>
    public float bobAmount;
    public float maxHeight;
    public float minHeight;
    private float targetHeight;

    private bool goingUp = true;
    private float cBobAmount = 0;

    /// <summary>
    /// Made for animations fixed update here is used for animating the boat up and down
    /// </summary>
    void FixedUpdate()
    {
        if(FishingLevelTarget.DemoFishing || FishingLevelTarget.fishingLevel)
        {

            if (goingUp)
            {
                cBobAmount = bobAmount;
                targetHeight = maxHeight;
                if (transform.position.y < targetHeight)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y + cBobAmount, transform.position.z);

                    cBobAmount = cBobAmount * 0.95f;
                }
                else
                {
                    goingUp = false;
                }
            }
            else
            {
                targetHeight = minHeight;
                if (transform.position.y > targetHeight)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y - bobAmount, transform.position.z);

                    cBobAmount = bobAmount * 0.95f;
                }
                else
                {
                    goingUp = true;
                }
            }
        }
    }
}
