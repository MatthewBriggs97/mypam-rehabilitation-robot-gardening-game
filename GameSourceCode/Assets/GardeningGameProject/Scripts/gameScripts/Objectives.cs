﻿using System.Collections;
using UnityEngine;

/// <summary>
/// This <c>objectives</c> class.
/// This simplle script ensure the object that collides with this object is sent to the other. 
/// Acts like a bounce between. Like ping pong.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class Objectives : MonoBehaviour
{
    /// <summary>
    /// Where the object that collids will be sent to
    /// </summary>
    public GameObject NextObject;

    /// <summary>
    /// records the amount of time it has been hit.
    /// </summary>
    public static int TimeHit = 0;

    /// <summary>
    /// When hit by the pllayer it adds a new time hit and endables the next objects colider
    /// Also calls method to move the target
    /// </summary>
    /// <param name="other">The player</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent.name == "Test player" && NextObject.name != "Target")
        {
            if (transform.name == "TargetOne")
            {
                TimeHit += 1;
            }
            //switching
            transform.GetComponent<BoxCollider>().enabled = false;
            NextObject.GetComponent<BoxCollider>().enabled = true;
            StartCoroutine(NextTarget());
        }
    }

    IEnumerator NextTarget()
    {
        yield return new WaitForSeconds(0.60f);//If change ensure you change the wait in the method
        //MoveCrops in RakingLevel script to the same or will break
        TargetMovement.TargetMoveto = NextObject.transform.position;
    }
}
