﻿using System.Collections;
using UnityEngine;

/// <summary>
/// <c>ButterflyMovement</c> class.
/// Contains all methods for moving the butterflys.
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class ButterflyMovement : MonoBehaviour
{
    // declare transform
    private Transform tr;
    public Vector3 defaultRotation = new Vector3(0, -15, 6);

    private Vector3 prevPos;
    public float threshold = 0.5f;
    private float turnSpeed = 20f;
    public float speed = 2f;

    public float maxX;
    public float minX;
    public float maxY;
    public float minY;
    public float maxZ;
    public float minZ;

    private Vector3 Place;

    //new optomises
    private Vector3 moveDirection;
    private Quaternion targetRotation;
    private Renderer myRenderer;
    private float angle = 0.0f;

    /// <summary>
    ///  saving locations of past and current location
    /// </summary>
    void Start()
    {
        // get transform, as we will need to set the position from labview
        tr = transform;

        prevPos = tr.position;
        if (tr.GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetComponent<Renderer>();
        }
        else if (tr.GetChild(0).GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetChild(0).GetComponent<Renderer>();
        }
        else if (tr.GetChild(1).GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetChild(1).GetComponent<Renderer>();
        }
        StartCoroutine(ButterFlyMovement());
    }
    /// <summary>
    /// FixedUpdate used for animations
    /// </summary>
    void FixedUpdate()
    {
        if (myRenderer.isVisible)
        {
            //turning
            moveDirection = tr.position - prevPos;

            if (moveDirection.magnitude / Time.fixedDeltaTime > threshold)
            {
                angle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg;
                targetRotation = Quaternion.AngleAxis(angle + 180, Vector3.up);
            }
            else
            {
                //targetRotation = Quaternion.Euler(new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z));
                targetRotation = Quaternion.Euler(transform.eulerAngles);
            }


            tr.rotation = Quaternion.Slerp(tr.rotation, targetRotation, Time.fixedDeltaTime * turnSpeed);

            prevPos = tr.position;
        }
    }

    /// <summary>
    ///  This moves the butterfly (duh)
    /// </summary>
    /// <returns></returns>
    IEnumerator ButterFlyMovement()
    {
        WaitForSeconds delay = new WaitForSeconds(0.02f);
        WaitForSeconds delayTwo = new WaitForSeconds(0.2f);
        
        float step = 0;

        for (int i = 0; i < 1000; i++)
        {

            Place = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), Random.Range(minZ, maxZ));
            
            step = speed * Time.deltaTime;

            while (Vector3.Distance(transform.position, Place) > 0.1f)
            {
                transform.transform.position = Vector3.MoveTowards(transform.transform.position, Place, step);
                yield return delay;
            }
            yield return delayTwo;
        }

    }

}
