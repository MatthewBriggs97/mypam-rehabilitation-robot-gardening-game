﻿using UnityEngine;


/// <summary>
/// This main <c>Animation</c> class.
/// Animations for the tools and other objects uses this.
/// </summary>
/// <author email="ll15m3b@leeds.ac.uk">Matthew Briggs</author>
public class Animation : MonoBehaviour
{
    // declare transform
    private Transform tr;
    public Vector3 defaultRotation = new Vector3(0, -15, 6);

    private Vector3 prevPos;
    public float threshold = 1.7f;
    public float turnSpeed = 10f;

    public bool Rotate = true;


    //new optomises
    private Vector3 moveDirection;
    private Quaternion targetRotation;
    private Renderer myRenderer;
    private float angle = 0.0f;
    private Vector3 rotateAngle = new Vector3(15, 30, 45);

    /// <summary>
    ///  Use this for initialization
    /// </summary>
    void Start()
    {
        // get transform, as we will need to set the position from labview
        tr = transform;

        prevPos = tr.position;
        if(tr.GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetComponent<Renderer>();
        }
        else if(tr.GetChild(0).GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetChild(0).GetComponent<Renderer>();
        }
        else if (tr.GetChild(1).GetComponent<Renderer>() != null)
        {
            myRenderer = tr.GetChild(1).GetComponent<Renderer>();
        }
    }


    /// <summary>
    ///  Update is called once per frame
    ///  Called by a fixed time to ensure safe aniamations
    /// </summary>
    void FixedUpdate()
    {
        if (myRenderer.isVisible)
        {
            //turning
            moveDirection = tr.position - prevPos;

            if (moveDirection.magnitude / Time.fixedDeltaTime > threshold)
            {
                angle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg;
                targetRotation = Quaternion.AngleAxis(angle + 90, Vector3.up);
            }
            else
            {
                if (transform.parent.name != "Test player" && Rotate)
                {
                    transform.Rotate(rotateAngle * Time.deltaTime);
                }
                //targetRotation = Quaternion.Euler(new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z));
                targetRotation = Quaternion.Euler(transform.eulerAngles);
            }


            tr.rotation = Quaternion.Slerp(tr.rotation, targetRotation, Time.fixedDeltaTime * turnSpeed);

            prevPos = tr.position;
        }
    }
}
