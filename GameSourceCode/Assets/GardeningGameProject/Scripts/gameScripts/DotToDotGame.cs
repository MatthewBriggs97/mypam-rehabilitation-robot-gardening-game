﻿using System.Collections;
using UnityEngine;
/// <summary>
/// This <c>DotToDotGame</c> class.
/// This script simply moves the camera to the bench
/// and starts the scene to load so that the dot to dot game
/// will play
/// pretty simple, the game logic for the dot to dot is in DotToDotCore
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class DotToDotGame : MonoBehaviour
{
    public static bool started = false; // as with a lot of the other ones, just call with DotToDotGame.started = true;
    public GameObject player;

    // Update is called once per frame
    void Update()
    {
        if (started)
        {
            Movement.speed = 10f;
            Movement.MoveToPostion = new Vector3(20f, 2.38f, 2.6f);
            Movement.MoveToRotation = new Vector3(7.5f, 146f, 0f);
            started = false;


            StartCoroutine(StartDotToDoTGame());
        }
    }

    /// <summary>
    /// sets up over scenes manager and zooms in on the bench                                                                            
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartDotToDoTGame()
    {

        yield return new WaitForSeconds(5f);
        Movement.MoveToPostion = new Vector3(24.67f,1.29f,-1.35f); // this just zooms in further into the bench
        Movement.MoveToRotation = new Vector3(71.6f,-179.6f,0f);
        Movement.speed = 1f;
        yield return new WaitForSeconds(5f);


        OverScenesManager.NextScene = "dotToDot";
        OverScenesManager.NextPlayerName = "player";
        OverScenesManager.NextTargetName = "target";
        OverScenesManager.LoadOverScene = true; // setting up the correct varialbes so that the MyPAM knnows what to control and what to follow

    }
}