﻿using UnityEngine;


/// <summary>
/// This <c>MoveBox</c> class.
/// Contains all methods for moving the spacegame objects, the box.
/// <list type="bullet">
/// <item>
/// <term>Update</term>
/// <description>Moves the box everyframe</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class MoveBox : MonoBehaviour {

    /// <summary>
    /// Target to move to place
    /// </summary>
    public Transform target;

    /// <summary>
    /// The speed of the movement
    /// </summary>
    public float speed;

    /// <summary>
    /// Called everyframe moves one object to another by a set speed
    /// </summary>
    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }
}
