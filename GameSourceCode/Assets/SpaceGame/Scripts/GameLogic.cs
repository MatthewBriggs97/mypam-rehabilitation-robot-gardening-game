﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This <c>GameLogic</c> class.
/// Contains all methods for handling the whole space game logic.
/// <list type="bullet">
/// <item>
/// <term>GameLogicMethod</term>
/// <description>The main power lifter of the space game. Judges all of the levels to be loaded and makes the holes smaller</description>
/// </item>
/// <item>
/// <term>UpdateScore</term>
/// <description>Updates the current score</description>
/// </item>
/// <item>
/// <term>PlayTheGame</term>
/// <description>Calls the relevent methods to start the game assistance</description>
/// </item>
/// <item>
/// <term>PlayerDead</term>
/// <description>Calls methods to show explotion and ends the game</description>
/// </item>
/// <item>
/// <term>startSequence</term>
/// <description>Starts the space game with text welcomming the user</description>
/// </item>
/// <item>
/// <term>RestafterTime</term>
/// <description>Restarts the game</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class GameLogic : MonoBehaviour
{
    /// <summary>
    /// There is a few text variables to set the score and welcome the player
    /// </summary>
    public Text scoreText;
    public GameObject endGameText;

    /// <summary>
    /// For the playr to put the name in
    /// </summary>
    public GameObject inputField;

    public float speed;

    public GameObject game;

    public GameObject objective;

    /// <summary>
    /// A few objects for each level, with each having differnt sized holes
    /// </summary>
    public GameObject levelOne;
    public GameObject levelTwo;
    public GameObject levelThree;
    public GameObject levelFour;
    public GameObject levelFive;
    public GameObject levelSix;
    public List<GameObject> lvlList = new List<GameObject>();

    /// <summary>
    /// the player for the space game
    /// </summary>
    public GameObject Player;

    private GameObject ob;

    private List<GameObject> listObstaclesOne = new List<GameObject>();
    private List<GameObject> listObstaclesTwo = new List<GameObject>();
    private List<GameObject> listObstaclesThree = new List<GameObject>();
    private List<GameObject> listObstaclesFour = new List<GameObject>();
    private List<GameObject> listObstaclesFive = new List<GameObject>();
    private List<GameObject> listObstaclesSix = new List<GameObject>();

    private List<GameObject> Temp = new List<GameObject>();

    /// <summary>
    /// The list of each level, and each level is a list of there tiles which are obsticals
    /// </summary>
    private List<List<GameObject>> listLevels = new List<List<GameObject>>();

    private int random;
    private int levelCounter;

    private MoveBox test;

    private List<int> rotation = new List<int>();

    private Vector3 target;

    private int spaceGameHighScore;

    /// <summary>
    /// On start up this will be called and will fill the lists and set default scores as well as rotations
    /// </summary>
    void Start()
    {

        spaceGameHighScore = PlayerPrefs.GetInt("spaceGameHighScore", 0);
        transform.position = new Vector3(0f, 0.5f, 0f);

        //scoreText = GameObject.Find("PointsBackGround").transform.GetChild(0).GetComponent<Text>();

        //adding all of the obsticals to a list, for each level
        for (int i = 0; i < levelOne.transform.childCount; i++)
        {
            listObstaclesOne.Add(levelOne.transform.GetChild(i).gameObject);
            listObstaclesTwo.Add(levelTwo.transform.GetChild(i).gameObject);
            listObstaclesThree.Add(levelThree.transform.GetChild(i).gameObject);
            listObstaclesFour.Add(levelFour.transform.GetChild(i).gameObject);
            listObstaclesFive.Add(levelFive.transform.GetChild(i).gameObject);
            listObstaclesSix.Add(levelSix.transform.GetChild(i).gameObject);
        }
        //adding the lists to a big list
        listLevels.Add(listObstaclesOne);
        listLevels.Add(listObstaclesTwo);
        listLevels.Add(listObstaclesThree);
        listLevels.Add(listObstaclesFour);
        listLevels.Add(listObstaclesFive);
        listLevels.Add(listObstaclesSix);

        //adding the rotations to a list 
        rotation.Add(0);
        rotation.Add(90);
        rotation.Add(180);
        rotation.Add(270);

        //AudioSettings temp to be used later
        Temp = listObstaclesOne;

        //calling main game logic
        GameLogicMethod();

        test = game.GetComponent<MoveBox>();

        StartCoroutine(StartSequence());

    }

    /// <summary>
    /// Called every frame move the target to the hole
    /// </summary>
    void Update()
    {
        if (levelCounter != 0 && test.speed != 0)
        {
            float step = speed * Time.deltaTime;
            objective.transform.position = Vector3.MoveTowards(objective.transform.position, target, step);
        }
    }

    /// <summary>
    /// when the player hits something this will be called, checks if it hit the objective or the walls
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Objective")
        {//if hit the objective it will give points
            ob.SetActive(false);
            test.speed = 1.15f;
            GameLogicMethod();
            //how many gone through.
            levelCounter += 1;
        }
        else if (other.name == "Target" && test.speed != 0)
        {//if in the other spaceship speeds the game up to make harder for people who are good at the game
            test.speed = 1.75f;
        }
        else if (other.name == "Barrel")
        {
            test.speed = 1.15f;
            other.gameObject.SetActive(false);
            // set state
            PlayTheGame();
            levelCounter += 1;

            UpdateScore();
        }
        else if ((other.name != "Objective") && (other.name != "Target"))
        {//if it hit anything else it will die
            PlayerDead();
        }
    }

    /// <summary>
    /// the power lifter of the space game, judges everything.
    /// Checking the points to load the right text and even load the right level
    /// </summary>
    void GameLogicMethod()
    {
        //levels for the game, choice how big the hole is
        if (levelCounter == -1)
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "You can do better than that...";
            Temp = listObstaclesOne;
        }
        if (levelCounter == 0)
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "Good effort, see if you can do better next time.";
            Temp = listObstaclesOne;
        }
        else if (levelCounter == 4)
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "Not bad, you can get further.";
            Temp = listObstaclesTwo;
        }
        else if (levelCounter == 7)
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "Good effort, quite far";
            Temp = listObstaclesThree;
        }
        else if (levelCounter == 12)
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "Great, you got half way there.";
            Temp = listObstaclesFour;
        }
        else if (levelCounter == 14)
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "Fantastic, soon you'll be able to finish it all.";
            Temp = listObstaclesFive;
        }
        else if (levelCounter == 18)
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "So close! Only a few more before the end!";
            Temp = listObstaclesSix;
        }
        else if (levelCounter == 23)
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "Perfect! Congratulations!";
            PlayerDead();
        }

        //placing new objects from random in the list 
        random = Random.Range(0, Temp.Count);
        ob = Temp[random];



        //spawn in the object
        Vector3 spawn = new Vector3(0, (Player.transform.position.y) - 6, 0);
        ob.transform.position = spawn;

        //rotating the obstical to a random degree
        random = Random.Range(0, rotation.Count);
        random = rotation[random];

        ob.transform.eulerAngles = new Vector3(ob.transform.eulerAngles.x,
            ob.transform.eulerAngles.y + random, ob.transform.eulerAngles.z);

        //setting the target so that assist works
        GameObject obje = ob.transform.Find("Objective").gameObject;
        target = new Vector3(obje.transform.position.x, Player.transform.position.y, obje.transform.position.z);

        //finishlise with setting the whole plane true
        ob.SetActive(true);

        UpdateScore();

    }


    public void UpdateScore()
    {
        scoreText.text = "Current Score: " + (PlayerPrefs.GetInt("mainGameScore", 0) + (levelCounter * 100)).ToString();
    }


    /// <summary>
    /// Bellow deals with starting and ending the game
    /// Also trys update the data base.
    /// </summary>
    void PlayTheGame()
    {
        OverScenesManager.PlayLabView = true;
    }

    /// <summary>
    /// when player dies calls needed moethods to load next level
    /// </summary>
    void PlayerDead()
    {
        test.speed = 0;
        Player.transform.Find("Explosion").gameObject.SetActive(true);
        Player.GetComponent<MeshRenderer>().enabled = false;
        Player.GetComponent<Collider>().enabled = false;

        objective.transform.position = new Vector3(0, 25, 0);
        StartCoroutine(RestafterTime(0));
        PlayerPrefs.SetInt("mainGameScore", (PlayerPrefs.GetInt("mainGameScore", 0)) + ((levelCounter - 1) * 100));


        if (OverScenesManager.gameMode)
        {
            OverScenesManager.gameMode = false;
        }
        else
        {
            OverScenesManager.level += 1;
        }
    }

    /// <summary>
    /// At the start it will introduce you to the game
    /// </summary>
    IEnumerator StartSequence()
    {
        endGameText.SetActive(true);
        endGameText.transform.GetChild(1).GetComponent<Text>().text = "Move the arm to go through all the holes";
        yield return new WaitForSeconds(3f);

        endGameText.transform.GetChild(1).GetComponent<Text>().text = "The high score is " + spaceGameHighScore.ToString();
        yield return new WaitForSeconds(3f);
        endGameText.transform.GetChild(1).GetComponent<Text>().text = "Collect the fuel to start! Good luck!";
        yield return new WaitForSeconds(3f);
        endGameText.SetActive(false);
    }

    /// <summary>
    /// After the set time it will stop the game
    /// </summary>
    /// <param name="time"></param>
    IEnumerator RestafterTime(float time)
    {

        PauseTheGame();
        yield return new WaitForSeconds(3);
        levelCounter -= 1; // it is just one higher than it should be
        if (levelCounter > spaceGameHighScore)
        {
            PlayerPrefs.SetInt("spaceGameHighScore", levelCounter);
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "New High Score of  " + levelCounter.ToString();
        }
        else
        {
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "You Scored " + levelCounter.ToString() + " Well Done!";
            yield return new WaitForSeconds(3);
            endGameText.transform.GetChild(1).GetComponent<Text>().text = "The High Score is  " + spaceGameHighScore.ToString();
        }
        
        game.transform.position = new Vector3(0, -15, 0);
        Player.GetComponent<MeshRenderer>().enabled = true;
        ob.SetActive(false);
        Player.transform.Find("Explosion").gameObject.SetActive(false);

        Player.GetComponent<Collider>().enabled = true;

        StopTheGame();
    }


    void StopTheGame()
    {
        OverScenesManager.NextScene = "Gamegardening";
        OverScenesManager.NextPlayerName = "Test player";
        OverScenesManager.NextTargetName = "Target";
        OverScenesManager.LoadOverScene = true;
    }

    void PauseTheGame()
    {

        endGameText.SetActive(true);

        OverScenesManager.PauseLabView = true;

    }

}