﻿using UnityEngine;


/// <summary>
/// This <c>SpaceGameDemo</c> class.
/// Handles weather its going to be a demo or the player, playing the game.
/// <list type="bullet">
/// <item>
/// <term>Start</term>
/// <description>Checks if the user wants to see a demo or the live game</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class SpaceGameDemo : MonoBehaviour
{
    public static bool SpaceDemo = false;

    public GameObject SpacePlayer;
    public GameObject SpaceDemoPlayer;
    public GameObject Cam;

    // Use this for initialization
    void Start()
    {
        if (SpaceDemo)
        {
            SpaceDemoPlayer.SetActive(true);
            SpaceDemoPlayer.transform.position = new Vector3(SpacePlayer.transform.position.x, 0.5f, SpacePlayer.transform.position.z);
            Cam.GetComponent<CameraControl>().player = SpaceDemoPlayer;
        }
        else
        {
            SpacePlayer.SetActive(true);
            SpacePlayer.transform.position = new Vector3(SpacePlayer.transform.position.x, 0.5f, SpacePlayer.transform.position.z);
            Cam.GetComponent<CameraControl>().player = SpacePlayer;
        }
    }
}
