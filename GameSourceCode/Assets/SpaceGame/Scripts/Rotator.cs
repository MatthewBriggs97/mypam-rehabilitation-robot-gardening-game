﻿using UnityEngine;

/// <summary>
/// This <c>rotator</c> class.
/// Rotats a object on all axis.
/// <list type="bullet">
/// <item>
/// <term>Update</term>
/// <description>Moves the rotation</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class Rotator : MonoBehaviour {

    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}
