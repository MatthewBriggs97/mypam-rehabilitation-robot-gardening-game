﻿using UnityEngine;

/// <summary>
/// This <c>cameraControl</c> class.
/// Moves the camera for the space game.
/// <list type="bullet">
/// <item>
/// <term>Update</term>
/// <description>CAlled every frame and sets the camera postion behind the player</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class CameraControl : MonoBehaviour {

    /// <summary>
    /// The player and the height away from the player
    /// </summary>
    public GameObject player;
    public float cameraHeight = 20.0f;

    /// <summary>
    /// the update method to move the camera way from the player
    /// </summary>
    void Update()
    {
        Vector3 pos = player.transform.position;
        pos.y = cameraHeight;
        transform.position = pos;
    }
}