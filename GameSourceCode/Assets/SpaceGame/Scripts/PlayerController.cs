﻿using UnityEngine;


/// <summary>
/// This <c>PlayerController</c> class.
/// Contains all methods for moving the rotation of object to face there moving direction.
/// <list type="bullet">
/// <item>
/// <term>Start</term>
/// <description>Gets the objects transform and the last postion of the object</description>
/// </item>
/// <item>
/// <term>Update</term>
/// <description>Fixed update is used for animations, this is animating the direction of the spaceship</description>
/// </item>
/// </list>
/// </summary>
/// <remarks>
/// <para>Authors Matthew Briggs and Jonathan Alderson</para>
/// <para>Emails: ll15m3b@leeds.ac.uk and sc17j3a@leeds.ac.uk</para>
/// </remarks>
public class PlayerController : MonoBehaviour
{

    // declare transform
    private Transform tr;
    public Vector3 defaultRotation = new Vector3(270, 180, 0);
    public GameObject game;

    private Vector3 prevPos;
    public float threshold;
    public float turnSpeed = 10f;

    /// <summary>
    /// Used this for initialization
    /// </summary>    
    void Start()
    {
        // get transform, as we will need to set the position from labview
        tr = transform;

        prevPos = tr.position;
    }

    /// <summary>
    /// Update is called once per frame, used for animations
    /// </summary>    
    void FixedUpdate()
    {
        //turning
        Vector3 moveDirection = tr.position - prevPos;
        Quaternion targetRotation;

        MoveBox test = game.GetComponent<MoveBox>();

        if (moveDirection.magnitude / Time.fixedDeltaTime > threshold)
        {
            float angle = Mathf.Atan2(moveDirection.z, moveDirection.x) * Mathf.Rad2Deg;
            targetRotation = Quaternion.AngleAxis(angle - 270, Vector3.down);
            
        }
        else if (test.speed > 0)
        {
            targetRotation = Quaternion.Euler(defaultRotation);
        }
        else
        {
            targetRotation = Quaternion.Euler(new Vector3(0, defaultRotation.y, defaultRotation.z));
        }


        tr.rotation = Quaternion.Slerp(tr.rotation, targetRotation, Time.fixedDeltaTime * turnSpeed);
        prevPos = tr.position;
    }
}